<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Page for editing options on an allocation form.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use mod_allocationform\helper;
use mod_allocationform\option;
use mod_allocationform\option_form;
use mod_allocationform\output\form;

require_once(dirname(dirname(__DIR__)) . '/config.php');

$id = required_param('id', PARAM_INT); // Course_module ID.

list($course, $cm) = get_course_and_cm_from_cmid($id, 'allocationform');

if ($cm->customdata['state'] == helper::STATE_EDITING) {
    $optionid = optional_param('option', 0, PARAM_INT);
} else {
    $optionid = required_param('option', PARAM_INT);
}

require_login($course, false, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/allocationform:edit', $context);

$PAGE->set_url('/mod/allocationform/editoption.php', ['id' => $id, 'option' => $optionid]);
$PAGE->set_title(format_string($cm->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);

$formdata = array(
    'course' => $course->id,
    'allocationform' => $cm->instance,
    'id' => $cm->id,
);

if ($optionid > 0) {
    $formdata['option'] = $optionid;
    $formdata['function'] = helper::FUNC_EDIT_OPTION;
    $option = option::get($optionid);
} else {
    $option = new option(new stdClass());
}

// Create form and set defaults.
$form = new option_form(null, $formdata);
$form->set_data($option->get_record());

$returnurl = new moodle_url('/mod/allocationform/view.php', ['id' => $id]);

$validstates = array(
    helper::STATE_EDITING,
    helper::STATE_REVIEW,
);
if (!in_array($cm->customdata['state'], $validstates) || $form->is_cancelled()) {
    // The form is cancelled, or in the wrong state.
    redirect($returnurl);
}

if ($option->formdata = $form->get_data()) {
    if ($option->update()) {
        if ($optionid > 0) {
            $message = get_string('optionmodified', 'mod_allocationform', $option->get_name());
            $event = \mod_allocationform\event\option_edited::create(array(
                'objectid' => $cm->instance,
                'context' => $context,
                'other' => ['option' => $optionid],
            ));
        } else {
            $message = get_string('optioncreated', 'mod_allocationform', $option->get_name());
            $event = \mod_allocationform\event\option_created::create(array(
                'objectid' => $cm->instance,
                'context' => $context,
                'other' => ['option' => $option->get_id()],
            ));
        }
        $event->trigger();
        $status = \core\output\notification::NOTIFY_SUCCESS;
    } else {
        $message = get_string('duplicateoption', 'mod_allocationform');
        $status = \core\output\notification::NOTIFY_ERROR;
    }
    redirect($returnurl, $message, null, $status);
}

$output = $PAGE->get_renderer('mod_allocationform');
$renderable = new form($form, $cm->customdata['state']);
echo $output->header();
echo $output->heading(format_string($cm->name));
echo $output->render($renderable);
echo $output->footer();

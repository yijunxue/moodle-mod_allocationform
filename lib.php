<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants for module allocationform
 *
 * All the core Moodle functions, neeeded to allow the module to work
 * integrated in Moodle should be placed here.
 * All the allocationform specific functions, needed to implement all the module
 * logic, should go to locallib.php. This will help to save some memory when
 * Moodle is performing actions across all modules.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Returns the information on whether the module supports a feature
 *
 * @see plugin_supports() in lib/moodlelib.php
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 *
 * FEATURE_GRADE_HAS_GRADE, FEATURE_GRADE_OUTCOMES, FEATURE_COMPLETION_TRACKS_VIEWS, FEATURE_COMPLETION_HAS_RULES,
 * FEATURE_NO_VIEW_LINK, FEATURE_IDNUMBER, FEATURE_GROUPS, FEATURE_GROUPINGS, FEATURE_GROUPMEMBERSONLY, FEATURE_MOD_ARCHETYPE,
 * FEATURE_MOD_INTRO, FEATURE_MODEDIT_DEFAULT_COMPLETION, FEATURE_COMMENT, FEATURE_RATE, FEATURE_BACKUP_MOODLE2
 */
function allocationform_supports($feature) {
    switch($feature) {
        case FEATURE_MOD_INTRO:
        case FEATURE_BACKUP_MOODLE2:
        case FEATURE_COMPLETION_TRACKS_VIEWS:
        case FEATURE_COMPLETION_HAS_RULES:
            return true;
        case FEATURE_SHOW_DESCRIPTION:
            return true;
        case FEATURE_GRADE_HAS_GRADE:
        case FEATURE_GRADE_OUTCOMES:
        case FEATURE_RATE:
        case FEATURE_COMMENT:
        case FEATURE_GROUPINGS:
        case FEATURE_GROUPS:
            return false;
        default:
            return null;
    }
}

/**
 * List of view style log actions
 * @return array
 */
function allocationform_get_view_actions() {
    return array('view', 'view all', 'generate_csv', 'access_denied');
}

/**
 * List of update style log actions
 * @return array
 */
function allocationform_get_post_actions() {
    return array('update', 'add', 'back_to_edit', 'make_ready', 'edit_option', 'edit_disallow',
        'reprocess', 'set_results_available');
}

/**
 * This function is used by the reset_course_userdata function in moodlelib.
 * @param stdClass $data the data submitted from the reset course.
 * @return array status array
 */
function allocationform_reset_userdata($data) {
    return \mod_allocationform\activity::reset_user_data($data);
}

/**
 * Implementation of the function for printing the form elements that control
 * whether the course reset functionality affects the assignment.
 * @param moodleform $mform form passed by reference
 */
function allocationform_reset_course_form_definition(&$mform) {
    $mform->addElement('header', 'allocationformheader', get_string('modulenameplural', 'allocationform'));
    $mform->addElement('advcheckbox', 'reset_allocation_state', get_string('resetstate', 'allocationform'));
    $mform->addElement('advcheckbox', 'reset_allocation_userdata', get_string('deleteuserdata', 'allocationform'));
    $mform->disabledIf('reset_allocation_userdata', 'reset_allocation_state', 'checked');
}

/**
 * Course reset form defaults.
 * @param  object $course
 * @return array
 */
function allocationform_reset_course_form_defaults($course) {
    return array(
        'reset_allocation_state' => 1,
        'reset_allocation_userdata' => 1,
    );
}

/**
 * Saves a new instance of the allocationform into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param stdClass $allocationform An object from the form in mod_form.php
 * @param mod_allocationform_mod_form $mform
 * @return int The id of the newly inserted allocationform record
 */
function allocationform_add_instance(stdClass $allocationform, mod_allocationform_mod_form $mform = null) {
    return \mod_allocationform\activity::create($allocationform);
}

/**
 * Updates an instance of the allocationform in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param \stdClass $allocationform An object from the form in mod_form.php
 * @param mod_allocationform_mod_form $mform
 * @return boolean Success/Fail
 */
function allocationform_update_instance(stdClass $allocationform, mod_allocationform_mod_form $mform = null) {
    $allocationform->id = $allocationform->instance;
    return \mod_allocationform\activity::update($allocationform);
}

/**
 * Removes an instance of the allocationform from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function allocationform_delete_instance($id) {
    return \mod_allocationform\activity::delete($id);
}

/**
 * Obtains the automatic completion state for this allocationform based on any conditions in allocationform settings.
 *
 * @param object $course Course
 * @param object $cm Course-module
 * @param int $userid User ID
 * @param bool $type Type of comparison (or/and; can be used as return value if no conditions)
 * @return bool True if completed, false if not. (If no conditions, then return
 *   value depends on comparison type)
 */
function allocationform_get_completion_state($course, $cm, $userid, $type) {
    global $DB;

    // Get allocationform details.
    if (!($allocationform = $DB->get_record('allocationform', array('id' => $cm->instance)))) {
        throw new Exception("Can't find allocation form ID: {$cm->instance}");
    }

    $result = $type; // Default return value.

    // If completion option is enabled, evaluate it and return true/false.
    if ($allocationform->trackcompletion) {
        $choices = $DB->record_exists('allocationform_choices', array('formid' => $cm->instance, 'userid' => $userid));

        return completion_info::aggregate_completion_states($type, $result, $choices);
    } else {
        // Completion option is not enabled so just return $type.
        return $type;
    }
}

/**
 * Callback which returns human-readable strings describing the active completion custom rules for the module instance.
 *
 * @param cm_info|stdClass $cm object with fields ->completion and ->customdata['customcompletionrules']
 * @return array $descriptions the array of descriptions for the custom rules.
 */
function mod_allocationform_get_completion_active_rule_descriptions($cm) {
    // Values will be present in cm_info, and we assume these are up to date.
    if (empty($cm->customdata['customcompletionrules'])
        || $cm->completion != COMPLETION_TRACKING_AUTOMATIC) {
        return [];
    }

    $descriptions = [];
    foreach ($cm->customdata['customcompletionrules'] as $key => $val) {
        switch ($key) {
            case 'trackcompletion':
                if (!empty($val)) {
                    $descriptions[] = get_string('trackcompletion', 'allocationform');
                }
                break;
            default:
                break;
        }
    }
    return $descriptions;
}

/**
 * This is used by the recent activity block
 *
 * Given a course and a time, this module should find recent activity
 * that has occurred in allocationform activities and print it out.
 * Return true if there was output, or false is there was none.
 *
 * @param mixed $course the course to print activity for
 * @param bool $viewfullnames boolean to determine whether to show full names or not
 * @param int $timestart the time the rendering started
 * @return bool true if activity was printed, false otherwise.
 */
function allocationform_print_recent_activity($course, $viewfullnames, $timestart) {
    return false; // True if anything was printed, otherwise false.
}

/**
 * Prepares the recent activity data
 *
 * This callback function is supposed to populate the passed array with
 * custom activity records. These records are then rendered into HTML via
 * {@see allocationform_print_recent_mod_activity()}.
 *
 * @param array $activities sequentially indexed array of objects with the 'cmid' property
 * @param int $index the index in the $activities to use for the next record
 * @param int $timestart append activity since this time
 * @param int $courseid the id of the course we produce the report for
 * @param int $cmid course module id
 * @param int $userid check for a particular user's activity only, defaults to 0 (all users)
 * @param int $groupid check for a particular group's activity only, defaults to 0 (all groups)
 * @return void adds items into $activities and increases $index
 */
function allocationform_get_recent_mod_activity(&$activities, &$index, $timestart, $courseid, $cmid, $userid=0, $groupid=0) {
}

/**
 * Prints single activity item prepared by {see allocationform_get_recent_mod_activity()}
 *
 * @param \stdClass $activity the activity object the glossary resides in
 * @param int $courseid the id of the course the glossary resides in
 * @param bool $detail not used, but required for compatibilty with other modules
 * @param int $modnames not used, but required for compatibilty with other modules
 * @param bool $viewfullnames not used, but required for compatibilty with other modules
 */
function allocationform_print_recent_mod_activity($activity, $courseid, $detail, $modnames, $viewfullnames) {
}

/**
 * Returns an array of users who are participanting in this allocationform
 *
 * Must return an array of users who are participants for a given instance
 * of allocationform. Must include every user involved in the instance,
 * independient of his role (student, teacher, admin...). The returned
 * objects must contain at least id property.
 * See other modules as example.
 *
 * @param int $allocationformid ID of an instance of this module
 * @return boolean|array false if no participants, array of objects otherwise
 */
function allocationform_get_participants($allocationformid) {
    return \mod_allocationform\activity::get_participants($allocationformid);
}

/**
 * Returns all other caps used in the module
 *
 * @return array
 */
function allocationform_get_extra_capabilities() {
    return array(
        'mod/allocationform:viewform',
        'mod/allocationform:reallocate',
        'mod/allocationform:edit',
        'mod/allocationform:viewallocations',
        'mod/allocationform:exportallocations',
        'mod/allocationform:exportchoices',
    );
}

// File API.

/**
 * Returns the lists of all browsable file areas within the given module context
 *
 * The file area 'intro' for the activity introduction field is added automatically
 * by {@see file_browser::get_file_info_context_module()}
 *
 * @param \stdClass $course
 * @param \stdClass $cm
 * @param \stdClass $context
 * @return array of [(string)filearea] => (string)description
 */
function allocationform_get_file_areas($course, $cm, $context) {
    return array();
}

/**
 * Serves the files from the allocationform file areas
 *
 * @param \stdClass $course
 * @param \stdClass $cm
 * @param \stdClass $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @return void this should never return to the caller
 */
function allocationform_pluginfile($course, $cm, $context, $filearea, array $args, $forcedownload) {

    if ($context->contextlevel != CONTEXT_MODULE) {
        send_file_not_found();
    }

    require_login($course, true, $cm);

    send_file_not_found();
}

/**
 * Add a get_coursemodule_info function in case any assignment type wants to add 'extra' information
 * for the course (see resource).
 *
 * Given a course_module object, this function returns any "extra" information that may be needed
 * when printing this activity in a course listing, it saves us having to go back to the database
 * if we have a cm_info object. See get_array_of_activities() in course/lib.php.
 *
 * @param cm_info $coursemodule The coursemodule object (record).
 * @return cached_cm_info An object on information that the courses
 *                        will know about (most noticeably, an icon).
 */
function allocationform_get_coursemodule_info($coursemodule) {
    global $DB;
    $dbparams = ['id' => $coursemodule->instance];
    $fields = 'id, name, startdate, deadline, state, roleid, processed, intro, introformat, trackcompletion, numberofallocations, '
            . 'numberofchoices, notwant';
    if (!$allocation = $DB->get_record('allocationform', $dbparams, $fields)) {
        return null;
    }

    $result = new cached_cm_info();
    $result->name = $allocation->name;
    if ($coursemodule->showdescription) {
        $result->content = format_module_intro('allocationform', $allocation, $coursemodule->id, false);
    }

    // Populate the custom completion rules as key => value pairs, but only if the completion mode is 'automatic'.
    if ($coursemodule->completion == COMPLETION_TRACKING_AUTOMATIC) {
        $result->customdata['customcompletionrules']['trackcompletion'] = $allocation->trackcompletion;
    }
    // Store basic data about the allocation form.
    $result->customdata['startdate'] = $allocation->startdate;
    $result->customdata['deadline'] = $allocation->deadline;
    $result->customdata['state'] = $allocation->state;
    $result->customdata['roleid'] = $allocation->roleid;
    $result->customdata['allocations'] = $allocation->numberofallocations;
    $result->customdata['choices'] = $allocation->numberofchoices;
    $result->customdata['notwant'] = $allocation->notwant;
    return $result;
}

/**
 * Check if the module has any update that affects the current user since a given time.
 *
 * @param cm_info $cm course module data
 * @param int $from the time to check updates from
 * @param array $filter if we need to check only specific updates
 * @return stdClass an object with the different type of areas indicating if they were updated or not
 * @since Moodle 3.2
 */
function allocationform_check_updates_since(cm_info $cm, $from, $filter = array()) {
    // It isn't really possible at present to detect any other updates in the plugin right now as nothing is dated.
    $updates = course_check_module_updates_since($cm, $from, array(), $filter);
    return $updates;
}

/**
 * Checks if the event should be visible to the user.
 *
 * @param calendar_event $event
 * @param int $userid User id to use for all capability checks, etc. Set to 0 for current user (default).
 * @return bool Returns true if the event is visible to the current user, false otherwise.
 */
function mod_allocationform_core_calendar_is_event_visible(calendar_event $event, $userid = 0) {
    global $USER;
    if (empty($userid)) {
        $userid = $USER->id;
    }
    $cm = get_fast_modinfo($event->courseid)->instances['allocationform'][$event->instance];
    $context = context_module::instance($cm->id);
    if ($event->eventtype == \mod_allocationform\activity::EVENT_DEADLINE) {
        if (\mod_allocationform\activity::user_can_choose($userid, $cm)) {
            // Notify the user who can make choices that their deadline is aproaching.
            return true;
        } else if (\mod_allocationform\activity::user_can_review($userid, $cm)) {
            // Notify a user who can release the results that they have been processed.
            return true;
        }
    }
    if ($event->eventtype == \mod_allocationform\activity::EVENT_AVALIABLE) {
        if (\mod_allocationform\activity::user_can_be_allocated($userid, $cm)) {
            return true;
        }
    }
    return false;
}


/**
 * This function receives a calendar event and returns the action associated with it, or null if there is none.
 *
 * This is used by block_myoverview in order to display the event appropriately. If null is returned then the event
 * is not displayed on the block.
 *
 * @param calendar_event $event
 * @param \core_calendar\action_factory $factory
 * @param int $userid User id to use for all capability checks, etc. Set to 0 for current user (default).
 * @return \core_calendar\local\event\entities\action_interface|null
 */
function mod_allocationform_core_calendar_provide_event_action(calendar_event $event,
                                                               \core_calendar\action_factory $factory,
                                                               $userid = 0) {
    global $USER;
    if (empty($userid)) {
        $userid = $USER->id;
    }
    $cm = get_fast_modinfo($event->courseid, $userid)->instances['allocationform'][$event->instance];
    $url = new moodle_url('/mod/allocationform/view.php', ['id' => $cm->id]);

    if ($event->eventtype == \mod_allocationform\activity::EVENT_DEADLINE) {
        $canreview = \mod_allocationform\activity::user_can_review($userid, $cm);
        if ($canreview) {
            $name = get_string('overviewreview', 'mod_allocationform');
        } else {
            $name = get_string('makechoice', 'mod_allocationform');
        }
        $itemcount = 1;
        $actionable = $canreview || \mod_allocationform\activity::user_can_choose($userid, $cm);
    } else if ($event->eventtype == \mod_allocationform\activity::EVENT_AVALIABLE) {
        $name = get_string('overviewready', 'mod_allocationform');
        $itemcount = 1;
        $actionable = \mod_allocationform\activity::user_can_be_allocated($userid, $cm);
    } else {
        return null;
    }

    return $factory->create_instance(
        $name,
        $url,
        $itemcount,
        $actionable
    );
}

/**
 * Callback function that determines whether an action event should be showing its item count
 * based on the event type and the item count.
 *
 * @param calendar_event $event The calendar event.
 * @param int $itemcount The item count associated with the action event.
 * @return bool
 */
function mod_allocationform_core_calendar_event_action_shows_item_count(calendar_event $event, $itemcount = 0) {
    return ($itemcount > 1);
}

<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main allocationform configuration form
 *
 * It uses the standard core Moodle formslib. For more info about them, please
 * visit: http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');

/**
 * Module instance settings form
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_allocationform_mod_form extends moodleform_mod {

    /**
     * Defines forms elements
     */
    public function definition() {
        global $PAGE, $CFG;

        $mform = $this->_form;

        // Adding the "general" fieldset, where all the common settings are shown.
        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Adding the standard "name" field.
        $mform->addElement('text', 'name', get_string('allocationformname', 'mod_allocationform'), array('size' => '64'));

        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEAN);
        }

        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('name', 'allocationformname', 'mod_allocationform');

        $this->standard_intro_elements();

        // Adding the rest of allocationform settings, spreeading all them into this fieldset
        // or adding more fieldsets ('header' elements) if needed for better logic.
        $mform->addElement('header', 'choicesheading', get_string('choiceheading', 'mod_allocationform'));
        $mform->setExpanded('choicesheading');

        $choiceoptions = array(1 => '1', '2', '3', '4', '5', '6', '7', '8', '9', '10');

        // The number of choices a user will be allowed to rank.
        $mform->addElement('select', 'numberofchoices', get_string('numberofchoices', 'mod_allocationform'), $choiceoptions, null);
        $mform->addHelpButton('numberofchoices', 'numberofchoices', 'mod_allocationform');
        $mform->setDefault('numberofchoices', 5);

        // If a avoid this option should be given to the user.
        $mform->addElement('advcheckbox', 'notwant', get_string('notwant', 'mod_allocationform'), '', null, array(0, 1));
        $mform->addHelpButton('notwant', 'notwant', 'mod_allocationform');

        // The number of options a user will be allocated to.
        $mform->addElement('select', 'numberofallocations',
                get_string('numberofallocations', 'mod_allocationform'), $choiceoptions, null);
        $mform->addHelpButton('numberofallocations', 'numberofallocations', 'mod_allocationform');
        $mform->setDefault('numberofallocations', 1);

        $mform->addElement('header', 'availabilityheading', get_string('availabilityheading', 'mod_allocationform'));
        $mform->setExpanded('availabilityheading');

        $mform->addElement('date_time_selector', 'startdate', get_string('startdate', 'mod_allocationform'));
        $mform->addHelpButton('startdate', 'startdate', 'mod_allocationform');

        $mform->addElement('date_time_selector', 'deadline', get_string('deadline', 'mod_allocationform'));
        $mform->addHelpButton('deadline', 'deadline', 'mod_allocationform');

        // Get a list of roles in Moodle.
        $roles = role_fix_names(get_all_roles(), $PAGE->context, ROLENAME_ALIAS);
        $roleoptions = array();

        foreach ($roles as $role) {
            $roleoptions[$role->id] = $role->localname;
        }

        $mform->addElement('select', 'roleid', get_string('roleselect', 'mod_allocationform'), $roleoptions, null);
        $mform->addHelpButton('roleid', 'roleselect', 'mod_allocationform');

        // Find andd set the default role.
        $defaultrole = get_config('mod_allocationform', 'defaultrole');

        if (!empty($defaultrole)) {
            $mform->setDefault('roleid', $defaultrole);
        }

        // Add standard elements, common to all modules.
        $this->standard_coursemodule_elements();

        // Add standard buttons, common to all modules.
        $this->add_action_buttons();
    }

    /**
     * Add a custom completion rule to the form
     *
     * @return array
     */
    public function add_completion_rules() {
        $mform =& $this->_form;

        $mform->addElement('checkbox', 'trackcompletion',
                get_string('requirechoice', 'mod_allocationform'),
                get_string('trackcompletion', 'mod_allocationform'));
        $mform->setType('trackcompletion', PARAM_INT);
        return array('trackcompletion');
    }

    /**
     * Check whether the user selected the custom completion tracking option
     *
     * @param array $data
     * @return bool|array
     */
    public function completion_rule_enabled($data) {
        return (!empty($data['trackcompletion']));
    }

    /**
     * {@see moodleform_mod::data_postprocessing}
     */
    public function data_postprocessing($data) {
        if (!empty($data->completionunlocked)) {
            // Turn off completion settings if the checkboxes aren't ticked.
            $autocompletion = !empty($data->completion) && $data->completion == COMPLETION_TRACKING_AUTOMATIC;
            if (empty($data->trackcompletion) || !$autocompletion) {
                $data->trackcompletion = 0;
            }
        }
        return $data;
    }
}

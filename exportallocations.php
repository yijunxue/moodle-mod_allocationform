<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Page for exporting allocations from the allocation form.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use mod_allocationform\init;

require_once(dirname(dirname(__DIR__)) . '/config.php');

$id = required_param('id', PARAM_INT); // Course_module ID.

list($course, $cm) = get_course_and_cm_from_cmid($id, 'allocationform');

require_login($course, false, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/allocationform:exportallocations', $context);

$PAGE->set_url('/mod/allocationform/exportallocations.php', ['id' => $id]);
$PAGE->set_title(format_string($cm->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);

$init = new init($DB->get_record('allocationform', array('id' => $cm->instance), '*', MUST_EXIST));

$event = \mod_allocationform\event\export_allocations::create(array(
    'objectid' => $cm->instance,
    'context' => $context
));
$event->trigger();

$init->generate_allocation_csv();

<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing unit tests for the mod_allocationform\output\editing class.
 *
 * @package   mod_allocationform
 * @copyright 2019 University of Nottingham
 * @author    Neill Magill <neill.magill@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\output;

defined('MOODLE_INTERNAL') || die();

/**
 * Unit tests for the mod_allocationform\output\editing class.
 *
 * @package   mod_allocationform
 * @copyright 2019 University of Nottingham
 * @author    Neill Magill <neill.magill@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_allocationform
 * @group uon
 */
class editing_test extends \advanced_testcase {
    /** @var \mod_allocationform_generator The allocation form data generator. */
    protected $generator;

    /**
     * Setup for each test.
     */
    public function setUp(): void {
        parent::setUp();
        $this->generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $this->resetAfterTest(true);
    }

    /**
     * Run at the end of each test.
     */
    public function tearDown(): void {
        $this->assertDebuggingNotCalled();
        parent::tearDown();
    }

    /**
     * Data for testing mod_allocationform_init::get_number_of_allocation_slots.
     *
     * @return array
     */
    public function data_get_number_of_allocation_slots() {
        return array(
            'no slots' => [0, []],
            'single option' => [2, [['maxallocation' => 2]]],
            'multiple options' => [8, [['maxallocation' => 2], ['maxallocation' => 6]]],
        );
    }

    /**
     * Tests that the number of slots is calculated correctly.
     *
     * @param int $spaces
     * @param array $choices Data used to create choices
     * @dataProvider data_get_number_of_allocation_slots
     */
    public function test_get_number_of_allocation_slots($spaces, $choices) {
        $course = self::getDataGenerator()->create_course();
        $allocation = self::getDataGenerator()->create_module('allocationform', ['course' => $course->id]);
        $cm = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        foreach ($choices as $choice) {
            $this->generator->create_option($allocation, $choice);
        }
        $editing = editing::get($cm);
        $this->assertEquals($spaces, $editing->totalslots);
    }

    /**
     * Test that if a user has set of restrictions on them that means they cannot be allocated correctly they are detected.
     */
    public function test_count_bad_restrictions() {
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($user2->id, $course->id, 'student');
        $allocationparams = array(
            'course' => $course->id,
            'numberofchoices' => 1,
            'numberofallocations' => 1,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $cm = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        $option1 = $this->generator->create_option($allocation, ['maxallocation' => 1]);
        $option2 = $this->generator->create_option($allocation, ['maxallocation' => 2]);
        $editing = editing::get($cm);
        // No restrictions at all.
        $this->assertEquals(0, $editing->badrestrictions);
        // A restriction that means the user can still be allocated.
        $this->generator->create_restriction($allocation, $user1, $option1);
        $editing2 = editing::get($cm);
        $this->assertEquals(0, $editing2->badrestrictions);
        // A user who cannot be allocated at all.
        $this->generator->create_restriction($allocation, $user1, $option2);
        $editing3 = editing::get($cm);
        $this->assertEquals(1, $editing3->badrestrictions);
        // Give another user a restriction that will not block them being allocated.
        $this->generator->create_restriction($allocation, $user2, $option1);
        $editing4 = editing::get($cm);
        $this->assertEquals(1, $editing4->badrestrictions);
        // Two users cannot be allocated.
        $this->generator->create_restriction($allocation, $user2, $option2);
        $editing5 = editing::get($cm);
        $this->assertEquals(2, $editing5->badrestrictions);
    }

    /**
     * Tests that the correct number of users are found.
     */
    public function test_count_users() {
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($user2->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'editingteacher');
        $allocationparams = array(
            'course' => $course->id,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $cm = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        $editing = editing::get($cm);
        $this->assertEquals(2, $editing->people);
    }

    /**
     * Tests that the options for a form are found correctly.
     */
    public function test_get_options() {
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($user2->id, $course->id, 'student');
        $allocationparams = array(
            'course' => $course->id,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $cm = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        $option1 = $this->generator->create_option($allocation, ['maxallocation' => 1]);
        $option2 = $this->generator->create_option($allocation, ['maxallocation' => 2]);
        $this->generator->create_restriction($allocation, $user1, $option2);
        $this->generator->create_restriction($allocation, $user2, $option2);
        $editing = editing::get($cm);
        $this->assertCount(2, $editing->options);
        $this->assertEquals(0, $editing->options[$option1->id]->restrictedusers);
        $this->assertEquals($option1->maxallocation, $editing->options[$option1->id]->maxallocation);
        $this->assertEquals($option1->name, $editing->options[$option1->id]->name);
        $this->assertEquals(2, $editing->options[$option2->id]->restrictedusers);
        $this->assertEquals($option2->maxallocation, $editing->options[$option2->id]->maxallocation);
        $this->assertEquals($option2->name, $editing->options[$option2->id]->name);
    }
}

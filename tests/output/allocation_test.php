<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing unit tests for the mod_allocationform\output\allocation class.
 *
 * @package   mod_allocationform
 * @copyright 2019 University of Nottingham
 * @author    Neill Magill <neill.magill@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\output;

defined('MOODLE_INTERNAL') || die();

/**
 * Unit tests for the mod_allocationform\output\allocation class.
 *
 * @package   mod_allocationform
 * @copyright 2019 University of Nottingham
 * @author    Neill Magill <neill.magill@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_allocationform
 * @group uon
 */
class allocation_test extends \advanced_testcase {
    /** @var \mod_allocationform_generator The allocation form data generator. */
    protected $generator;

    /**
     * Setup for each test.
     */
    public function setUp(): void {
        parent::setUp();
        $this->generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $this->resetAfterTest(true);
    }

    /**
     * Run at the end of each test.
     */
    public function tearDown(): void {
        $this->assertDebuggingNotCalled();
        parent::tearDown();
    }

    /**
     * Test that we can correctly get allocations for a user.
     */
    public function test_get_allocations() {
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $allocation = self::getDataGenerator()->create_module('allocationform', ['course' => $course->id]);
        $cm = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        $option1 = $this->generator->create_option($allocation);
        $option2 = $this->generator->create_option($allocation);
        $this->generator->create_user_choices($allocation, $user, ['choice1' => $option2->id]);
        $this->generator->create_user_choices($allocation, $otheruser, ['choice1' => $option1->id]);
        $this->generator->create_allocation($allocation, $user, $option2);
        $this->generator->create_allocation($allocation, $otheruser, $option1);
        $renderable = allocation::get($cm, $user->id);
        $this->assertCount(1, $renderable->options);
        $this->assertArrayHasKey($option2->id, $renderable->options);
    }
}

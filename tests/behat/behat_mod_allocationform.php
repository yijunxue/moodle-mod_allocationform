<?php
// This file is part of the allocation form activity plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Steps definitions related with the allocationform activity.
 *
 * @package   mod_allocationform
 * @copyright University of Nottingham, 2014
 * @author    Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// NOTE: no MOODLE_INTERNAL test here, this file may be required by behat before including /config.php.

require_once(__DIR__ . '/../../../../lib/behat/behat_base.php');

use Behat\Behat\Context\Step\Given as Given,
    Behat\Gherkin\Node\TableNode as TableNode;

/**
 * Forum-related steps definitions.
 *
 * @package    mod_allocationform
 * @copyright  2014 University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class behat_mod_allocationform extends behat_base {
    /**
     * Check that the allocation is not available
     *
     * @Then /^allocation form is not available$/
     * @return void
     */
    public function allocation_form_is_not_available() {
        $this->execute('behat_general::assert_page_contains_text', array(get_string('not_active', 'mod_allocationform')));
    }

    /**
     * Check that the allocation is available
     *
     * @Then /^allocation form is available$/
     * @return void
     */
    public function allocation_form_is_available() {
        $this->execute('behat_general::assert_page_not_contains_text', array(get_string('not_active', 'mod_allocationform')));
    }

    /**
     * Make the allocation form available
     *
     * @Given /^I make the allocation form active$/
     * @return void
     */
    public function i_make_the_allocation_form_active() {
        $this->execute('behat_general::click_link', array($this->escape(get_string('make_active', 'mod_allocationform'))));
        $this->execute('behat_forms::press_button', array(get_string('yes')));
    }

    /**
     * Check that the allocation form is waiting to be processed
     *
     * @Then /^the allocation form is waiting to be processed$/
     * @return void
     */
    public function the_allocation_form_is_waiting_to_be_processed() {
        $this->execute('behat_general::assert_page_contains_text', array(get_string('queued_for_processing', 'mod_allocationform')));
    }

    /**
     * Check that a link to generate CSV of choices exists
     *
     * @Then /^I should see generate csv$/
     * @return void
     */
    public function i_should_see_generate_csv() {
        $this->execute('behat_general::assert_page_contains_text', array(get_string('generate_csv', 'mod_allocationform')));
    }

    /**
     * Switch the allocation form back to edit mode
     *
     * @Given /^I switch to edit mode$/
     * @return void
     */
    public function i_switch_to_edit_mode() {
        $this->execute('behat_general::click_link', array(get_string('back_to_edit', 'mod_allocationform')));
        $this->execute('behat_forms::press_button', array(get_string('yes')));
        $this->execute('behat_general::i_wait_to_be_redirected', array());
    }

    /**
     * Check that the duplicate option notice is displayed
     *
     * @Then /^I should see duplicate option notice$/
     * @return void
     */
    public function i_should_see_duplicate_option_notice() {
        $this->execute('behat_general::assert_page_contains_text', array(get_string('duplicateoption', 'mod_allocationform')));
    }

    /**
     * Tests that a message stating the number of users and options that are affected by over restriction is present.
     *
     * @Then /^workable restrictions were exceeded for (\d+) user\(s\) and (\d+) options required per user$/
     * @return void
     */
    public function workable_restrictions_were_exceeded_for_users_options_required_per_user($users, $numberofchoices) {
        $a = new stdClass();
        $a->users = $users;
        $a->numberofchoices = $numberofchoices;
        $this->execute('behat_general::assert_page_contains_text',
                array(get_string('restrictionsexceeded', 'mod_allocationform', $a)));
    }

    /**
     * Deletes the named option from the current allocation form activity that is in it's edit stage.
     *
     * @Given /^I delete allocation form option "([^"]*)"$/
     * @param string $option
     * @return void
     */
    public function i_delete_allocationform_option($option) {
        $xpath = "//*[contains(concat(' ', normalize-space(@class), ' '), ' option ') "
            . "and .//text()[contains(., '" . $this->escape($option) . "')]]"
            . "//a[contains(concat(' ', normalize-space(@class), ' '), ' delete ')]";
        $this->execute('behat_general::i_click_on', array($xpath, 'xpath_element'));
        $this->execute('behat_general::i_wait_to_be_redirected', array());
    }

    /**
     * Presses the edit button for an option while on an allocation form editing page.
     *
     * @Given /^I edit allocation form option "([^"]*)"$/
     *
     * @param string $option
     * @return void
     */
    public function i_edit_the_allocation_form_option($option) {
        $xpath = "//*[contains(concat(' ', normalize-space(@class), ' '), ' option ') "
            . "and .//text()[contains(., '" . $this->escape($option) . "')]]"
            . "//a[contains(concat(' ', normalize-space(@class), ' '), ' edit ')]";
        $this->execute('behat_general::i_click_on', array($xpath, 'xpath_element'));
    }

    /**
     * Set the restrictions for users on  a allocation form option.
     *
     *  The table passed can contain two values:
     * - student (required) The username of a student
     * - status (optional) If set to 0 the user will be unchecked otherwise they will be checked
     *
     * The allocation form activity must be in it's editing stage.
     *
     * @Given /^I restrict allocation form option "([^"]*)" from:$/
     *
     * @param string $option
     * @return void
     */
    public function i_restrict_allocationform_option($option, TableNode $students) {
        global $DB;
        $xpath = "//*[contains(concat(' ', normalize-space(@class), ' '), ' option ') "
            . "and .//text()[contains(., '" . $this->escape($option) . "')]]"
            . "//a[contains(concat(' ', normalize-space(@class), ' '), ' restrict ')]";
        $this->execute('behat_general::i_click_on', array($xpath, 'xpath_element'));
        $data = array();
        foreach ($students->getHash() as $row) {
            $user = $DB->get_record('user', array('username' => $row['student']), 'id');
            $field = 'user' . $user->id;
            if (isset($row['status']) && $row['status'] == '0') {
                $status = '0';
            } else {
                $status = '1';
            }
            $data[] = array($field, $status);
        }
        $datefields = new TableNode($data);
        $this->execute('behat_forms::i_set_the_following_fields_to_these_values', array($datefields));
        $this->execute('behat_forms::press_button', array(get_string('savechanges')));
        $this->execute('behat_general::i_wait_to_be_redirected', array());
    }

    /**
     * Checks that an allocation form option has a specific number of restrictions on it.
     *
     * The allocation form activity must be in it's editing stage.
     *
     * @Given /^allocation form option "([^"]*)" should have "([^"]*)" restricted users$/
     *
     * @param string $option
     * @param string $restrictions
     * @return void
     */
    public function allocation_form_option_should_have_restricted_users($option, $restrictions) {
        $xpath = "//*[contains(concat(' ', normalize-space(@class), ' '), ' option ') "
            . "and .//text()[contains(., '" . $this->escape($option) . "')]]"
            . "//*[contains(concat(' ', normalize-space(@class), ' '), ' option_restricted ')]";
        $this->execute('behat_general::assert_element_contains_text', array($restrictions, $xpath, 'xpath_element'));
    }

    /**
     * Check the restrictions for users on  a allocation form option.
     *
     *  The table passed can contain two values:
     * - student The username of a student
     * - status If set to 0 the user will be unchecked otherwise they will be checked
     *
     * The allocation form activity must be in it's editing stage.
     *
     * @Given /^I see allocation form option "([^"]*)" has the following user restrictions:$/
     *
     * @global moodle_datavbase $DB
     * @param string $option
     * @param TableNode $students
     * @return void
     */
    public function i_see_users_resricted_on_allocationform_option($option, TableNode $students) {
        global $DB;
        $xpath = "//*[contains(concat(' ', normalize-space(@class), ' '), ' option ') "
            . "and .//text()[contains(., '" . $this->escape($option) . "')]]"
            . "//a[contains(concat(' ', normalize-space(@class), ' '), ' restrict ')]";
        $this->execute('behat_general::i_click_on', array($xpath, 'xpath_element'));
        $data = array();
        foreach ($students->getHash() as $row) {
            $user = $DB->get_record('user', array('username' => $row['student']), 'id');
            $field = 'user' . $user->id;
            if ($row['status'] == '0') {
                $status = '0';
            } else {
                $status = '1';
            }
            $data[] = array($field, $status);
        }
        $datefields = new TableNode($data);
        $this->execute('behat_forms::the_following_fields_match_these_values', array($datefields));
        $this->execute('behat_forms::press_button', array(get_string('cancel')));
        $this->execute('behat_general::i_wait_to_be_redirected', array());
    }

    /**
     * Adds options to allocation forms.
     *
     * The following parameters should be sent via the table:
     * - form: The idnumber of an allocation form
     * - name: The name of the option
     * - allocations: The number of users who can be allocated to the option
     *
     * @Given the following allocation form options exist:
     *
     * @global moodle_database $DB
     * @param TableNode $options
     * @return void
     */
    public function the_following_allocation_form_options_exist(TableNode $options) {
        global $DB;
        $datagenerator = testing_util::get_data_generator()->get_plugin_generator('mod_allocationform');
        foreach ($options->getHash() as $option) {
            $form = new stdClass();
            $form->id = $DB->get_field('course_modules', 'instance', ['idnumber' => $option['form']], MUST_EXIST);
            $datagenerator->create_option($form, ['name' => $option['name'], 'maxallocation' => $option['allocations']]);
        }
    }

    /**
     * Adds restrictions to allocation forms.
     *
     * The following parameters should be sent via the table:
     * - form: The idnumber of an allocation form
     * - option: The name of the option
     * - user: The username of the user being restricted
     *
     * @Given the following allocation form restrictions exist:
     *
     * @global moodle_database $DB
     * @param TableNode $restrictions
     * @return void
     */
    public function the_following_allocation_form_restrictions_exist(TableNode $restrictions) {
        global $DB;
        $datagenerator = testing_util::get_data_generator()->get_plugin_generator('mod_allocationform');
        foreach ($restrictions->getHash() as $restriction) {
            $form = new stdClass();
            $form->id = $DB->get_field('course_modules', 'instance', ['idnumber' => $restriction['form']], MUST_EXIST);
            $optionparams = ['formid' => $form->id, 'name' => $restriction['option']];
            $option = $DB->get_record('allocationform_options', $optionparams, '*', MUST_EXIST);
            $user = $DB->get_record('user', ['username' => $restriction['user']], '*', MUST_EXIST);
            $datagenerator->create_restriction($form, $user, $option);
        }
    }

    /**
     * Checks that an option exists on the editing page of an allocation form.
     *
     * @Then /^I should see allocation form option "([^"]*)"$/
     *
     * @param string $option
     * @return void
     */
    public function i_should_see_allocation_form_option($option) {
        $params = [$option, '#allocationform-option-list', 'css_element'];
        $this->execute('behat_general::assert_element_contains_text', $params);
    }

    /**
     * Checks that an option does not exist on the editing page of an allocation form.
     *
     * @Then /^I should not see allocation form option "([^"]*)"$/
     *
     * @param string $option
     * @return void
     */
    public function i_should_not_see_allocation_form_option($option) {
        $params = [$option, '#allocationform-option-list', 'css_element'];
        $this->execute('behat_general::assert_element_not_contains_text', $params);
    }

    /**
     * Convert page names to URLs for steps.
     *
     * Recognised page names are:
     * | Page type     | Identifier meaning    | description
     * | View          | Allocation form name  | The main activity page.
     *
     * @param string $type identifies which type of page this is, e.g. 'Attempt review'.
     * @param string $identifier identifies the particular page, e.g. 'Test quiz > student > Attempt 1'.
     * @return moodle_url the corresponding URL.
     * @throws Exception with a meaningful error message if the specified page cannot be found.
     */
    protected function resolve_page_instance_url(string $type, string $identifier): moodle_url {
        switch ($type) {
            case 'View':
                return $this->resolve_view_page_url($identifier);
            default:
                throw new Exception('Unrecognised allocation page type "' . $type . '."');
        }
    }

    /**
     * Gets the view page url for an allocation form.
     *
     * @param string $identifier The name of an allocation form.
     * @return \moodle_url
     */
    protected function resolve_view_page_url(string $identifier): moodle_url {
        $instanceid = $this->resolve_id_from_name($identifier);
        $cm = get_coursemodule_from_instance('allocationform', $instanceid);
        return new moodle_url('/mod/allocationform/view.php', ['id' => $cm->id]);
    }

    /**
     * Gets the instance od of an allocation form.
     *
     * @global moodle_database $DB
     * @param string $name The name of the form.
     * @return int
     * @throws Exception
     */
    protected function resolve_id_from_name(string $name): int {
        global $DB;
        $id = $DB->get_field('allocationform', 'id', ['name' => trim($name)]);
        if (!$id) {
            throw new Exception("The specified allocation form with name '$name' does not exist");
        }
        return $id;
    }
}

@mod @uon @mod_allocationform
Feature: Making choices on an allocation form
  In order to be allocated to the options I desire
  As a student
  I should be able to make choices

  Background:
    Given the following "users" exist:
      | username | firstname | lastname | email                |
      | student1 | Sam       | Student  | student1@example.com |
      | teacher1 | Bob       | Teacher  | teacher1@example.com |
    And the following "courses" exist:
      | fullname | shortname | category | summary       |
      | C1       | C1        | 0        | Test course 1 |
    And the following "course enrolments" exist:
      | user     | course | role           |
      | student1 | C1     | student        |
      | teacher1 | C1     | editingteacher |
    And the following "activities" exist:
      | activity | course | idnumber | name | intro | numberofchoices | state |
      | allocationform | C1 | A1 | Test Allocation form name | Test Allocation form description | 3 | 2 |
    And the following allocation form options exist:
      | form | name    | allocations |
      | A1   | Apples  | 2           |
      | A1   | Oranges | 2           |
      | A1   | Pears   | 2           |
      | A1   | Bananas | 2           |
      | A1   | Thorns  | 2           |

  Scenario: I should have my choices saved
    Given I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "student1"
    And allocation form is available
    When I set the following fields to these values:
      | id_choice1 | Apples  |
      | id_choice2 | Oranges |
      | id_choice3 | Bananas |
    And I press "Save changes"
    And I should see "Your choices have been saved"
    And I am on the "Test Allocation form name" "mod_allocationform > View" page
    Then the following fields match these values:
      | id_choice1 | Apples  |
      | id_choice2 | Oranges |
      | id_choice3 | Bananas |
    And the "id_choice1" select box should contain "Apples"
    And the "id_choice1" select box should contain "Oranges"
    And the "id_choice1" select box should contain "Pears"
    And the "id_choice1" select box should contain "Bananas"
    And the "id_choice1" select box should contain "Thorns"

  Scenario: I should not see options I am restricted from
    Given the following allocation form restrictions exist:
      | form | option | user     |
      | A1   | Apples | student1 |
    And I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "student1"
    Then the "id_choice1" select box should contain "Oranges"
    And the "id_choice1" select box should contain "Pears"
    And the "id_choice1" select box should contain "Bananas"
    And the "id_choice1" select box should contain "Thorns"
    But the "id_choice1" select box should not contain "Apples"

  Scenario: A teacher should not have choices saved
    Given I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "teacher1"
    When I set the following fields to these values:
      | id_choice1 | Apples  |
      | id_choice2 | Oranges |
      | id_choice3 | Bananas |
    And I press "Save changes"
    And I should not see "Your choices have been saved"
    And I am on the "Test Allocation form name" "mod_allocationform > View" page
    Then the following fields match these values:
      | id_choice1 |  |
      | id_choice2 |  |
      | id_choice3 |  |

  Scenario: A teacher should be able to download user choices
    Given I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "teacher1"
    Then I should see generate csv

  Scenario: A teacher should be able to switch back to editing mode
    Given I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "teacher1"
    When I switch to edit mode
    Then I should see allocation form option "Apples"
    And I should see allocation form option "Oranges"
    And I should see allocation form option "Pears"
    And I should see allocation form option "Bananas"
    And I should see allocation form option "Thorns"

<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Allocation form data generator
 *
 * @package   mod_allocationform
 * @copyright University of Nottingham, 2014
 * @author    Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Used to generate data for mod_allocationform.
 *
 * @package   mod_allocationform
 * @copyright University of Nottingham, 2014
 * @author    Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_allocationform_generator extends testing_module_generator {
    /** @var int The number of choices that have been created. */
    protected $choicecount = 0;

    /**
     * Creates an allocation record.
     *
     * @param \stdClass $allocationform The allocation form activity record.
     * @param \stdClass $user The record of the user that has been allocated.
     * @param \stdClass $allocation The record for the option that the user has been allocated to.
     * @return stdClass The allocation record.
     */
    public function create_allocation(stdClass $allocationform, stdClass $user, stdClass $allocation) {
        global $DB;
        $record = array(
            'formid' => $allocationform->id,
            'userid' => $user->id,
            'allocation' => $allocation->id
        );
        $id = $DB->insert_record('allocationform_allocations', $record);
        return $DB->get_record('allocationform_allocations', ['id' => $id], '*', MUST_EXIST);
    }

    /**
     * Create a set of user choices.
     *
     * The choices are passed in an associative array with the following keys:
     * - choice1 (required)
     * - choice2 - choice10 (optional)
     * - notwant (optional)
     *
     * The values should be the id of an option in the allocation form (note the values are not validated)
     *
     * @param \stdClass $allocationform The allocation form activity record.
     * @param \stdClass $user The record of the user who made the choices.
     * @param array $choices An associative array of choices the user has made.
     * @return stdClass The choices record.
     */
    public function create_user_choices(stdClass $allocationform, stdClass $user, array $choices) {
        global $DB;
        $choices = (array)$choices;
        unset($choices['id']);
        unset($choices['formid']);
        unset($choices['userid']);
        if (empty($choices['choice1'])) {
            throw new coding_exception('choice1 is required to be set');
        }
        $defaults = array(
            'formid' => $allocationform->id,
            'userid' => $user->id,
            'choice2' => null,
            'choice3' => null,
            'choice4' => null,
            'choice5' => null,
            'choice6' => null,
            'choice7' => null,
            'choice8' => null,
            'choice9' => null,
            'choice10' => null,
            'notwant' => null,
        );
        $record = $this->datagenerator->combine_defaults_and_record($defaults, $choices);
        $id = $DB->insert_record('allocationform_choices', $record);
        return $DB->get_record('allocationform_choices', ['id' => $id], '*', MUST_EXIST);
    }

    /**
     * Creates an instance of the module for testing purposes.
     *
     * Module type will be taken from the class name. Each module type may overwrite
     * this function to add other default values used by it.
     *
     * @param array|stdClass $record Data for module being generated.
     * @param array $options General options for course module.
     * @return stdClass Record from module-defined table with additional field cmid (corresponding id in course_modules table)
     */
    public function create_instance($record = null, array $options = null) {
        $record = (object)(array)$record;

        if (!isset($record->numberofchoices)) {
            $record->numberofchoices = 1;
        }
        if (!isset($record->notwant)) {
            $record->notwant = 0;
        }
        if (!isset($record->numberofallocations)) {
            $record->numberofallocations = 1;
        }
        if (!isset($record->startdate)) {
            $record->startdate = time();
        }
        if (!isset($record->deadline)) {
            $record->deadline = time() + 86200;
        }
        if (!isset($record->processed)) {
            $record->processed = 0;
        }
        if (!isset($record->state)) {
            $record->state = \mod_allocationform\helper::STATE_EDITING;
        }
        if (!isset($record->roleid)) {
            $studentroles = get_archetype_roles('student');

            foreach ($studentroles as $role) {
                $roleid = $role->id;
                break;
            }
            $record->roleid = $roleid;
        }
        return parent::create_instance($record, (array)$options);
    }

    /**
     * Creates an option for the allocation form.
     *
     * @param \stdClass $allocationform
     * @param $record
     * @return stdClass The option record.
     */
    public function create_option(stdClass $allocationform, $record = []) {
        global $DB;
        $this->choicecount++;
        $record = (array)$record;
        unset($record['id']);
        unset($record['formid']);
        $defaults = array(
            'formid' => $allocationform->id,
            'name' => "Choice {$this->choicecount}",
            'maxallocation' => 10,
            'heading' => '',
            'sortorder' => 1,
        );
        $record = $this->datagenerator->combine_defaults_and_record($defaults, $record);
        $id = $DB->insert_record('allocationform_options', $record);
        return $DB->get_record('allocationform_options', ['id' => $id], '*', MUST_EXIST);
    }

    /**
     * Creates an option that restricts the user from selecting an option.
     *
     * @param \stdClass $allocationform The allocation form activity record.
     * @param \stdClass $user The record of the user that has been restricted.
     * @param \stdClass $restriction The record for the option that the user has been restricted from selecting.
     * @return stdClass The restriction record.
     */
    public function create_restriction(stdClass $allocationform, stdClass $user, stdClass $restriction) {
        global $DB;
        $record = array(
            'formid' => $allocationform->id,
            'userid' => $user->id,
            'disallow_allocation' => $restriction->id
        );
        $id = $DB->insert_record('allocationform_disallow', $record);
        return $DB->get_record('allocationform_disallow', ['id' => $id], '*', MUST_EXIST);
    }

    /**
     * Resets the counter.
     *
     * Should be called when clearing up unit tests after use.
     *
     * @return void
     */
    public function reset() {
        $this->choicecount = 0;
        parent::reset();
    }
}

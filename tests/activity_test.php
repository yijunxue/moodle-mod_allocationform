<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing unit tests for the \mod_allocationform\activity class.
 *
 * @package   mod_allocationform
 * @copyright 2019 University of Nottingham
 * @author    Neill Magill <neill.magill@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform;

defined('MOODLE_INTERNAL') || die();

/**
 * Unit tests for the \mod_allocationform\activity class.
 *
 * @package   mod_allocationform
 * @copyright 2019 University of Nottingham
 * @author    Neill Magill <neill.magill@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_allocationform
 * @group uon
 */
class activity_test extends \advanced_testcase {
    /**
     * Setup for each test.
     */
    public function setUp(): void {
        parent::setUp();
        $this->resetAfterTest(true);
    }

    /**
     * Run at the end of each test.
     */
    public function tearDown(): void {
        $this->assertDebuggingNotCalled();
        parent::tearDown();
    }

    /**
     * Tests creating an allocation form.
     *
     * We test \mod_allocationform\activity::create indirectly, so that all the correct parameters are passed to it.
     *
     * @global \moodle_database $DB
     */
    public function test_create() {
        global $DB;
        $course = self::getDataGenerator()->create_course();
        $time = time();
        // This will call \mod_allocationform\activity::create().
        $allocation = self::getDataGenerator()->create_module('allocationform', ['course' => $course->id]);
        $this->assertTrue($DB->record_exists('allocationform', ['id' => $allocation->id]));
        $expected = clone $allocation;
        unset($expected->cmid);
        $record = $DB->get_record('allocationform', ['id' => $allocation->id]);
        $this->assertEquals($expected, $record);
        $this->assertGreaterThanOrEqual($time, $record->timecreated);
        $this->assertEquals($record->timecreated, $record->timemodified);
        $eventtype = activity::EVENT_DEADLINE;
        $eventparams = ['modulename' => 'allocationform', 'instance' => $allocation->id, 'eventtype' => $eventtype];
        $this->assertTrue($DB->record_exists('event', $eventparams));
    }

    /**
     * Tests deleting an allocation form.
     *
     * @global \moodle_database $DB
     */
    public function test_delete() {
        global $DB;
        $this->assertFalse(activity::delete(15));
        $generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $user = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        // Create a form that will be deleted.
        $allocation = self::getDataGenerator()->create_module('allocationform', ['course' => $course->id]);
        $option1 = $generator->create_option($allocation);
        $option2 = $generator->create_option($allocation);
        $generator->create_user_choices($allocation, $user, ['choice1' => $option2->id]);
        $generator->create_allocation($allocation, $user, $option2);
        $generator->create_restriction($allocation, $user, $option1);
        // Create a form that should not be deleted.
        $otherallocation = self::getDataGenerator()->create_module('allocationform', ['course' => $course->id]);
        $option3 = $generator->create_option($otherallocation);
        $option4 = $generator->create_option($otherallocation);
        $generator->create_user_choices($otherallocation, $user, ['choice1' => $option4->id]);
        $generator->create_allocation($otherallocation, $user, $option4);
        $generator->create_restriction($otherallocation, $user, $option3);
        // Do the deletion, via the API.
        course_delete_module($allocation->cmid, false);
        // Test we deleted the target forms data.
        $this->assertFalse($DB->record_exists('allocationform', ['id' => $allocation->id]));
        $this->assertFalse($DB->record_exists('allocationform_options', ['formid' => $allocation->id]));
        $this->assertFalse($DB->record_exists('allocationform_choices', ['formid' => $allocation->id]));
        $this->assertFalse($DB->record_exists('allocationform_allocations', ['formid' => $allocation->id]));
        $this->assertFalse($DB->record_exists('allocationform_disallow', ['formid' => $allocation->id]));
        $eventtype = activity::EVENT_DEADLINE;
        $eventparams = ['modulename' => 'allocationform', 'instance' => $allocation->id, 'eventtype' => $eventtype];
        $this->assertFalse($DB->record_exists('event', $eventparams));
        // Test the other form is still there.
        $this->assertTrue($DB->record_exists('allocationform', ['id' => $otherallocation->id]));
        $this->assertTrue($DB->record_exists('allocationform_options', ['formid' => $otherallocation->id]));
        $this->assertTrue($DB->record_exists('allocationform_choices', ['formid' => $otherallocation->id]));
        $this->assertTrue($DB->record_exists('allocationform_allocations', ['formid' => $otherallocation->id]));
        $this->assertTrue($DB->record_exists('allocationform_disallow', ['formid' => $otherallocation->id]));
        $event2params = ['modulename' => 'allocationform', 'instance' => $otherallocation->id, 'eventtype' => $eventtype];
        $this->assertTrue($DB->record_exists('event', $event2params));
    }

    /**
     * Tests that we can get a list of participants correctly.
     */
    public function test_get_participants() {
        $generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $user1 = self::getDataGenerator()->create_user(); // Made a choice and allocated.
        $user2 = self::getDataGenerator()->create_user(); // Made a choice, not allocated.
        $user3 = self::getDataGenerator()->create_user(); // Allocated, no choice.
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($user2->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($user3->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        // Create a form that will be counted.
        $allocation = self::getDataGenerator()->create_module('allocationform', ['course' => $course->id]);
        $option1 = $generator->create_option($allocation);
        $option2 = $generator->create_option($allocation);
        $generator->create_user_choices($allocation, $user1, ['choice1' => $option2->id]);
        $generator->create_user_choices($allocation, $user2, ['choice1' => $option1->id]);
        $generator->create_allocation($allocation, $user1, $option2);
        $generator->create_allocation($allocation, $user3, $option1);
        $generator->create_restriction($allocation, $user1, $option1);
        // Create a form that should not be counted.
        $otherallocation = self::getDataGenerator()->create_module('allocationform', ['course' => $course->id]);
        $option3 = $generator->create_option($otherallocation);
        $option4 = $generator->create_option($otherallocation);
        $generator->create_user_choices($otherallocation, $user1, ['choice1' => $option4->id]);
        $generator->create_allocation($otherallocation, $user1, $option4);
        $generator->create_restriction($otherallocation, $user1, $option3);
        $generator->create_user_choices($otherallocation, $otheruser, ['choice1' => $option4->id]);
        $generator->create_allocation($otherallocation, $otheruser, $option4);
        // Get the participants.
        $participants = activity::get_participants($allocation->id);
        // Test we got the correct users.
        $this->assertCount(2, $participants);
        $this->assertArrayHasKey($user1->id, $participants);
        $this->assertArrayHasKey($user2->id, $participants);
    }

    /**
     * Tests updating an allocation form.
     *
     * @global \moodle_database $DB
     */
    public function test_update() {
        global $DB;
        $course = self::getDataGenerator()->create_course();
        $allocation = self::getDataGenerator()->create_module('allocationform', ['course' => $course->id]);
        $this->waitForSecond();
        $time = time();
        $record = clone $allocation;
        unset($record->timecreated);
        unset($record->timemodified);
        unset($record->processed);
        unset($record->state);
        $record->coursemodule = $record->cmid;
        $record->visible = true;
        unset($record->cmid);
        $newname = 'Modified allocation form';
        $record->name = $newname;
        $record->deadline += HOURSECS;
        // Update the activity.
        $this->assertTrue(activity::update($record));
        // Test that the record changed.
        $expected = clone $allocation;
        unset($expected->timemodified);
        unset($expected->cmid);
        $expected->name = $newname;
        $expected->deadline = $record->deadline;
        $testfields = 'id,course,name,intro,introformat,timecreated,numberofchoices,notwant,numberofallocations,startdate,deadline,'
                . 'roleid,trackcompletion,processed,state';
        $this->assertEquals($expected, $DB->get_record('allocationform', ['id' => $allocation->id], $testfields));
        $modified = $DB->get_field('allocationform', 'timemodified', ['id' => $allocation->id]);
        $this->assertGreaterThanOrEqual($time, $modified);
        $this->assertLessThan($modified, $DB->get_field('allocationform', 'timecreated', ['id' => $allocation->id]));
        $eventparams = array(
            'modulename' => 'allocationform',
            'instance' => $allocation->id,
            'eventtype' => activity::EVENT_DEADLINE,
            'timestart' => $record->deadline,
        );
        $this->assertTrue($DB->record_exists('event', $eventparams));
    }

    /**
     * Data for testing \mod_allocationform\activity::user_can_choose.
     *
     * @return array
     */
    public function data_user_can_choose() {
        $editing = helper::STATE_EDITING;
        $ready = helper::STATE_READY;
        $now = time();
        $yesterday = $now - DAYSECS;
        $tomorrow = $now + DAYSECS;
        $nextweek = $now + WEEKSECS;
        $lastsec = $now - 1;
        return array(
            'open_now' => ['student', 'student', $yesterday, $tomorrow, $ready, true],
            'editing_state' => ['student', 'student', $yesterday, $tomorrow, $editing, false],
            'recently_closed' => ['student', 'student', $yesterday, $lastsec, $ready, false],
            'opens_in_future' => ['student', 'student', $tomorrow, $nextweek, $ready, false],
            'different_role' => ['student', 'teacher', $yesterday, $tomorrow, $ready, false],
        );
    }

    /**
     * Tests that we correctly detect if a user can make a choice on an allocation form.
     *
     * @global \moodle_database $DB
     * @param string $userrole The shortname of the role the user has.
     * @param string $allocationrole The shortname of the role the allocation form will use.
     * @param int $start Timestamp for when the user can start making a choice.
     * @param int $deadline Timestamp for when a user has to make a choice.
     * @param int $state The state the form is in.
     * @param bool $expected The expected response from \mod_allocationform\activity::user_can_choose.
     *
     * @dataProvider data_user_can_choose
     */
    public function test_user_can_choose(string $userrole, string $allocationrole, int $start, int $deadline, int $state,
                                         bool $expected) {
        global $DB;
        $generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $user = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, $userrole);
        $allocationparams = array(
            'course' => $course->id,
            'state' => $state,
            'roleid' => $DB->get_field('role', 'id', ['shortname' => $allocationrole]),
            'startdate' => $start,
            'deadline' => $deadline,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $cm = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        $this->assertEquals($expected, activity::user_can_choose($user->id, $cm));
    }

    /**
     * Data for testing \mod_allocationform\activity::user_can_review.
     *
     * @return array
     */
    public function data_user_can_review() {
        return array(
            'editing_teacher' => ['editingteacher', helper::STATE_REVIEW, true],
            'teacher' => ['teacher', helper::STATE_REVIEW, true],
            'student' => ['student', helper::STATE_REVIEW, false],
            'editing' => ['editingteacher', helper::STATE_EDITING, false],
            'ready' => ['editingteacher', helper::STATE_READY, false],
            'processing' => ['editingteacher', helper::STATE_PROCESS, false],
            'processed' => ['editingteacher', helper::STATE_PROCESSED, false],
        );
    }

    /**
     * Tests that we can correctly identify users who should be able to review the allocations.
     *
     * @param string $userrole The shortname of the role the user has.
     * @param int $state The state the form is in
     * @param bool $expected The expected response from \mod_allocationform\activity::user_can_review.
     *
     * @dataProvider data_user_can_review
     */
    public function test_user_can_review(string $userrole, int $state, bool $expected) {
        $generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $user = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, $userrole);
        $allocationparams = array(
            'course' => $course->id,
            'state' => $state,
            'startdate' => time() - WEEKSECS,
            'deadline' => time() - HOURSECS,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $cm = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        $this->assertEquals($expected, activity::user_can_review($user->id, $cm));
    }

    /**
     * Tests that user data is reset correctly.
     *
     * @global \moodle_database $DB
     */
    public function test_reset_user_data() {
        global $DB;
        $generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $user = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $othercourse = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($user->id, $othercourse->id, 'student');
        $allocated = helper::STATE_PROCESSED;
        // Create a form that will be deleted.
        $allocationparams = ['course' => $course->id, 'state' => $allocated];
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $option1 = $generator->create_option($allocation);
        $generator->create_user_choices($allocation, $user, ['choice1' => $option1->id]);
        $generator->create_allocation($allocation, $user, $option1);
        $generator->create_restriction($allocation, $user, $option1);
        // Create a form that will not be deleted.
        $otherallocationparams = ['course' => $othercourse->id, 'state' => $allocated];
        $otherallocation = self::getDataGenerator()->create_module('allocationform', $otherallocationparams);
        $option2 = $generator->create_option($otherallocation);
        $generator->create_user_choices($otherallocation, $user, ['choice1' => $option2->id]);
        $generator->create_allocation($otherallocation, $user, $option2);
        $generator->create_restriction($otherallocation, $user, $option2);
        // Do the reset.
        $data = (object) array(
            'courseid' => $course->id,
            'reset_allocation_state' => 1,
            'reset_allocation_userdata' => 1,
            'timeshift' => 0,
        );
        activity::reset_user_data($data);
        // Check the reset happened in the target course.
        $this->assertFalse($DB->record_exists('allocationform_choices', ['formid' => $allocation->id]));
        $this->assertFalse($DB->record_exists('allocationform_allocations', ['formid' => $allocation->id]));
        $this->assertFalse($DB->record_exists('allocationform_disallow', ['formid' => $allocation->id]));
        $editing = helper::STATE_EDITING;
        $this->assertEquals($editing, $DB->get_field('allocationform', 'state', ['id' => $allocation->id]));
        // Check the form in the other course has not been reset.
        $this->assertTrue($DB->record_exists('allocationform_choices', ['formid' => $otherallocation->id]));
        $this->assertTrue($DB->record_exists('allocationform_allocations', ['formid' => $otherallocation->id]));
        $this->assertTrue($DB->record_exists('allocationform_disallow', ['formid' => $otherallocation->id]));
        $this->assertEquals($allocated, $DB->get_field('allocationform', 'state', ['id' => $otherallocation->id]));
    }

    /**
     * Data for testing \mod_allocationform\activity::user_can_be_allocated.
     *
     * @return array
     */
    public function data_user_can_be_allocated() {
        return array(
            'student:student' => ['student', 'student', true],
            'student:teacher' => ['student', 'teacher', false],
            'teacher:teacher' => ['teacher', 'teacher', true],
            'teacher:student' => ['teacher', 'student', false],
        );
    }

    /**
     * Tests if we detect that a user can be allocated correctly.
     *
     * @gloabl \moodle_database $DB
     * @param string $userrole The shortname of the role the user has.
     * @param bool $expected The expected response
     *
     * @dataProvider data_user_can_be_allocated
     */
    public function test_user_can_be_allocated(string $userrole, string $formrole, bool $expected) {
        global $DB;
        $user = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, $userrole);
        $roleid = $DB->get_field('role', 'id', ['shortname' => $formrole]);
        $allocationparams = array(
            'course' => $course->id,
            'roleid' => $roleid,
            'startdate' => time() - WEEKSECS,
            'deadline' => time() - HOURSECS,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $cm = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        $this->assertEquals($expected, activity::user_can_be_allocated($user->id, $cm));
    }

    /**
     * Data for testing \mod_allocationform\activity::user_can_edit.
     *
     * @return array
     */
    public function data_user_can_edit() {
        return array(
            'student' => ['student', false],
            'teacher' => ['teacher', false],
            'editingteacher' => ['editingteacher', true],
            'manager' => ['manager', true],
        );
    }

    /**
     * Test is a user is detected as being able to edit correctly.
     *
     * @param string $userrole The shortname of the role the user has.
     * @param bool $expected The expected response
     *
     * @dataProvider data_user_can_edit
     */
    public function test_user_can_edit(string $userrole, bool $expected) {
        $user = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, $userrole);
        $allocationparams = array(
            'course' => $course->id,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $cm = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        $this->assertEquals($expected, activity::user_can_edit($user->id, $cm));
    }

    /**
     * Tests that we can find the options a user can select correctly.
     */
    public function test_get_valid_choices() {
        $generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $user3 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($user2->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($user3->id, $course->id, 'student');
        $allocationparams = array(
            'course' => $course->id,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $option1 = $generator->create_option($allocation);
        $option2 = $generator->create_option($allocation);
        $generator->create_restriction($allocation, $user2, $option1);
        $generator->create_restriction($allocation, $user3, $option2);
        $expected1 = array(
            $option1->id => (object) ['id' => $option1->id, 'name' => $option1->name],
            $option2->id => (object) ['id' => $option2->id, 'name' => $option2->name],
        );
        $this->assertEquals($expected1, activity::get_valid_choices($user1->id, $allocation->id));
        $expected2 = array(
            $option2->id => (object) ['id' => $option2->id, 'name' => $option2->name],
        );
        $this->assertEquals($expected2, activity::get_valid_choices($user2->id, $allocation->id));
        $expected3 = array(
            $option1->id => (object) ['id' => $option1->id, 'name' => $option1->name],
        );
        $this->assertEquals($expected3, activity::get_valid_choices($user3->id, $allocation->id));
    }

    /**
     * Tests that a form in the ready state where the deadline has passed has it's state changed.
     */
    public function test_automatic_state_change_ready_deadline_passed() {
        $course = self::getDataGenerator()->create_course();
        $allocationparams = array(
            'course' => $course->id,
            'state' => helper::STATE_READY,
            'startdate' => time() - WEEKSECS,
            'deadline' => time() - 1,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $cm = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        $this->assertTrue(activity::automatic_state_change($cm));
        $cmafter = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        $this->assertEquals(helper::STATE_PROCESS, $cmafter->customdata['state']);
    }

    /**
     * Data for testing \mod_allocationform\activity::automatic_state_change
     *
     * @return array
     */
    public function data_automatic_state_no_change() {
        $past = time() - 1;
        $future = time() + DAYSECS;
        return array(
            'editing past' => [helper::STATE_EDITING, $past],
            'editing future' => [helper::STATE_EDITING, $future],
            'ready future' => [helper::STATE_READY, $future],
            'process past' => [helper::STATE_PROCESS, $past],
            'process future' => [helper::STATE_PROCESS, $future],
            'review past' => [helper::STATE_REVIEW, $past],
            'review future' => [helper::STATE_REVIEW, $future],
            'processed past' => [helper::STATE_PROCESSED, $past],
            'processed future' => [helper::STATE_PROCESSED, $future],
        );
    }

    /**
     * Tests that the form state will not be changed unexpectedly.
     *
     * @param int $state The state of the allocation form.
     * @param int $deadline Timestamp for the deadline.
     *
     * @dataProvider data_automatic_state_no_change
     */
    public function test_automatic_state_no_change(int $state, int $deadline) {
        $course = self::getDataGenerator()->create_course();
        $allocationparams = array(
            'course' => $course->id,
            'state' => $state,
            'startdate' => time() - WEEKSECS,
            'deadline' => $deadline,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $cm = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        $this->assertFalse(activity::automatic_state_change($cm));
        $cmafter = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        $this->assertEquals($state, $cmafter->customdata['state']);
    }
}

<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing unit tests for the mod_allocationform_init class.
 *
 * @package   mod_allocationform
 * @copyright 2019 University of Nottingham
 * @author    Neill Magill <neill.magill@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform;

use Exception;
use mod_allocationform\output\allallocations;

defined('MOODLE_INTERNAL') || die();

/**
 * Unit tests for the mod_allocationform_init class.
 *
 * @package   mod_allocationform
 * @copyright 2019 University of Nottingham
 * @author    Neill Magill <neill.magill@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_allocationform
 * @group uon
 */
class init_test extends \advanced_testcase {
    /** @var \mod_allocationform_generator The allocation form data generator. */
    protected $generator;

    /**
     * Setup for each test.
     */
    public function setUp(): void {
        parent::setUp();
        $this->generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $this->resetAfterTest(true);
    }

    /**
     * Run at the end of each test.
     */
    public function tearDown(): void {
        $this->assertDebuggingNotCalled();
        parent::tearDown();
    }

    /**
     * Converts a recordset into an array.
     *
     * @param moodle_recordset $recordset
     * @return array
     */
    protected function recordset_to_array(moodle_recordset $recordset) {
        $returnarray = [];
        foreach ($recordset as $key => $record) {
            $returnarray[$key] = $record;
        }
        return $returnarray;
    }

    /**
     * Test that allocating users works correctly.
     *
     * In this scenario all the users have made a choice and they will all get their choice.
     *
     * @global moodle_database $DB
     */
    public function test_allocate_all() {
        global $DB;
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($user2->id, $course->id, 'student');
        $allocationparams = array(
            'course' => $course->id,
            'state' => helper::STATE_PROCESS,
            'numberofchoices' => 1,
            'numberofallocations' => 1,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $option1 = $this->generator->create_option($allocation);
        $option2 = $this->generator->create_option($allocation);
        $this->generator->create_user_choices($allocation, $user1, ['choice1' => $option2->id]);
        $this->generator->create_user_choices($allocation, $user2, ['choice1' => $option1->id]);
        $init = new init($allocation);
        $init->allocate();
        $this->assertEquals(2, $DB->count_records('allocationform_allocations', ['formid' => $allocation->id]));
        $params1 = ['formid' => $allocation->id, 'userid' => $user1->id, 'allocation' => $option2->id];
        $this->assertTrue($DB->record_exists('allocationform_allocations', $params1));
        $params2 = ['formid' => $allocation->id, 'userid' => $user2->id, 'allocation' => $option1->id];
        $this->assertTrue($DB->record_exists('allocationform_allocations', $params2));
    }

    /**
     * Test that allocating users works correctly.
     *
     * In this scenario we test that a user who has made a choice will be allocated it before a user who has not made a choice.
     *
     * @global moodle_database $DB
     */
    public function test_allocate_no_choice() {
        global $DB;
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $allocationparams = array(
            'course' => $course->id,
            'state' => helper::STATE_PROCESS,
            'numberofchoices' => 1,
            'numberofallocations' => 1,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $option1 = $this->generator->create_option($allocation, ['maxallocation' => 1]);
        $option2 = $this->generator->create_option($allocation, ['maxallocation' => 1]);
        $this->generator->create_user_choices($allocation, $user, ['choice1' => $option1->id]);
        $init = new init($allocation);
        $init->allocate();
        $this->assertEquals(2, $DB->count_records('allocationform_allocations', ['formid' => $allocation->id]));
        $params1 = ['formid' => $allocation->id, 'userid' => $user->id, 'allocation' => $option1->id];
        $this->assertTrue($DB->record_exists('allocationform_allocations', $params1));
        $params2 = ['formid' => $allocation->id, 'userid' => $otheruser->id, 'allocation' => $option2->id];
        $this->assertTrue($DB->record_exists('allocationform_allocations', $params2));
    }

    /**
     * Test that allocating users works correctly.
     *
     * In this scenario we test that if an option is oversubscribed that only a user who chooses it gets in the slot,
     * and that the user who does not get it will be given an alternative slot.
     *
     * @global \moodle_database $DB
     */
    public function test_allocate_oversubscription() {
        global $DB;
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($user2->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $allocationparams = array(
            'course' => $course->id,
            'state' => helper::STATE_PROCESS,
            'numberofchoices' => 1,
            'numberofallocations' => 1,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $option1 = $this->generator->create_option($allocation, ['maxallocation' => 1]);
        $option2 = $this->generator->create_option($allocation, ['maxallocation' => 2]);
        $this->generator->create_user_choices($allocation, $user1, ['choice1' => $option1->id]);
        $this->generator->create_user_choices($allocation, $user2, ['choice1' => $option1->id]);
        $init = new init($allocation);
        $init->allocate();
        $this->assertEquals(3, $DB->count_records('allocationform_allocations', ['formid' => $allocation->id]));
        $params1 = ['formid' => $allocation->id, 'allocation' => $option1->id];
        $this->assertEquals(1, $DB->count_records('allocationform_allocations', $params1));
        $params2 = ['formid' => $allocation->id, 'allocation' => $option2->id];
        $this->assertEquals(2, $DB->count_records('allocationform_allocations', $params2));
        $params3 = ['formid' => $allocation->id, 'userid' => $otheruser->id, 'allocation' => $option1->id];
        $this->assertFalse($DB->record_exists('allocationform_allocations', $params3));
    }

    /**
     * Test that allocating users works correctly.
     *
     * In this scenario we test that a user restricted from option 1 is not allocated to it.
     *
     * @global \moodle_database $DB
     */
    public function test_allocate_restriction() {
        global $DB;
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $allocationparams = array(
            'course' => $course->id,
            'state' => helper::STATE_PROCESS,
            'numberofchoices' => 1,
            'numberofallocations' => 1,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $option1 = $this->generator->create_option($allocation, ['maxallocation' => 1]);
        $option2 = $this->generator->create_option($allocation, ['maxallocation' => 2]);
        $this->generator->create_restriction($allocation, $user, $option1);
        $init = new init($allocation);
        $init->allocate();
        $this->assertEquals(2, $DB->count_records('allocationform_allocations', ['formid' => $allocation->id]));
        $params1 = ['formid' => $allocation->id, 'userid' => $user->id, 'allocation' => $option2->id];
        $this->assertTrue($DB->record_exists('allocationform_allocations', $params1));
        $params2 = ['formid' => $allocation->id, 'userid' => $otheruser->id];
        $this->assertTrue($DB->record_exists('allocationform_allocations', $params2));
    }

    /**
     * Test that allocating users works correctly.
     *
     * In this scenario we test that a user is not put into a slot they actively do not wish to be.
     *
     * @global \moodle_database $DB
     */
    public function test_allocate_notwant() {
        global $DB;
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($user2->id, $course->id, 'student');
        $allocationparams = array(
            'course' => $course->id,
            'state' => helper::STATE_PROCESS,
            'numberofchoices' => 1,
            'numberofallocations' => 1,
            'notwant' => 1,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $cm = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        $option1 = $this->generator->create_option($allocation, ['maxallocation' => 1]);
        $option2 = $this->generator->create_option($allocation, ['maxallocation' => 1]);
        $this->generator->create_user_choices($allocation, $user1, ['choice1' => $option1->id, 'notwant' => $option2->id]);
        $this->generator->create_user_choices($allocation, $user2, ['choice1' => $option1->id, 'notwant' => $option2->id]);
        $init = new init($allocation);
        $init->allocate();
        // Only one of the users can be allocated a space.
        $this->assertEquals(1, $DB->count_records('allocationform_allocations', ['formid' => $allocation->id]));
        $params1 = ['formid' => $allocation->id, 'allocation' => $option1->id];
        $this->assertEquals(1, $DB->count_records('allocationform_allocations', $params1));
        $params2 = ['formid' => $allocation->id, 'allocation' => $option2->id];
        $this->assertEquals(0, $DB->count_records('allocationform_allocations', $params2));
        $renderable = allallocations::get($cm);
        $this->assertCount(1, $renderable->unallocated);
    }

    /**
     * Test that allocating users works correctly.
     *
     * In this scenario we test that when a user does not make a choice they will be allocated places last (in this case
     * since there are not enough slots for everyone they should not be allocated)
     *
     * @global \moodle_database $DB
     */
    public function test_allocate_no_enough_places() {
        global $DB;
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($user2->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $allocationparams = array(
            'course' => $course->id,
            'state' => helper::STATE_PROCESS,
            'numberofchoices' => 1,
            'numberofallocations' => 1,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $cm = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        $option1 = $this->generator->create_option($allocation, ['maxallocation' => 1]);
        $option2 = $this->generator->create_option($allocation, ['maxallocation' => 1]);
        $this->generator->create_user_choices($allocation, $user1, ['choice1' => $option1->id]);
        $this->generator->create_user_choices($allocation, $user2, ['choice1' => $option1->id]);
        $init = new init($allocation);
        $init->allocate();
        $this->assertEquals(2, $DB->count_records('allocationform_allocations', ['formid' => $allocation->id]));
        $params1 = ['formid' => $allocation->id, 'allocation' => $option1->id];
        $this->assertEquals(1, $DB->count_records('allocationform_allocations', $params1));
        $params2 = ['formid' => $allocation->id, 'allocation' => $option2->id];
        $this->assertEquals(1, $DB->count_records('allocationform_allocations', $params2));
        $params3 = ['formid' => $allocation->id, 'userid' => $otheruser->id, 'allocation' => $option1->id];
        $this->assertFalse($DB->record_exists('allocationform_allocations', $params3));
        $renderable = allallocations::get($cm);
        $this->assertCount(1, $renderable->unallocated);
        $this->assertArrayHasKey($otheruser->id, $renderable->unallocated);
    }

    /**
     * Test that allocating users works correctly.
     *
     * In this scenario we test that ranking multiple options are considered.
     *
     * @global \moodle_database $DB
     */
    public function test_allocate_ranking() {
        global $DB;
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($user2->id, $course->id, 'student');
        $allocationparams = array(
            'course' => $course->id,
            'state' => helper::STATE_PROCESS,
            'numberofchoices' => 2,
            'numberofallocations' => 1,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $option1 = $this->generator->create_option($allocation, ['maxallocation' => 1]);
        $option2 = $this->generator->create_option($allocation, ['maxallocation' => 1]);
        $this->generator->create_option($allocation, ['maxallocation' => 1]);
        $this->generator->create_user_choices($allocation, $user1, ['choice1' => $option1->id, 'choice2' => $option2->id]);
        $this->generator->create_user_choices($allocation, $user2, ['choice1' => $option1->id, 'choice2' => $option2->id]);
        $init = new init($allocation);
        $init->allocate();
        // No users should be allocated tothe third option.
        $this->assertEquals(2, $DB->count_records('allocationform_allocations', ['formid' => $allocation->id]));
        $params1 = ['formid' => $allocation->id, 'allocation' => $option1->id];
        $this->assertEquals(1, $DB->record_exists('allocationform_allocations', $params1));
        $params2 = ['formid' => $allocation->id, 'allocation' => $option2->id];
        $this->assertEquals(1, $DB->record_exists('allocationform_allocations', $params2));
    }

    /**
     * Test that allocating users works correctly.
     *
     * In this scenario we test allocation to multiple slots works.
     *
     * @global \moodle_database $DB
     */
    public function test_allocate_multiple_allocation() {
        global $DB;
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($user2->id, $course->id, 'student');
        $allocationparams = array(
            'course' => $course->id,
            'state' => helper::STATE_PROCESS,
            'numberofchoices' => 2,
            'numberofallocations' => 2,
        );
        $allocation = self::getDataGenerator()->create_module('allocationform', $allocationparams);
        $option1 = $this->generator->create_option($allocation, ['maxallocation' => 1]);
        $option2 = $this->generator->create_option($allocation, ['maxallocation' => 2]);
        $option3 = $this->generator->create_option($allocation, ['maxallocation' => 2]);
        $this->generator->create_user_choices($allocation, $user1, ['choice1' => $option1->id, 'choice2' => $option2->id]);
        $this->generator->create_user_choices($allocation, $user2, ['choice1' => $option1->id, 'choice2' => $option2->id]);
        $init = new init($allocation);
        $init->allocate();
        // No users should be allocated tothe third option.
        $this->assertEquals(4, $DB->count_records('allocationform_allocations', ['formid' => $allocation->id]));
        $params1 = ['formid' => $allocation->id, 'allocation' => $option1->id];
        $this->assertEquals(1, $DB->record_exists('allocationform_allocations', $params1));
        $params2 = ['formid' => $allocation->id, 'allocation' => $option2->id];
        $this->assertEquals(2, $DB->record_exists('allocationform_allocations', $params2));
        $params3 = ['formid' => $allocation->id, 'allocation' => $option3->id];
        $this->assertEquals(1, $DB->record_exists('allocationform_allocations', $params3));
    }

    /**
     * All the state changes that should happen.
     *
     * @return array
     */
    public function data_valid_state_changes() {
        return array(
            'to ready' => [helper::STATE_EDITING, helper::STATE_READY],
            'deadline passed' => [helper::STATE_READY, helper::STATE_PROCESS],
            'back to editing' => [helper::STATE_READY, helper::STATE_EDITING],
            'allocation done' => [helper::STATE_PROCESS, helper::STATE_REVIEW],
            'release to students' => [helper::STATE_REVIEW, helper::STATE_READY],
            'retry allocation' => [helper::STATE_REVIEW, helper::STATE_PROCESS],
            'out of avaliable' => [helper::STATE_PROCESSED, helper::STATE_PROCESS],
        );
    }

    /**
     * Test that the state changes for normal combinations.
     *
     * @global \moodle_database $DB
     * @param int $initial
     * @param int $final
     * @dataProvider data_valid_state_changes
     */
    public function test_change_state($initial, $final) {
        global $DB;
        $course = self::getDataGenerator()->create_course();
        $allocation = self::getDataGenerator()->create_module('allocationform', ['course' => $course->id, 'state' => $initial]);
        $this->generator->create_option($allocation);
        $this->generator->create_option($allocation);
        $startsate = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id]->customdata['state'];
        $this->assertEquals($initial, $startsate);
        $init = new init($allocation);
        $init->change_state($final);
        $finalsate = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id]->customdata['state'];
        $this->assertEquals($final, $DB->get_field('allocationform', 'state', ['id' => $allocation->id]));
        $this->assertEquals($final, $init->get_state());
        $this->assertEquals($final, $finalsate);
    }

    /**
     * Tests that no change occurs if an invalid state is passed.
     *
     * @global \moodle_database $DB
     */
    public function test_change_state_inavlid() {
        global $DB;
        $course = self::getDataGenerator()->create_course();
        $state = helper::STATE_EDITING;
        $allocation = self::getDataGenerator()->create_module('allocationform', ['course' => $course->id, 'state' => $state]);
        $this->generator->create_option($allocation);
        $this->generator->create_option($allocation);
        $init = new init($allocation);
        try {
            $init->change_state(-1); // We will never use a negative value for the state.
            $this->fail('No exception was thrown.');
        } catch (Exception $e) {
            // The exception is required for the test to pass.
        }
        $this->assertEquals($state, $DB->get_field('allocationform', 'state', ['id' => $allocation->id]));
        $this->assertEquals($state, $init->get_state());
    }

    /**
     * Tests that when a state is changed back to editing mode all choices for the form are deleted.
     *
     * @global \moodle_database $DB
     */
    public function test_change_state_to_editing() {
        global $DB;
        $user = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $state = helper::STATE_READY;
        $allocation = self::getDataGenerator()->create_module('allocationform', ['course' => $course->id, 'state' => $state]);
        $option = $this->generator->create_option($allocation);
        $this->generator->create_option($allocation);
        $this->generator->create_user_choices($allocation, $user, ['choice1' => $option->id]);
        $init = new init($allocation);
        $init->change_state(helper::STATE_EDITING);
        $this->assertEquals(0, $DB->count_records('allocationform_choices', ['formid' => $allocation->id]));
    }

    /**
     * Tests that when an allocation form has it's results released an event is created.
     *
     * @global \moodle_database $DB
     */
    public function test_change_state_to_avaliable() {
        global $DB;
        $user = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        $state = helper::STATE_REVIEW;
        $allocation = self::getDataGenerator()->create_module('allocationform', ['course' => $course->id, 'state' => $state]);
        $option = $this->generator->create_option($allocation);
        $this->generator->create_option($allocation);
        $this->generator->create_user_choices($allocation, $user, ['choice1' => $option->id]);
        $this->generator->create_allocation($allocation, $user, $option);
        $init = new init($allocation);
        $init->change_state(helper::STATE_PROCESSED);
        // There should now be a calendar event for the activity.
        $select = "modulename = :modulename
                   AND instance = :instance
                   AND eventtype = :eventtype
                   AND groupid = 0
                   AND courseid <> 0";
        $eventtype = activity::EVENT_AVALIABLE;
        $params = array('modulename' => 'allocationform', 'instance' => $allocation->id, 'eventtype' => $eventtype);
        $this->assertTrue($DB->record_exists_select('event', $select, $params));
    }

    /**
     * Tests that if an allocation form is changed from the released state the available event is removed.
     *
     * @global \moodle_database $DB
     */
    public function test_change_state_from_avaliable() {
        global $DB;
        $user = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        $state = helper::STATE_PROCESSED;
        $allocation = self::getDataGenerator()->create_module('allocationform', ['course' => $course->id, 'state' => $state]);
        $option = $this->generator->create_option($allocation);
        $this->generator->create_option($allocation);
        $this->generator->create_user_choices($allocation, $user, ['choice1' => $option->id]);
        $this->generator->create_allocation($allocation, $user, $option);
        $cm = get_fast_modinfo($course)->get_instances_of('allocationform')[$allocation->id];
        activity::create_avaliable_event($cm);
        $init = new init($allocation);
        $init->change_state(helper::STATE_PROCESS);
        // There should no longer be a calendar event for the activity.
        $select = "modulename = :modulename
                   AND instance = :instance
                   AND eventtype = :eventtype
                   AND groupid = 0
                   AND courseid <> 0";
        $eventtype = activity::EVENT_AVALIABLE;
        $params = array('modulename' => 'allocationform', 'instance' => $allocation->id, 'eventtype' => $eventtype);
        $this->assertFalse($DB->record_exists_select('event', $select, $params));
    }
}

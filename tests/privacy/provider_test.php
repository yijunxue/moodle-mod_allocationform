<?php
// This file is part of the Allocation form activity.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests the allocation form Privacy API implementation.
 *
 * @package     mod_allocationform
 * @copyright   University of Nottingham, 2018
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\privacy;

defined('MOODLE_INTERNAL') || die();

/**
 * Tests the allocation form privacy provider class.
 *
 * @package     mod_allocationform
 * @copyright   University of Nottingham, 2018
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_allocationform
 * @group uon
 */
class provider_test extends \core_privacy\tests\provider_testcase {
    /** @var \mod_allocationform_generator The allocation form data generator. */
    protected $generator;

    /**
     * Setup for each test.
     */
    public function setUp(): void {
        parent::setUp();
        $this->generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $this->resetAfterTest(true);
    }

    /**
     * Run at the end of each test.
     */
    public function tearDown(): void {
        $this->assertDebuggingNotCalled();
        parent::tearDown();
    }

    /**
     * A user who has made no choices and has had no rescrictions placed on them should not return any data.
     */
    public function test_user_with_no_choices_or_rescrictions() {
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $allocation = $this->getDataGenerator()->create_module('allocationform', array('course' => $course->id));
        $option1 = $this->generator->create_option($allocation);
        $option2 = $this->generator->create_option($allocation);
        $this->generator->create_user_choices($allocation, $otheruser, ['choice1' => $option1->id]);
        $this->generator->create_restriction($allocation, $otheruser, $option2);
        $this->generator->create_allocation($allocation, $otheruser, $option1);
        // Test no contexts are retrived.
        $contextlist = $this->get_contexts_for_userid($user->id, 'mod_allocationform');
        $contexts = $contextlist->get_contextids();
        $this->assertCount(0, $contexts);
    }

    /**
     * A user who has not made any choices, but has restrictions should get data for the restrictions.
     */
    public function test_user_with_only_restrictions() {
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $allocation = $this->getDataGenerator()->create_module('allocationform', array('course' => $course->id));
        $option1 = $this->generator->create_option($allocation);
        $option2 = $this->generator->create_option($allocation);
        $this->generator->create_user_choices($allocation, $otheruser, ['choice1' => $option1->id]);
        $this->generator->create_restriction($allocation, $user, $option1);
        $this->generator->create_restriction($allocation, $otheruser, $option2);
        $this->generator->create_allocation($allocation, $otheruser, $option1);

        // Test that contexts were retrieved.
        $contextlist = $this->get_contexts_for_userid($user->id, 'mod_allocationform');
        $contexts = $contextlist->get_contextids();
        $this->assertCount(1, $contexts);

        $cm = get_coursemodule_from_instance('allocationform', $allocation->id);
        $context = \context_module::instance($cm->id);
        $this->assertEquals($context, $contextlist->current());

        // Test export.
        $this->export_context_data_for_user($user->id, $context, 'mod_allocationform');
        $writer = \core_privacy\local\request\writer::with_context($context);
        $this->assertTrue($writer->has_any_data());
        $restrictioncontext = [get_string('privacy:export:restrictions', 'mod_allocationform')];
        $this->assertTrue($writer->has_any_data($restrictioncontext));
        $choicecontext = [get_string('privacy:export:choices', 'mod_allocationform')];
        $this->assertEmpty($writer->get_data($choicecontext));
        $allocationcontext = [get_string('privacy:export:allocations', 'mod_allocationform')];
        $this->assertFalse($writer->has_any_data($allocationcontext));
    }

    /**
     * A user who has made choices, but before the allocation process has happened.
     */
    public function test_user_with_no_allocations() {
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $allocation = $this->getDataGenerator()->create_module('allocationform', array('course' => $course->id));
        $option1 = $this->generator->create_option($allocation);
        $option2 = $this->generator->create_option($allocation);
        $this->generator->create_user_choices($allocation, $user, ['choice1' => $option2->id]);
        $this->generator->create_user_choices($allocation, $otheruser, ['choice1' => $option1->id]);
        $this->generator->create_restriction($allocation, $user, $option1);
        $this->generator->create_restriction($allocation, $otheruser, $option2);
        $this->generator->create_allocation($allocation, $otheruser, $option1);

        // Test that contexts were retrieved.
        $contextlist = $this->get_contexts_for_userid($user->id, 'mod_allocationform');
        $contexts = $contextlist->get_contextids();
        $this->assertCount(1, $contexts);

        $cm = get_coursemodule_from_instance('allocationform', $allocation->id);
        $context = \context_module::instance($cm->id);
        $this->assertEquals($context, $contextlist->current());

        // Test export.
        $this->export_context_data_for_user($user->id, $context, 'mod_allocationform');
        $writer = \core_privacy\local\request\writer::with_context($context);
        $this->assertTrue($writer->has_any_data());
        $restrictioncontext = [get_string('privacy:export:restrictions', 'mod_allocationform')];
        $this->assertTrue($writer->has_any_data($restrictioncontext));
        $choicecontext = [get_string('privacy:export:choices', 'mod_allocationform')];
        $this->assertNotEmpty($writer->get_data($choicecontext));
        $allocationcontext = [get_string('privacy:export:allocations', 'mod_allocationform')];
        $this->assertFalse($writer->has_any_data($allocationcontext));
    }

    /**
     * A user who has been allocated after making some choices.
     */
    public function test_allocated_user() {
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $allocation = $this->getDataGenerator()->create_module('allocationform', array('course' => $course->id));
        $option1 = $this->generator->create_option($allocation);
        $option2 = $this->generator->create_option($allocation);
        $this->generator->create_user_choices($allocation, $user, ['choice1' => $option2->id]);
        $this->generator->create_user_choices($allocation, $otheruser, ['choice1' => $option1->id]);
        $this->generator->create_restriction($allocation, $user, $option1);
        $this->generator->create_restriction($allocation, $otheruser, $option2);
        $this->generator->create_allocation($allocation, $user, $option2);
        $this->generator->create_allocation($allocation, $otheruser, $option1);

        // Test that contexts were retrieved.
        $contextlist = $this->get_contexts_for_userid($user->id, 'mod_allocationform');
        $contexts = $contextlist->get_contextids();
        $this->assertCount(1, $contexts);

        $cm = get_coursemodule_from_instance('allocationform', $allocation->id);
        $context = \context_module::instance($cm->id);
        $this->assertEquals($context, $contextlist->current());

        // Test export.
        $this->export_context_data_for_user($user->id, $context, 'mod_allocationform');
        $writer = \core_privacy\local\request\writer::with_context($context);
        $this->assertTrue($writer->has_any_data());
        $restrictioncontext = [get_string('privacy:export:restrictions', 'mod_allocationform')];
        $this->assertTrue($writer->has_any_data($restrictioncontext));
        $choicecontext = [get_string('privacy:export:choices', 'mod_allocationform')];
        $this->assertNotEmpty($writer->get_data($choicecontext));
        $allocationcontext = get_string('privacy:export:allocations', 'mod_allocationform');
        $this->assertTrue($writer->has_any_data([$allocationcontext]));
        $this->assertNotEmpty($writer->get_data([$allocationcontext, $option2->name]));
        $this->assertEmpty($writer->get_data([$allocationcontext, $option1->name]));
    }

    /**
     * A user who has been allocated after making no choices.
     */
    public function test_allocated_without_making_choice() {
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($user->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $allocation = $this->getDataGenerator()->create_module('allocationform', array('course' => $course->id));
        $option1 = $this->generator->create_option($allocation);
        $option2 = $this->generator->create_option($allocation);
        $this->generator->create_user_choices($allocation, $otheruser, ['choice1' => $option1->id]);
        $this->generator->create_restriction($allocation, $otheruser, $option2);
        $this->generator->create_allocation($allocation, $user, $option1);
        $this->generator->create_allocation($allocation, $user, $option2);
        $this->generator->create_allocation($allocation, $otheruser, $option1);

        // Test that contexts were retrieved.
        $contextlist = $this->get_contexts_for_userid($user->id, 'mod_allocationform');
        $contexts = $contextlist->get_contextids();
        $this->assertCount(1, $contexts);

        $cm = get_coursemodule_from_instance('allocationform', $allocation->id);
        $context = \context_module::instance($cm->id);
        $this->assertEquals($context, $contextlist->current());

        // Test export.
        $this->export_context_data_for_user($user->id, $context, 'mod_allocationform');
        $writer = \core_privacy\local\request\writer::with_context($context);
        $this->assertTrue($writer->has_any_data());
        $restrictioncontext = [get_string('privacy:export:restrictions', 'mod_allocationform')];
        $this->assertFalse($writer->has_any_data($restrictioncontext));
        $choicecontext = [get_string('privacy:export:choices', 'mod_allocationform')];
        $this->assertEmpty($writer->get_data($choicecontext));
        $allocationcontext = get_string('privacy:export:allocations', 'mod_allocationform');
        $this->assertTrue($writer->has_any_data([$allocationcontext]));
        $this->assertNotEmpty($writer->get_data([$allocationcontext, $option2->name]));
        $this->assertNotEmpty($writer->get_data([$allocationcontext, $option1->name]));
    }

    /**
     * Test that all users with data in the context can be found.
     */
    public function test_get_users_in_context() {
        $choiceuser = self::getDataGenerator()->create_user();
        $allocateduser = self::getDataGenerator()->create_user();
        $restricteduser = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($choiceuser->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($allocateduser->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($restricteduser->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $allocation = $this->getDataGenerator()->create_module('allocationform', array('course' => $course->id));
        $option1 = $this->generator->create_option($allocation);
        $option2 = $this->generator->create_option($allocation);
        $this->generator->create_user_choices($allocation, $choiceuser, ['choice1' => $option1->id]);
        $this->generator->create_restriction($allocation, $restricteduser, $option2);
        $this->generator->create_allocation($allocation, $allocateduser, $option1);
        $otherallocation = $this->getDataGenerator()->create_module('allocationform', array('course' => $course->id));
        $option3 = $this->generator->create_option($otherallocation);
        $option4 = $this->generator->create_option($otherallocation);
        $this->generator->create_user_choices($otherallocation, $otheruser, ['choice1' => $option3->id]);
        $this->generator->create_restriction($otherallocation, $otheruser, $option4);
        $this->generator->create_allocation($otherallocation, $otheruser, $option3);

        $cm = get_coursemodule_from_instance('allocationform', $allocation->id);
        $context = \context_module::instance($cm->id);

        // Run the test.
        $userlist = new \core_privacy\local\request\userlist($context, 'allocationform');
        provider::get_users_in_context($userlist);
        $userids = $userlist->get_userids();
        $this->assertCount(3, $userids);
        $this->assertTrue(in_array($choiceuser->id, $userids));
        $this->assertTrue(in_array($allocateduser->id, $userids));
        $this->assertTrue(in_array($restricteduser->id, $userids));
    }

    /**
     * Test that all data for a selected list of users in a context are deleted.
     *
     * @global \moodle_database $DB
     */
    public function test_delete_data_for_users() {
        global $DB;
        $choiceuser = self::getDataGenerator()->create_user();
        $allocateduser = self::getDataGenerator()->create_user();
        $restricteduser = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        self::getDataGenerator()->enrol_user($choiceuser->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($allocateduser->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($restricteduser->id, $course->id, 'student');
        self::getDataGenerator()->enrol_user($otheruser->id, $course->id, 'student');
        $allocation = $this->getDataGenerator()->create_module('allocationform', array('course' => $course->id));
        $option1 = $this->generator->create_option($allocation);
        $option2 = $this->generator->create_option($allocation);
        $this->generator->create_user_choices($allocation, $choiceuser, ['choice1' => $option1->id]);
        $this->generator->create_restriction($allocation, $restricteduser, $option2);
        $this->generator->create_allocation($allocation, $allocateduser, $option1);
        // None of this data should be deleted.
        $this->generator->create_user_choices($allocation, $otheruser, ['choice1' => $option2->id]);
        $this->generator->create_restriction($allocation, $otheruser, $option1);
        $this->generator->create_allocation($allocation, $otheruser, $option2);
        $otherallocation = $this->getDataGenerator()->create_module('allocationform', array('course' => $course->id));
        $option3 = $this->generator->create_option($otherallocation);
        $option4 = $this->generator->create_option($otherallocation);
        $this->generator->create_user_choices($otherallocation, $choiceuser, ['choice1' => $option3->id]);
        $this->generator->create_restriction($otherallocation, $restricteduser, $option4);
        $this->generator->create_allocation($otherallocation, $allocateduser, $option3);

        $cm = get_coursemodule_from_instance('allocationform', $allocation->id);
        $context = \context_module::instance($cm->id);

        $users = [$choiceuser->id, $allocateduser->id, $restricteduser->id];
        $userlist = new \core_privacy\local\request\approved_userlist($context, 'allocationform', $users);
        provider::delete_data_for_users($userlist);

        // Check that the correct data been deleted.
        $formparams = ['formid' => $allocation->id];
        $userparams = ['formid' => $allocation->id, 'userid' => $otheruser->id];
        $this->assertEquals(1, $DB->count_records('allocationform_allocations', $formparams));
        $this->assertEquals(1, $DB->count_records('allocationform_allocations', $userparams));
        $this->assertEquals(1, $DB->count_records('allocationform_choices', $formparams));
        $this->assertEquals(1, $DB->count_records('allocationform_choices', $userparams));
        $this->assertEquals(1, $DB->count_records('allocationform_disallow', $formparams));
        $this->assertEquals(1, $DB->count_records('allocationform_disallow', $userparams));

        // Should be no changes.
        $unchangedparams = ['formid' => $otherallocation->id];
        $this->assertEquals(1, $DB->count_records('allocationform_allocations', $unchangedparams));
        $this->assertEquals(1, $DB->count_records('allocationform_choices', $unchangedparams));
        $this->assertEquals(1, $DB->count_records('allocationform_disallow', $unchangedparams));
    }
}

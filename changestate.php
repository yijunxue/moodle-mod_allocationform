<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Page for changing the state of an allocation form.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use core\output\notification;
use mod_allocationform\helper;
use mod_allocationform\output\progress;

require_once(dirname(dirname(__DIR__)) . '/config.php');

$id = required_param('id', PARAM_INT); // Course_module ID.
$state = required_param('state', PARAM_INT);
$confirmed = optional_param('confirmed', false, PARAM_BOOL);

list($course, $cm) = get_course_and_cm_from_cmid($id, 'allocationform');

require_login($course, false, $cm);
$context = context_module::instance($cm->id);

$PAGE->set_url('/mod/allocationform/changestate.php', ['id' => $id, 'state' => $state]);
$PAGE->set_title(format_string($cm->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);

$returnurl = new moodle_url('/mod/allocationform/view.php', ['id' => $id]);

if (!helper::is_valid_manual_state_change($state, $cm)) {
    // We do not need to do anything.
    $message = get_string('invalidstatechange', 'mod_allocationform');
    redirect($returnurl, $message, null, notification::NOTIFY_ERROR);
}

// Get strings needed for the change.
switch ($state) {
    case helper::STATE_PROCESS:
        require_capability('mod/allocationform:reallocate', $context);
        $eventstring = 'reprocess';
        $statename = get_string('processmode', 'mod_allocationform');
        $notification = new notification(get_string('reprocess_warning', 'mod_allocationform'), notification::NOTIFY_ERROR);
        $notification->set_show_closebutton(false);
        break;
    case helper::STATE_EDITING:
        require_capability('mod/allocationform:edit', $context);
        $eventstring = 'editing_state';
        $statename = get_string('editingmode', 'mod_allocationform');
        $notification = new notification(get_string('back_to_edit_warning', 'mod_allocationform'), notification::NOTIFY_ERROR);
        $notification->set_show_closebutton(false);
        break;
    case helper::STATE_PROCESSED:
        require_capability('mod/allocationform:edit', $context);
        $eventstring = 'set_results_available';
        $statename = get_string('processedmode', 'mod_allocationform');
        break;
    case helper::STATE_READY:
        require_capability('mod/allocationform:edit', $context);
        $eventstring = 'make_ready';
        $statename = get_string('readymode', 'mod_allocationform');
        break;
    case helper::STATE_REVIEW:
        require_capability('mod/allocationform:edit', $context);
        $eventstring = 'review';
        $statename = get_string('reviewmode', 'mod_allocationform');
        break;
}

if ($confirmed) {
    // Change the state of the form.
    require_sesskey();
    $record = $DB->get_record('allocationform', ['id' => $cm->instance], '*', MUST_EXIST);
    $allocationform = new \mod_allocationform\init($record);
    if ($allocationform->change_state($state)) {
        // Log that the state was changed.
        $event = \mod_allocationform\event\state_changed::create(array(
            'objectid' => $cm->instance,
            'context' => $context,
            'other' => ['function' => $state, 'newstate' => $eventstring],
        ));
        $event->trigger();
        $message = get_string('statechanged', 'mod_allocationform');
        $status = notification::NOTIFY_SUCCESS;
    } else {
        $message = get_string('statechangefailed', 'mod_allocationform');
        $status = notification::NOTIFY_ERROR;
    }
    redirect($returnurl, $message, null, $status);
}

// Display a confirmation.
$confirmurl = new moodle_url('/mod/allocationform/changestate.php', ['id' => $id, 'state' => $state, 'confirmed' => true]);
$confirm = new single_button($confirmurl, get_string('yes'));
$cancel = new single_button($returnurl, get_string('no'));

$output = $PAGE->get_renderer('mod_allocationform');
$progress = new progress($cm->customdata['state']);
echo $output->header();
echo $output->heading(format_string($cm->name));
echo $output->render($progress);
if (isset($notification)) {
    echo $output->render($notification);
}
echo $output->confirm(get_string('changestateto', 'mod_allocationform', $statename), $confirm, $cancel);
echo $output->footer();

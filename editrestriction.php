<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Page for editing restrictions on an allocation form.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use mod_allocationform\disallow;
use mod_allocationform\disallow_form;
use mod_allocationform\helper;
use mod_allocationform\output\form;

require_once(dirname(dirname(__DIR__)) . '/config.php');

$id = required_param('id', PARAM_INT); // Course_module ID.
$optionid = required_param('option', PARAM_INT);

list($course, $cm) = get_course_and_cm_from_cmid($id, 'allocationform');

require_login($course, false, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/allocationform:edit', $context);

$PAGE->set_url('/mod/allocationform/editrestriction.php', ['id' => $id, 'option' => $optionid]);
$PAGE->set_title(format_string($cm->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);

// New disallow object.
$disallowlist = new disallow($cm->instance, $optionid);
// Create a new disallow form.
$customdata = array(
    'course' => $course->id,
    'allocationform' => $cm->instance,
    'id' => $cm->id,
    'option' => $optionid,
    'users' => $disallowlist->get_users(),
);
$form = new disallow_form(null, $customdata);

$returnurl = new moodle_url('/mod/allocationform/view.php', ['id' => $id]);

if ($cm->customdata['state'] != helper::STATE_EDITING || $form->is_cancelled()) {
    // The form is cancelled, or in the wrong state.
    redirect($returnurl);
}

if ($disallowlist->formdata = $form->get_data()) {
    $event = \mod_allocationform\event\permission_edited::create(array(
        'objectid' => $cm->instance,
        'context' => $context,
        'other' => ['option' => $optionid],
    ));
    $event->trigger();
    $disallowlist->update();
    $message = get_string('restrictionsmodified', 'mod_allocationform');
    redirect($returnurl, $message, null, \core\output\notification::NOTIFY_SUCCESS);
}

$event = \mod_allocationform\event\permission_viewed::create(array(
    'objectid' => $cm->instance,
    'context' => $context,
    'other' => ['option' => $optionid],
));
$event->trigger();

$output = $PAGE->get_renderer('mod_allocationform');
$renderable = new form($form, helper::STATE_EDITING);
echo $output->header();
echo $output->heading(format_string($cm->name));
echo $output->render($renderable);
echo $output->footer();

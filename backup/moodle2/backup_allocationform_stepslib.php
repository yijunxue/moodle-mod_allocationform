<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Define all the backup steps that will be used by the backup_allocationform_activity_task
 * @package   mod_allocationform
 * @copyright 2012 onwards, Nottingham University {@link http://nottingham.ac.uk}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Define the complete allocationform structure for backup, with file and id annotations
 * @package   mod_allocationform
 * @copyright 2012 onwards, Nottingham University {@link http://nottingham.ac.uk}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class backup_allocationform_activity_structure_step extends backup_activity_structure_step {

    /**
     * Define the structure for the allocationform activity
     * @return void
     */
    protected function define_structure() {

        // To know if we are including userinfo.
        $userinfo = $this->get_setting_value('userinfo');

        // Define each element separated.
        $allocationform = new backup_nested_element('allocationform', array('id'), array(
            'name', 'intro', 'introformat', 'timecreated', 'timemodified', 'numberofchoices', 'notwant',
            'numberofallocations', 'startdate', 'deadline', 'processed', 'roleid', 'state', 'trackcompletion'));

        $choices = new backup_nested_element('choices');

        $choice = new backup_nested_element('choice', array('id'), array(
            'userid', 'choice1', 'choice2', 'choice3', 'choice4', 'choice5', 'choice6',
            'choice7', 'choice8', 'choice9', 'choice10', 'notwant'));

        $options = new backup_nested_element('options');

        $option = new backup_nested_element('option', array('id'), array(
            'name', 'maxallocation', 'heading', 'sortorder'));

        $disallows = new backup_nested_element('disallows');

        $disallow = new backup_nested_element('disallow', array('id'), array(
            'userid', 'disallow_allocation'));

        $allocations = new backup_nested_element('allocations');

        $allocation = new backup_nested_element('allocation', array('id'), array(
            'userid', 'allocation'));

        // Build the tree.
        $allocationform->add_child($options);
        $options->add_child($option);

        $allocationform->add_child($disallows);
        $disallows->add_child($disallow);

        $allocationform->add_child($choices);
        $choices->add_child($choice);

        $allocationform->add_child($allocations);
        $allocations->add_child($allocation);

        // Define sources.
        $allocationform->set_source_table('allocationform', array('id' => backup::VAR_ACTIVITYID));

        $option->set_source_table('allocationform_options', array('formid' => backup::VAR_PARENTID));

        // All the rest of elements only happen if we are including user info.
        if ($userinfo) {
            $choice->set_source_table('allocationform_choices', array('formid' => backup::VAR_PARENTID));
            $disallow->set_source_table('allocationform_disallow', array('formid' => backup::VAR_PARENTID));
            $allocation->set_source_table('allocationform_allocations', array('formid' => backup::VAR_PARENTID));
        }

        // Define id annotations.
        $disallow->annotate_ids('user', 'userid');
        $disallow->annotate_ids('choice', 'disallow_allocation');

        $allocation->annotate_ids('user', 'userid');
        $allocation->annotate_ids('choice', 'allocation');

        $choice->annotate_ids('user', 'userid');
        $choice->annotate_ids('choice', 'choice1');
        $choice->annotate_ids('choice', 'choice2');
        $choice->annotate_ids('choice', 'choice3');
        $choice->annotate_ids('choice', 'choice4');
        $choice->annotate_ids('choice', 'choice5');
        $choice->annotate_ids('choice', 'choice6');
        $choice->annotate_ids('choice', 'choice7');
        $choice->annotate_ids('choice', 'choice8');
        $choice->annotate_ids('choice', 'choice9');
        $choice->annotate_ids('choice', 'choice10');
        $choice->annotate_ids('choice', 'notwant');

        // Define file annotations.
        $allocationform->annotate_files('mod_allocationform', 'intro', null);

        // Return the root element (allocationform), wrapped into standard activity structure.
        return $this->prepare_activity_structure($allocationform);
    }
}

<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Define all the restore steps that will be used by the restore_allocationform_activity_task
 * @package   mod_allocationform
 * @copyright 2012 onwards, Nottingham University {@link http://nottingham.ac.uk}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Structure step to restore one allocationform activity
 * @package   mod_allocationform
 * @copyright 2012 onwards, Nottingham University {@link http://nottingham.ac.uk}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class restore_allocationform_activity_structure_step extends restore_activity_structure_step {

    /**
     * Define the structure of the restore workflow.
     *
     * @return restore_path_element $structure
     */
    protected function define_structure() {

        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');

        $paths[] = new restore_path_element('allocationform', '/activity/allocationform');
        $paths[] = new restore_path_element('allocationform_options', '/activity/allocationform/options/option');

        if ($userinfo) {
            $paths[] = new restore_path_element('allocationform_choices', '/activity/allocationform/choices/choice');
            $paths[] = new restore_path_element('allocationform_disallow', '/activity/allocationform/disallows/disallow');
            $paths[] = new restore_path_element('allocationform_allocations', '/activity/allocationform/allocations/allocation');
        }

        // Return the paths wrapped into standard activity structure.
        return $this->prepare_activity_structure($paths);
    }

    /**
     * Process an allocationform restore.
     *
     * @param object $data The data in object form
     * @return void
     */
    protected function process_allocationform($data) {
        global $DB;

        $userinfo = $this->get_setting_value('userinfo');

        $data = (object)$data;
        $data->course = $this->get_courseid();

        if (!$userinfo) { // Only update these if user information is not being imported.
            $data->startdate = $this->apply_date_offset($data->startdate);
            $data->deadline = $this->apply_date_offset($data->deadline);
            $data->processed = 0;
            $data->state = \mod_allocationform\helper::STATE_EDITING;
        }

        // Insert the allocationform record.
        $newitemid = $DB->insert_record('allocationform', $data);
        $this->apply_activity_instance($newitemid);
    }

    /**
     * Restore the allocationform options
     * @param array $data
     */
    protected function process_allocationform_options($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->formid = $this->get_new_parentid('allocationform');

        // Insert the entry record.
        $newitemid = $DB->insert_record('allocationform_options', $data);
        $this->set_mapping('choice', $oldid, $newitemid);
    }

    /**
     * Restore the disallowed choices for this allocationform
     * @param array $data
     */
    protected function process_allocationform_disallow($data) {
        global $DB;

        $data = (object)$data;

        $data->formid = $this->get_new_parentid('allocationform');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->disallow_allocation = $this->get_mappingid('choice', $data->disallow_allocation, null);

        $DB->insert_record('allocationform_disallow', $data);
    }

    /**
     * Restore the allocationform choices
     * @param array $data
     */
    protected function process_allocationform_choices($data) {
        global $DB;

        $data = (object)$data;

        $data->formid = $this->get_new_parentid('allocationform');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->choice1 = $this->get_mappingid('choice', $data->choice1, null);
        $data->choice2 = $this->get_mappingid('choice', $data->choice2, null);
        $data->choice3 = $this->get_mappingid('choice', $data->choice3, null);
        $data->choice4 = $this->get_mappingid('choice', $data->choice4, null);
        $data->choice5 = $this->get_mappingid('choice', $data->choice5, null);
        $data->choice6 = $this->get_mappingid('choice', $data->choice6, null);
        $data->choice7 = $this->get_mappingid('choice', $data->choice7, null);
        $data->choice8 = $this->get_mappingid('choice', $data->choice8, null);
        $data->choice9 = $this->get_mappingid('choice', $data->choice9, null);
        $data->choice10 = $this->get_mappingid('choice', $data->choice10, null);
        $data->notwant = $this->get_mappingid('choice', $data->notwant, null);

        $DB->insert_record('allocationform_choices', $data);
    }

    /**
     * Restore the allocations
     * @param array $data
     */
    protected function process_allocationform_allocations($data) {
        global $DB;

        $data = (object)$data;

        $data->formid = $this->get_new_parentid('allocationform');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->allocation = $this->get_mappingid('choice', $data->allocation, null);

        $DB->insert_record('allocationform_allocations', $data);
    }

    /**
     * Once the database tables have been fully restored, restore the files
     * @return void
     */
    protected function after_execute() {
        $this->add_related_files('mod_allocationform', 'intro', null);
    }
}
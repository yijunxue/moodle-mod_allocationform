<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Define all the backup steps that will be used by the allocationfrom task
 * @package   mod_allocationform
 * @copyright 2012 onwards, Nottingham University {@link http://nottingham.ac.uk}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
/* @global \stdClass $CFG The global configuration object. */
require_once($CFG->dirroot . '/mod/allocationform/backup/moodle2/restore_allocationform_stepslib.php'); // Because it exists.

/**
 * allocationform restore task that provides all the settings and steps to perform one complete restore of the activity
 * @package   mod_allocationform
 * @copyright 2012 onwards, Nottingham University {@link http://nottingham.ac.uk}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class restore_allocationform_activity_task extends restore_activity_task {

    /**
     * Define (add) particular settings this activity can have
     */
    protected function define_my_settings() {
        // No particular settings for this activity.
    }

    /**
     * Define (add) particular steps this activity can have
     */
    protected function define_my_steps() {
        // Choice only has one structure step.
        $this->add_step(new restore_allocationform_activity_structure_step('allocationform_structure', 'allocationform.xml'));
    }

    /**
     * Define the contents in the activity that must be
     * processed by the link decoder
     */
    public static function define_decode_contents() {
        $contents = array();

        $contents[] = new restore_decode_content('allocationform', array('intro'), 'allocationform');

        return $contents;
    }

    /**
     * Define the decoding rules for links belonging
     * to the activity to be executed by the link decoder
     */
    public static function define_decode_rules() {
        $rules = array();
        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@see restore_logs_processor} when restoring
     * allocationform logs. It must return one array
     * of {@see restore_log_rule} objects
     */
    public static function define_restore_log_rules() {
        $rules = array();

        $rules[] = new restore_log_rule('allocationform', 'add', 'view.php?id={course_module}', '{allocationform}');
        $rules[] = new restore_log_rule('allocationform', 'update', 'view.php?id={course_module}', '{allocationform}');
        $rules[] = new restore_log_rule('allocationform', 'view', 'view.php?id={course_module}', '{allocationform}');
        $rules[] = new restore_log_rule('allocationform', 'view all', 'view.php?id={course_module}', '{allocationform}');
        $rules[] = new restore_log_rule('allocationform', 'generate_csv', 'view.php?id={course_module}', '{allocationform}');
        $rules[] = new restore_log_rule('allocationform', 'back_to_edit', 'view.php?id={course_module}', '{allocationform}');
        $rules[] = new restore_log_rule('allocationform', 'make_ready', 'view.php?id={course_module}', '{allocationform}');
        $rules[] = new restore_log_rule('allocationform', 'access_denied', 'view.php?id={course_module}', '{allocationform}');
        $rules[] = new restore_log_rule('allocationform', 'edit_disallow', 'view.php?id={course_module}', '{allocationform}');
        $rules[] = new restore_log_rule('allocationform', 'edit_option', 'view.php?id={course_module}', '{allocationform}');
        $rules[] = new restore_log_rule('allocationform', 'set_results_available', 'view.php?id={course_module}',
                '{allocationform}');
        $rules[] = new restore_log_rule('allocationform', 'reprocess', 'view.php?id={course_module}', '{allocationform}');

        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@see restore_logs_processor} when restoring
     * course logs. It must return one array
     * of {@see restore_log_rule} objects
     *
     * Note this rules are applied when restoring course logs
     * by the restore final task, but are defined here at
     * activity level. All them are rules not linked to any module instance (cmid = 0)
     */
    public static function define_restore_log_rules_for_course() {
        $rules = array();

        $rules[] = new restore_log_rule('allocationform', 'view all', 'index.php?id={course}', null);

        return $rules;
    }
}
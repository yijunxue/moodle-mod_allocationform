<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing class that stores the restriction information
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform;

defined('MOODLE_INTERNAL') || die();

/**
 * Class that stores the restriction information
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class disallow {
    /**
     * List of users including whether they have been disallowed from certain choices
     * @var array
     */
    protected $users;

    /**
     * Submitted form data
     * @var object
     */
    public $formdata;

    /**
     * Constructor method
     *
     * Sets the $this->users object which is a  list of all people eligible for allocation on the form
     * and includes any disallows for the option specified
     *
     * @param int $formid
     * @param int $optionid
     */
    public function __construct($formid, $optionid) {
        global $DB;

        $allocationform  = $DB->get_record('allocationform', array('id' => $formid), '*', MUST_EXIST);
        $course = get_course($allocationform->course);
        $cm = get_coursemodule_from_instance('allocationform', $formid, $course->id, false, MUST_EXIST);
        $context = \context_module::instance($cm->id);

        // Get a list of all people eligible for allocation on the form and include any disallows for the option specified.
        $mainfields = 'id, MAX(disallowid) as disallowid, MAX(formid) as formid, ' .
                'MAX(disallow_allocation) as disallow_allocation, firstname, lastname';
        $fields = 'u.id, d.id AS disallowid, d.formid, d.disallow_allocation, u.firstname, u.lastname';
        $fields2 = 'u.id, NULL, NULL, NULL, u.firstname, u.lastname';

        $params['roleid'] = $allocationform->roleid;
        $params['optionid'] = $optionid;
        $params['context'] = $context->id;
        $params['roleid2'] = $allocationform->roleid;
        $params['optionid2'] = $optionid;
        $params['context2'] = $context->id;

        $contexts = $context->get_parent_context_ids();
        $parentcontexts = ' OR r.contextid IN ('.implode(',', $contexts).')';

        // Get all the users and indicates if they have been restricted.
        $sql = "SELECT $mainfields FROM (SELECT $fields ".
                "FROM {role_assignments} r ".
                    "JOIN {user} u ON u.id = r.userid ".
                    "LEFT JOIN {allocationform_disallow} d ON d.userid = u.id ".
                "WHERE (r.contextid = :context $parentcontexts) ".
                    "AND r.roleid = :roleid ".
                    "AND u.deleted = 0 ".
                    "AND (d.disallow_allocation = :optionid OR d.disallow_allocation IS NULL) ".
                "UNION ".
                "SELECT $fields2 ".
                "FROM {role_assignments} r ".
                    "JOIN {user} u ON u.id = r.userid ".
                    "JOIN {allocationform_disallow} d ON d.userid = u.id ".
                "WHERE (r.contextid = :context2 $parentcontexts) ".
                    "AND r.roleid = :roleid2 ".
                    "AND u.deleted = 0 ".
                    "AND NOT (d.disallow_allocation = :optionid2)) t ".
                "GROUP BY t.id , t.lastname , t.firstname ".
                "ORDER BY t.lastname ASC, t.firstname ASC";

        $this->users = $DB->get_records_sql($sql, $params);
    }

    /**
     * Updates the restrictions
     */
    public function update() {
        global $DB;

        $formid = $this->formdata->allocationform;
        $optionid = $this->formdata->option;

        // Delete all disallow entries for this option.
        $DB->delete_records('allocationform_disallow', array('formid' => $formid, 'disallow_allocation' => $optionid));

        // Find the length of the pre-pended string identifying users.
        $length = strlen(helper::USER);
        $record = new \stdClass();
        $record->formid = $formid;
        $record->disallow_allocation = $optionid;

        foreach ($this->formdata as $key => $data) {
            if (substr($key, 0, $length) == helper::USER && $data == 1) {
                // This is a user who has been checked.
                // Remove the pre-appended string and get the users id.
                $record->userid = substr($key, $length);

                // Create a disallow record for the user.
                $DB->insert_record('allocationform_disallow', $record);
            }
        }
    }

    /**
     * Gets the list of disallowed users.
     *
     * Returns false if there are none.
     *
     * @return array|boolean
     */
    public function get_users() {
        if (!empty($this->users)) {
            return $this->users;
        }
        return false;
    }
}
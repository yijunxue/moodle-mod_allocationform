<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing the form for adding options to an allocationform
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace mod_allocationform;

defined('MOODLE_INTERNAL') || die();
require_once("$CFG->libdir/formslib.php");

/**
 * The form for adding options to an allocationform
 *
 * It uses the standard core Moodle formslib. For more info about them, please
 * visit: http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class option_form extends \moodleform {

    /**
     * Form definition for adding, deleting or editing options
     */
    public function definition() {
        global $CFG;
        $mform =& $this->_form;

        $course = $this->_customdata['course'];
        $allocationform = $this->_customdata['allocationform'];
        $id = $this->_customdata['id'];
        if (!empty($this->_customdata['option'])) {
            $option = $this->_customdata['option'];
        } else {
            $option = 0;
        }
        if (!empty($this->_customdata['function'])) {
            $function = $this->_customdata['function'];
        } else {
            $function = 0;
        }

        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('hidden', 'course', null);
        $mform->setType('course', PARAM_ALPHANUM);
        $mform->setConstant('course', $course);

        $mform->addElement('hidden', 'allocationform', null);
        $mform->setType('allocationform', PARAM_ALPHANUM);
        $mform->setConstant('allocationform', $allocationform);

        $mform->addElement('hidden', 'id', null);
        $mform->setType('id', PARAM_ALPHANUM);
        $mform->setConstant('id', $id);

        if ($option > 0) {
            $mform->addElement('hidden', 'option', null);
            $mform->setType('option', PARAM_ALPHANUM);
            $mform->setConstant('option', $option);
        }

        $mform->addElement('text', 'name', get_string('option_name', 'mod_allocationform'), array('size' => '64'));

        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEAN);
        }

        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('name', 'option_name', 'mod_allocationform');

        // Create an array with 300 options.
        $maxallocationarray = array(0 => '');

        for ($i = 1; $i < 301; $i++) {
            $maxallocationarray[$i] = $i;
        }

        $mform->addElement('select', 'maxallocation', get_string('option_maxallocation', 'mod_allocationform'),
                $maxallocationarray, null);
        $mform->addRule('maxallocation', null, 'required', null, 'client');
        $mform->addHelpButton('maxallocation', 'option_maxallocation', 'mod_allocationform');
        $mform->setDefault('maxallocation', 0);

        $this->add_action_buttons();
    }

    /**
     * Form validation method
     *
     * @param array $data array of ("fieldname"=>value) of submitted data
     * @param array $files array of uploaded files "element_name"=>tmp_file_path
     * @return array of "element_name"=>"error_description" if there are errors,
     *         or an empty array if everything is OK (true allowed for backwards compatibility too).
     */
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);

        if ($data['maxallocation'] < 1) {
            $errors['maxallocation'] = get_string('option_maxallocation_invalid', 'mod_allocationform');
        }

        return $errors;
    }
}

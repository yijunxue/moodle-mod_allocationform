<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing init class
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace mod_allocationform;


/**
 * A class that stores the important information about an allocation form
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class init {
    /** The allocationform object
     * @var \stdClass
     */
    protected $allocationform;

    /** An array of options for this form.
     * @var array
     */
    protected $options;

    /**
     * Constructor
     *
     * @param \stdClass $allocationform Allocation form object (retrieved from database)
     */
    public function __construct(\stdClass $allocationform) {
        $this->allocationform = $allocationform;
    }

    /**
     * change_state
     *
     * Update the state of the form e.g editing, ready, processed, process, review
     *
     * @param int $state
     * @return boolean
     * @throws \Exception
     */
    public function change_state($state) {
        global $DB;

        if (empty($this->allocationform->state)) { // The allocationform object is invalid.
            return false;
        }

        if ($this->allocationform->state == helper::STATE_PROCESSED) {
            // We are changing from a release state, so we should remove the event for .
            \mod_allocationform\activity::delete_avaliable_event($this->allocationform->id);
        }

        $cm = get_fast_modinfo($this->allocationform->course)->get_instances_of('allocationform')[$this->allocationform->id];
        switch($state) {
            case helper::STATE_EDITING:
                // Delete any alloctions made.
                $DB->delete_records('allocationform_choices', array('formid' => $this->get_id()));
                break;
            case helper::STATE_PROCESSED:
                // Add an event to say the allocations are avaliable.
                activity::create_avaliable_event($cm);
                break;
            case helper::STATE_READY:
            case helper::STATE_PROCESS:
            case helper::STATE_REVIEW:
                // Everything is good.
                break;
            default:
                throw new \Exception('mod_allocationform: invalid state');
        }

        $record = new \stdClass();
        $record->id = $this->get_id();
        $record->state = $state;
        $DB->update_record('allocationform', $record);
        $this->allocationform->state = $state;
        // The state is cached in the course modinfo so we need to ensure that is rebuilt.
        rebuild_course_cache($this->allocationform->course, true);
        return true;
    }

    /**
     * Get the context for the activity.
     *
     * @return \context_module $context
     */
    private function get_context() {
        $course = get_course($this->allocationform->course);
        $cm = get_coursemodule_from_instance('allocationform', $this->allocationform->id, $course->id, false, MUST_EXIST);
        $context = \context_module::instance($cm->id);
        return $context;
    }

    /**
     * Set allocation form options details for the particular form id
     *
     * @param array $groupmax
     * @param array $groupnames
     * @param array $groupidmap
     */
    private function set_options_details(&$groupmax, &$groupnames, &$groupidmap) {
        global $DB;

        $results = $DB->get_recordset('allocationform_options', array('formid' => $this->allocationform->id));

        foreach ($results as $row) {
            $groupmax[$row->id] = $row->maxallocation;
            $groupnames[$row->id] = $row->name;
            $groupidmap[] = $row->id;
        }

        $results->close();
    }

    /**
     * Get allocation restrictions (array of disallowed choices for specific users)
     *
     * @return array $disallow Disallowed allocation for a specific user
     */
    private function get_allocation_restrictions() {
        global $DB;

        // Get a list of allocation restrictions.
        $disallow = array(); // An array of userid's that contains an array of options they are not able to be allocated to.
        $results = $DB->get_recordset('allocationform_disallow', array('formid' => $this->allocationform->id));

        foreach ($results as $row) {
            $disallow[$row->userid][] = $row->disallow_allocation;
        }

        $results->close();

        return $disallow;
    }

    /**
     * Get selections made by users
     *
     * @param object $context
     * @param array $choicesarray
     * @param array $incompletearray
     * @param int $incomplete
     * @param int $userno
     */
    private function get_selections($context, &$choicesarray, &$incompletearray, &$incomplete, &$userno) {
        $contexts = $context->get_parent_context_ids();
        $parentcontexts = ' OR r.contextid IN ('.implode(',', $contexts).')';

        $fields = 'u.id AS userid, a.id, a.choice1, a.choice2, a.choice3, a.choice4, a.choice5, a.choice6, a.choice7, a.choice8, '.
            'a.choice9, a.choice10, a.notwant';

        $fields2 = 'u.id AS userid, null, null, null, null, null, null, null, null, null, null, null, null';

        $results = $this->get_user_choices($fields, $fields2, $parentcontexts, $context);
        $this->get_choices($results, $choicesarray, $incompletearray, $incomplete, $userno);
    }

    /**
     * Get array of completed and incomplete choices
     *
     * @param \moodle_recordset $results
     * @param array $choicesarray
     * @param array $incompletearray
     * @param int $incomplete
     * @param int $userno
     */
    private function get_choices($results, &$choicesarray, &$incompletearray, &$incomplete, &$userno) {
        foreach ($results as $row) {
            if ($row->choice1 !== null) {
                $choicesarray[$userno]['userid'] = $row->userid;
                $choicesarray[$userno]['choice0'] = $row->choice1;
                $choicesarray[$userno]['choice1'] = $row->choice2;
                $choicesarray[$userno]['choice2'] = $row->choice3;
                $choicesarray[$userno]['choice3'] = $row->choice4;
                $choicesarray[$userno]['choice4'] = $row->choice5;
                $choicesarray[$userno]['choice5'] = $row->choice6;
                $choicesarray[$userno]['choice6'] = $row->choice7;
                $choicesarray[$userno]['choice7'] = $row->choice8;
                $choicesarray[$userno]['choice8'] = $row->choice9;
                $choicesarray[$userno]['choice9'] = $row->choice10;
                $choicesarray[$userno]['not_want'] = $row->notwant;
                $userno++;
            } else {
                $incompletearray[$incomplete]['userid'] = $row->userid;
                $incompletearray[$incomplete]['choice0'] = null;
                $incompletearray[$incomplete]['choice1'] = null;
                $incompletearray[$incomplete]['choice2'] = null;
                $incompletearray[$incomplete]['choice3'] = null;
                $incompletearray[$incomplete]['choice4'] = null;
                $incompletearray[$incomplete]['choice5'] = null;
                $incompletearray[$incomplete]['choice6'] = null;
                $incompletearray[$incomplete]['choice7'] = null;
                $incompletearray[$incomplete]['choice8'] = null;
                $incompletearray[$incomplete]['choice9'] = null;
                $incompletearray[$incomplete]['not_want'] = null;
                $incomplete++;
            }
        }
        $results->close();
    }

    /**
     * Shuffle the choices array and merge it with the incomplete array
     *
     * @param array $choicesarraytemp
     * @param array $incompletearray
     */
    private function merge_shuffle_choices(&$choicesarraytemp, $incompletearray) {
        shuffle($choicesarraytemp);
        $choicesarraytemp = array_merge($choicesarraytemp, $incompletearray);
    }

    /**
     * Get allocated choices and master selections
     *
     * @param array $masterselections
     * @param array $choicesallocated
     * @param int $fit
     * @param int $allocation
     * @param array $groupmax
     * @param int $optioniteration
     * @param int $choiceno
     * @param array $choicesarraytemp
     */
    private function get_allocated_choices(&$masterselections, &$choicesallocated, &$fit, $allocation, $groupmax, $optioniteration,
            $choiceno, $choicesarraytemp) {

        $j = 0;

        while ($j < $choiceno and count($choicesallocated) < $allocation) { // Loop through the users choices until they.
            // Have been allocated to enough, or there are no more choices.
            $choice = $choicesarraytemp[$optioniteration]["choice$j"];

            if (isset($groupmax[$choice]) && (!isset($masterselections[$choice]) ||
                    count($masterselections[$choice]) < $groupmax[$choice])) {
                // There are still open slots for this option.
                $masterselections[$choice][] = $choicesarraytemp[$optioniteration]['userid'];
                $fit += $choiceno - $j;
                $choicesallocated[] = $choice;
            }
            $j++;
        }
    }

    /**
     * Exclude a user from selecting a particular choice
     *
     * @param int $notwant
     * @param array $disallow
     * @param array $choicesallocated
     * @param array $masterselections
     * @param array $unallocated
     * @param array $groupidmap
     * @param array $choicesarraytemp
     * @param int $groupno
     * @param array $groupmax
     * @param int $i
     * @param int $fit
     * @param int $allocation
     * @return void
     */
    private function exclude_user_choice($notwant, $disallow, &$choicesallocated, &$masterselections, &$unallocated, $groupidmap,
            $choicesarraytemp, $groupno, $groupmax, $i, &$fit, $allocation) {
        if (count($groupidmap) === 0) {
            // There are no options to be allocated to.
            foreach ($choicesarraytemp as $choices) {
                $unallocated[] = $choices['userid'];
            }
            $fit = -10000000;
            return;
        }

        $iterator = 0;

        if ($notwant == 1) {  // Not want option used on form.
            do { // Loop through the options until they are given enough allocations, or we run out of options.
                $try = $groupidmap[$iterator];

                // Check if the user restricted from this option.
                $notdisallowed = true;

                if (isset($disallow[$choicesarraytemp[$i]['userid']]) &&
                        in_array($try, $disallow[$choicesarraytemp[$i]['userid']])) {

                    // They are disallowed from this group.
                    $notdisallowed = false;
                }

                if (((!isset($masterselections[$try]) || (count($masterselections[$try]) < $groupmax[$try]))
                        && $try <> $choicesarraytemp[$i]['not_want']
                        && !in_array($try, $choicesallocated)) && $notdisallowed) {

                    // The option has free slots and is not rejected by the person.
                    $masterselections[$try][] = $choicesarraytemp[$i]['userid'];
                    $fit -= 500;
                    $choicesallocated[] = $try;
                }
                $iterator++;
            } while ($iterator < $groupno and count($choicesallocated) < $allocation);

            if (count($choicesallocated) < $allocation) { // Still not enough allocations made for the person.
                $unallocated[] = $choicesarraytemp[$i]['userid'];
                $fit -= 50000;
            }
        } else {
            do { // Loop through all the options.
                $try = $groupidmap[$iterator];

                // Check if the user restricted from this option.
                $notdisallowed = true;

                if (isset($disallow[$choicesarraytemp[$i]['userid']]) &&
                        in_array($try, $disallow[$choicesarraytemp[$i]['userid']])) {
                    $notdisallowed = false; // They are disallowed from this group.
                }

                if ($notdisallowed && (!isset($masterselections[$try])
                        || (count($masterselections[$try]) < $groupmax[$try] && !in_array($try, $choicesallocated)))) {

                    $masterselections[$try][] = $choicesarraytemp[$i]['userid'];
                    $choicesallocated[] = $try;
                }

                $iterator++;
            } while ($iterator < $groupno and count($choicesallocated) < $allocation);

            if (count($choicesallocated) < $allocation) {
                $unallocated[] = $choicesarraytemp[$i]['userid'];
                $fit -= 50000;
            }
        }
    }

    /**
     * Store details of unallocated choices
     *
     * @param array|null $bufferunallocated
     * @return string $messages
     */
    private function buffer_unallocated_choices($bufferunallocated) {
        $messages = '';

        foreach ($bufferunallocated as $userid) {
            $messages .= get_string('form_allocation_error2', 'mod_allocationform', array('userid' => $userid))."\n";
        }

        if ($messages != '') {
            $messages = "\n".get_string('form_allocation_error', 'mod_allocationform',
                    array('formid' => $this->allocationform->id))."\n$messages\n";
        }

        return $messages;
    }

    /**
     * Delete previous allocations
     */
    private function delete_previous_allocations() {
        global $DB;

        // Delete all records from the allocation table for this form, this ensures if a re-allocation has been asked for
        // that the data is not contaminated.
        $DB->delete_records('allocationform_allocations', array('formid' => $this->allocationform->id));
    }

    /**
     * Save new allocations
     * @param array $buffer
     */
    private function insert_new_allocations($buffer) {
        global $DB;

        $transaction = $DB->start_delegated_transaction();

        try {
            $record = new \stdClass();
            $record->formid = $this->allocationform->id;

            foreach ($buffer as $key => $userlist) {// Loop through all the options.
                $record->allocation = $key;

                foreach ($userlist as $person) {// Loop through everyone that has been allocated to an option.
                    $record->userid = $person;
                    $DB->insert_record('allocationform_allocations', $record, false);
                }
            }

            $this->change_state(helper::STATE_REVIEW);

            // Save the allocation records.
            $transaction->allow_commit();
        } catch (\Exception $e) {
            $transaction->rollback($e);
        }
    }

    /**
     * This function will allocate users to the options in the form
     * @return string
     */
    public function allocate() {
        if (empty($this->allocationform->id)) {
            return false;
        }

        $context = $this->get_context();

        // Get information on the groups first.
        $choiceno = $this->allocationform->numberofchoices;
        $allocation = $this->allocationform->numberofallocations;
        $notwant = $this->allocationform->notwant;
        $groupmax = array();
        $groupnames = array();
        $groupidmap = array();

        $this->set_options_details($groupmax, $groupnames, $groupidmap);
        $groupno = count($groupnames);

        $disallow = $this->get_allocation_restrictions();

        // Query the database of selections.
        $userno = 0;
        $incomplete = 0;

        $choicesarray = array();
        $incompletearray = array();

        $this->get_selections($context, $choicesarray, $incompletearray, $incomplete, $userno);

        $maxfit = -99999999999;

        // Control how many attempts to get best fit.
        for ($go = 0; $go < helper::ALLOCATION_ATTEMPTS; $go++) {
            // Students who did not complete the form allways come last in the allocation.
            $choicesarraytemp = $choicesarray;
            $this->merge_shuffle_choices($choicesarraytemp, $incompletearray);
            // Randomise the order that we will attempted to allocate groups, when a user cannot be allocated a choice.
            shuffle($groupidmap);

            $masterselections = array();
            $unallocated = array();

            // Perform allocations.
            $fit = 0;

            // Loop through all the people to be allocated.
            for ($optioniteration = 0; $optioniteration < ($userno + $incomplete); $optioniteration++) {
                $choicesallocated = array();

                $this->get_allocated_choices($masterselections, $choicesallocated, $fit, $allocation,
                        $groupmax, $optioniteration, $choiceno, $choicesarraytemp);

                if (count($choicesallocated) < $allocation) { // The person could not be allocated to one of their choices.
                    $this->exclude_user_choice($notwant, $disallow, $choicesallocated, $masterselections, $unallocated, $groupidmap,
                            $choicesarraytemp, $groupno, $groupmax, $optioniteration, $fit, $allocation);
                }
            }

            if ($fit > $maxfit) { // If this is a better fit we will save it.
                $buffer = $masterselections;
                $bufferunallocated = $unallocated;
                $maxfit = $fit;
            }
        }
        $messages = $this->buffer_unallocated_choices($bufferunallocated);
        $this->delete_previous_allocations();
        $this->insert_new_allocations($buffer);

        return $messages;
    }

    /**
     * Get the state of the allocation from
     * @return int
     */
    public function get_state() {
        return $this->allocationform->state;
    }

    /**
     * Get the name of the allocation form instance
     * @return string
     */
    public function get_name() {
        return $this->allocationform->name;
    }

    /**
     * Get the id of the allocation form
     * @return int
     */
    public function get_id() {
        return $this->allocationform->id;
    }

    /**
     * Get the roleid for the role associated with this allocation form.
     * @return int
     */
    public function get_roleid() {
        return $this->allocationform->roleid;
    }

    /**
     * Get the intro of the allocation form
     * @return string
     */
    public function get_intro() {
        return $this->allocationform->intro;
    }

    /**
     * Get the allocationform object
     * @return \stdClass
     */
    public function get_form() {
        return $this->allocationform;
    }

    /**
     * Get the number of choices
     * @return int
     */
    public function get_numberofchoices() {
        return $this->allocationform->numberofchoices;
    }

    /**
     * Check whether this allocation form should include a 'notwant' option
     * @return int
     */
    public function get_notwant() {
        return $this->allocationform->notwant;
    }

    /**
     * Get the number of allocations
     * @return int
     */
    public function get_numberofallocations() {
        return $this->allocationform->numberofallocations;
    }

    /**
     * Get the start date for this allocation form
     * @return int
     */
    public function get_startdate() {
        return $this->allocationform->startdate;
    }

    /**
     * Get the deadline for this allocation form
     * @return int
     */
    public function get_deadline() {
        return $this->allocationform->deadline;
    }

    /**
     * Get user choices for the selected form
     *
     * @param string $fields Select the user's choices for the selected form
     * @param string $fields2 Select all other users who have not made a choice in the selected form
     * @param string $parentcontexts
     * @param \context $context
     * @return \moodle_recordset
     */
    protected function get_user_choices($fields, $fields2, $parentcontexts, $context) {
        global $DB;

        $params = array();
        $params['roleid'] = $this->allocationform->roleid;
        $params['context'] = $context->id;
        $params['formid'] = $this->allocationform->id;

        $params['roleid2'] = $this->allocationform->roleid;
        $params['context2'] = $context->id;
        $params['formid2'] = $this->allocationform->id;

        $assignedusers = 'SELECT a.userid FROM {allocationform_choices} a WHERE formid = :formid2';

        // Get all users in the current context with their choices for the selected form or null if no choices made.
        // Using a union with subselect here is more efficient that e.g. using a MAX on formid with GROUP BY on u.id.
        $sql = "SELECT $fields ".
                "FROM {role_assignments} r ".
                "JOIN {user} u ON u.id = r.userid ".
                "JOIN {allocationform_choices} a ON u.id = a.userid ".
                "WHERE (r.contextid = :context $parentcontexts) ".
                    "AND r.roleid = :roleid ".
                    "AND u.deleted = 0 ".
                    "AND a.formid = :formid ".
                "UNION ALL ". // We use a union here because we want at least one record per user including users with no choices.
                "SELECT $fields2 ".
                "FROM {role_assignments} r ".
                "JOIN {user} u ON u.id = r.userid ".
                "WHERE (r.contextid = :context2 $parentcontexts) ".
                    "AND r.roleid = :roleid2 ".
                    "AND u.deleted = 0 ".
                     // This query is based on users who have not made a choice yet in this form.
                    "AND u.id NOT IN ($assignedusers)"; // Could investigate NOT EXISTS if this proves to be a slow query.

        $results = $DB->get_recordset_sql($sql, $params);
        return $results;
    }

    /**
     * Generates a csv file of all users listing their choices
     */
    public function generate_choice_csv() {
        $filename = 'error';
        if (!empty($this->allocationform->id)) {
            $filename = $this->allocationform->name;
        }
        // Generate the column headings.
        $output = '"firstname","lastname","idnumber","choice 1","choice 2","choice 3","choice 4","choice 5","choice 6","choice 7",'.
                '"choice 8","choice 9","choice 10","not want"'."\n";

        if (!empty($this->allocationform->id)) {
            global $DB;
            // First we get all the options and generate an array we will use to translate an option id into an option name.
            $options = array();
            $optionslist = $DB->get_recordset('allocationform_options',
                    array('formid' => $this->allocationform->id), null, 'id,name');

            foreach ($optionslist as $record) {
                $options[$record->id] = $record->name;
            }

            $optionslist->close();

            // Get the context for the activity.
            $course = get_course($this->allocationform->course);
            $cm = get_coursemodule_from_instance('allocationform', $this->allocationform->id, $course->id, false, MUST_EXIST);
            $context = \context_module::instance($cm->id);

            $contexts = $context->get_parent_context_ids();
            $parentcontexts = ' OR r.contextid IN ('.implode(',', $contexts).')';

            $fields = 'u.id AS userid, u.firstname, u.lastname, u.idnumber, a.id, a.choice1, a.choice2, a.choice3, a.choice4, ' .
                    'a.choice5, a.choice6, a.choice7, a.choice8, a.choice9, a.choice10, a.notwant';

            $fields2 = 'u.id AS userid, u.firstname, u.lastname, u.idnumber, null, null, null, null, null, null, null, null, ' .
                    'null,  null, null, null';

            if (!$results = $this->get_user_choices($fields, $fields2, $parentcontexts, $context)) {
                return;
            }

            foreach ($results as $record) {
                $output .= '"'.$record->firstname.'","'.$record->lastname.'","'.$record->idnumber.'","';
                $output .= ((!empty($record->choice1)) ? ($options[$record->choice1]) : '').'","';
                $output .= ((!empty($record->choice2)) ? $options[$record->choice2] : '').'","';
                $output .= ((!empty($record->choice3)) ? $options[$record->choice3] : '').'","';
                $output .= ((!empty($record->choice4)) ? $options[$record->choice4] : '').'","';
                $output .= ((!empty($record->choice5)) ? $options[$record->choice5] : '').'","';
                $output .= ((!empty($record->choice6)) ? $options[$record->choice6] : '').'","';
                $output .= ((!empty($record->choice7)) ? $options[$record->choice7] : '').'","';
                $output .= ((!empty($record->choice8)) ? $options[$record->choice8] : '').'","';
                $output .= ((!empty($record->choice9)) ? $options[$record->choice9] : '').'","';
                $output .= ((!empty($record->choice10)) ? $options[$record->choice10] : '').'","';
                $output .= ((!empty($record->notwant)) ? $options[$record->notwant] : '').'"';
                $output .= "\n";
            }
            $results->close();
        }
        send_temp_file($output, $filename . '_user_choices.csv', true);
    }

    /**
     * Generates a csv file of all users listing their allocations
     */
    public function generate_allocation_csv() {
        $filename = 'error';

        if (!empty($this->allocationform->id)) {
            $filename = $this->allocationform->name;
        }

        // Generate the column headings.
        $file = '"firstname","lastname","idnumber","allocation 1","allocation 2","allocation 3","allocation 4","allocation 5",' .
                '"allocation 6","allocation 7","allocation 8","allocation 9","allocation 10"'."\n";

        if (!empty($this->allocationform->id)) {
            global $DB;
            // First we get all the options and generate an array we will use to translate an option id into an option name.
            $options = array();
            $optionslist = $DB->get_recordset('allocationform_options',
                    array('formid' => $this->allocationform->id), null, 'id,name');
            foreach ($optionslist as $record) {
                $options[$record->id] = $record->name;
            }
            $optionslist->close();

            // Get the context for the activity.
            $course = get_course($this->allocationform->course);
            $cm = get_coursemodule_from_instance('allocationform', $this->allocationform->id, $course->id, false, MUST_EXIST);
            $context = \context_module::instance($cm->id);

            $params['roleid'] = $this->allocationform->roleid;
            $params['context'] = $context->id;
            $params['formid'] = $this->allocationform->id;

            $params['roleid2'] = $this->allocationform->roleid;
            $params['context2'] = $context->id;
            $params['formid2'] = $this->allocationform->id;

            $contexts = $context->get_parent_context_ids();
            $parentcontexts = ' OR r.contextid IN ('.implode(',', $contexts).')';
            $assignedusers = 'SELECT DISTINCT a.userid FROM {allocationform_allocations} a WHERE formid = :formid2';

            $fields = 'u.id AS userid, u.firstname, u.lastname, u.idnumber, a.id, a.allocation';
            $fields2 = 'u.id AS userid, u.firstname, u.lastname, u.idnumber, null, null';

            // Get all users and their choices.
            $sql = "SELECT DISTINCT $fields ".
                    "FROM {user} u ".
                    "JOIN {allocationform_allocations} a ON u.id = a.userid ".
                    "WHERE u.deleted = 0 ".
                        "AND a.formid = :formid ".
                    "UNION ".
                    "SELECT $fields2 ".
                    "FROM {role_assignments} r ".
                    "JOIN {user} u ON u.id = r.userid ".
                    "WHERE (r.contextid = :context2 $parentcontexts) ".
                        "AND r.roleid = :roleid2 ".
                        "AND u.deleted = 0 ".
                        "AND u.id NOT IN ($assignedusers)";

            $rs = $DB->get_recordset_sql($sql, $params);

            // Build an array of the allocations made.
            $output = array();
            foreach ($rs as $record) {
                $output[$record->userid]['firstname'] = $record->firstname;
                $output[$record->userid]['lastname'] = $record->lastname;
                $output[$record->userid]['idnumber'] = $record->idnumber;
                if (array_key_exists($record->allocation, $options)) {
                    $output[$record->userid][] = $options[$record->allocation];
                } else {
                    $output[$record->userid][] = '';
                }
            }
            $rs->close();

            // Output the allocations.
            foreach ($output as $row) {
                $file .= '"'.implode('","', $row).'"'."\n";
            }
        }
        send_temp_file($file, $filename . '_user_allocations.csv', true);
    }
}

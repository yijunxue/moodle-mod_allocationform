<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing helper class for the allocationform module.
 *
 * @package    mod_allocationform
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace mod_allocationform;

defined('MOODLE_INTERNAL') || die();

/**
 * Helper class for the allocationform module.
 *
 * @package    mod_allocationform
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class helper {
    // A list of states that a form can be in.
    /** Editing state */
    const STATE_EDITING = 1;
    /** Ready state */
    const STATE_READY = 2;
    /** Waiting to process state */
    const STATE_PROCESS = 4;
    /** Waiting for review */
    const STATE_REVIEW = 5;
    /** Processed state */
    const STATE_PROCESSED = 3;

    // A list of function.
    /** Edit an option */
    const FUNC_EDIT_OPTION = 3;
    /** Save preferences */
    const FUNC_SAVE_PREFS = 5;

    /** The number of times to loop during allocation. */
    const ALLOCATION_ATTEMPTS = 50000;
    /** The string 'user' */
    const USER = 'user';

    /**
     * Tests that a state transition can be made by a user.
     *
     * @param int $newstate
     * @param \cm_info $cm
     * @return bool
     */
    public static function is_valid_manual_state_change(int $newstate, \cm_info $cm) : bool {
        // The key of this array is the from state, the value an array of states that can be manually activated from it.
        $validchanges = array(
            self::STATE_EDITING => [self::STATE_READY],
            self::STATE_PROCESS => [], // Only automatic changes.
            self::STATE_PROCESSED => [self::STATE_PROCESS],
            self::STATE_READY => [self::STATE_EDITING],
            self::STATE_REVIEW => [self::STATE_PROCESS, self::STATE_PROCESSED],
        );
        return in_array($newstate, $validchanges[$cm->customdata['state']]);
    }
}

<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing class that stores an option for the allocation form
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform;

/**
 * A class that stores an option for the allocation form
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class option {
    /**
     * A specific form option
     * @var object
     */
    protected $option;

    /**
     * Submitted form data
     * @var object
     */
    public $formdata;

    /**
     * Whether the form has been validated
     * @var bool
     */
    public $valid = true;

    /**
     * Class constructor
     * @param \stdClass $option
     */
    public function __construct(\stdClass $option) {
        $this->option = $option;
    }

    /**
     * Inserts or updates an option
     *
     * @return boolean
     * @throws moodle_exception
     */
    public function update() {
        global $DB, $COURSE;

        if (empty($this->formdata)) { // No information to update.
            $courseurl = new \moodle_url('course/view.php', array('id' => $COURSE->id));
            throw new moodle_exception('noformdatapassed', 'mod_allocationform', $courseurl);
        }

        // Set up the object containing the database values.
        $data = new \stdClass();
        if (!empty($this->formdata->heading)) {
            $data->heading = $this->formdata->heading;
        }
        $data->name = $this->formdata->name;
        $data->formid = $this->formdata->allocationform;
        $data->maxallocation = $this->formdata->maxallocation;

        if (!empty($this->option->id)) { // Update the record.
            $id = $this->option->id;
            $data->id = $id;

            try {
                $DB->update_record('allocationform_options', $data);
            } catch (\dml_exception $e) {
                return false;
            }
        } else {// Add a new record.
            // Find the highest sortoder for the form and add 1 so this option is added to the end of the form.
            $params['formid'] = $this->formdata->allocationform;
            $sql = 'SELECT 1 As id, MAX(sortorder) AS sortorder FROM {allocationform_options} '.
                    'WHERE formid = :formid GROUP BY formid';
            $sortorderrecords = $DB->get_records_sql($sql, $params);
            $data->sortorder = 1;
            foreach ($sortorderrecords as $record) {
                $data->sortorder = $record->sortorder + 1;
            }

            try {
                $id = $DB->insert_record('allocationform_options', $data, true);
            } catch (\dml_exception $e) {
                return false;
            }
        }

        $this->option = $DB->get_record('allocationform_options', array('id' => $id), '*', MUST_EXIST);

        return true;
    }

    /**
     * Deletes the option
     *
     * @return boolean
     */
    public function delete() {
        global $DB;
        if ($this->get_id() === false) { // No option.
            return false;
        }
        $DB->delete_records('allocationform_disallow', ['disallow_allocation' => $this->option->id]);
        $DB->delete_records('allocationform_options', ['id' => $this->option->id]);
        $this->option = new \stdClass();
        return true;
    }

    /**
     * Get the option ID or false if not set
     * @return boolean||int
     */
    public function get_id() {
        if (isset($this->option->id) && !empty($this->option->id)) {
            return $this->option->id;
        }
        // It is not set so return false.
        return false;
    }

    /**
     * Get option name
     *
     * @return boolean|string
     */
    public function get_name() {
        if ($this->get_id() === false) { // If no id is set then this whole object is likely not correct.
            return false;
        }

        if (isset($this->option->name) && !empty($this->option->name)) {
            return $this->option->name;
        }
        // It is not set so return false.
        return false;
    }

    /**
     * Get option heading
     *
     * @return boolean|string
     */
    public function get_heading() {
        if ($this->get_id() === false) { // If no id is set then this whole object is likely not correct.
            return false;
        }

        if (isset($this->option->heading) && !empty($this->option->heading)) {
            return $this->option->heading;
        }
        // It is not set so return false.
        return false;
    }

    /**
     * Get the maximum number of allocations
     *
     * @return boolean|int
     */
    public function get_maxallocation() {
        if ($this->get_id() === false) { // If no id is set then this whole object is likely not correct.
            return false;
        }

        if (isset($this->option->maxallocation) && !empty($this->option->maxallocation)) {
            return $this->option->maxallocation;
        }
        // It is not set so return false.
        return false;
    }

    /**
     * Loads an option object.
     *
     * @global \moodle_database $DB
     * @param int $id
     * @return \mod_allocationform\option
     */
    public static function get(int $id) : option {
        global $DB;
        $record = $DB->get_record('allocationform_options', ['id' => $id], '*', MUST_EXIST);
        return new option($record);
    }

    /**
     * Returns the raw option record.
     *
     * @return \stdclass
     */
    public function get_record() {
        return $this->option;
    }
}

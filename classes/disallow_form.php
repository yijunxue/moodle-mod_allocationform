<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing the form that is used to change the restrictions for an option
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace mod_allocationform;
defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . '/formslib.php');

/**
 * The form that is used to change the restrictions for an option
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class disallow_form extends \moodleform {

    /**
     * Disallow form definition
     */
    public function definition() {
        global $DB;
        $mform =& $this->_form;

        $course = $this->_customdata['course'];
        $allocationform = $this->_customdata['allocationform'];
        $id = $this->_customdata['id'];

        if (!empty($this->_customdata['users'])) {
            $users = $this->_customdata['users'];
        } else {
            $users = array();
        }

        if (!empty($this->_customdata['option'])) {
            $option = $this->_customdata['option'];
        } else {
            $option = 0;
        }

        $mform->addElement('header', 'general', get_string('disallow_list', 'mod_allocationform'));

        $mform->addElement('hidden', 'course', null);
        $mform->setType('course', PARAM_ALPHANUM);
        $mform->setConstant('course', $course);

        $mform->addElement('hidden', 'allocationform', null);
        $mform->setType('allocationform', PARAM_ALPHANUM);
        $mform->setConstant('allocationform', $allocationform);

        $mform->addElement('hidden', 'id', null);
        $mform->setType('id', PARAM_ALPHANUM);
        $mform->setConstant('id', $id);

        if ($option > 0) {
            $mform->addElement('hidden', 'option', null);
            $mform->setType('option', PARAM_ALPHANUM);
            $mform->setConstant('option', $option);
        }

        $record = $DB->get_record('allocationform_options', array('id' => $option), 'id, name', MUST_EXIST);
        $message = get_string('disallow_list_help', 'mod_allocationform', array('option_name' => $record->name));
        $mform->addElement('html', \html_writer::tag('p', $message, array('class' => 'bold')));

        // Generate a list of all people with a checkbox next to them.
        foreach ($users as $user) {
            $elementname = helper::USER.$user->id;
            $label = "$user->lastname, $user->firstname";
            $mform->addElement('checkbox', $elementname, $label);
            if (property_exists($user, 'disallowid') && $user->disallowid !== null) {
                $mform->setDefault($elementname, 1);
            }
        }

        $this->add_action_buttons();
    }
}
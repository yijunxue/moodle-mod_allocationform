<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing \mod_allocationform\allocation class
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform;

defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/formslib.php");

/**
 * Class that stores the choices a user has made
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class allocation {
    /**
     * The choices for a particular user on a particular form
     * @var object
     */
    protected $allocation;

    /** @var \stdClass The course object for the form. */
    protected $course;

    /**
     * Submitted form data
     * @var object
     */
    protected $formdata;

    /** @var int The id of allocation form. */
    protected $formid;

    /** @var int The id of the user that will have their choices recorded. */
    protected $userid;

    /**
     * Class constructor
     *
     * @param int $userid
     * @param int $formid
     */
    public function __construct($userid, $formid) {
        global $DB;
        // This didn't have a form id in before. This meant that there would only ever be one set of choices saved for a user! Oops!
        $this->allocation = $DB->get_record('allocationform_choices',
                array('userid' => $userid, 'formid' => $formid), '*', IGNORE_MISSING);
        $this->userid = $userid;
        $this->formid = $formid;
    }

    /**
     * Builds a record that can be inserted into the database.
     *
     * @return boolean|\stdClass false if no valid record can be built.
     */
    protected function build_data() {
        if (empty($this->formdata)) { // No information to update.
            return false;
        }
        $data = new \stdClass();
        $data->formid = $this->formid;

        if (!empty($this->allocation->userid)) {
            $data->userid = $this->allocation->userid;
        } else {
            $data->userid = $this->userid;
        }

        if (!empty($this->formdata->notwant)) {
            $data->notwant = $this->formdata->notwant;
        } else {
            $data->notwant = null;
        }

        // Loop through the 10 possible choices and update.
        for ($i = 1; $i < 11; $i++) {
            $choice = "choice$i";
            if (!empty($this->formdata->$choice)) {
                $data->$choice = $this->formdata->$choice;
            }
        }

        return $data;
    }

    /**
     * Display / process choices form
     *
     * @param \mod_allocationform\allocate_form $mform
     * @param boolean $savedata
     */
    public function parse_form(allocate_form $mform, $savedata = true) {
        global $CFG, $COURSE;
        // Try to get the id of the allocation form page.
        $id = optional_param('id', 0, PARAM_INT);
        $this->course = $COURSE;
        $activityurl = new \moodle_url($CFG->wwwroot . '/mod/allocationform/view.php', ['id' => $id]);

        if ($mform->is_cancelled()) { // Try to redirect to the allocation form.
            $url = '';
            if (!empty($id)) {
                $url = $activityurl;
            } else {
                $url = new \moodle_url($CFG->wwwroot);
            }
            redirect($url);
        } else if ($savedata && $data = $mform->get_data()) {
            $this->formdata = $data;
            $this->update();
            $message = get_string('saved', 'mod_allocationform');
            redirect($activityurl, $message, null, \core\output\notification::NOTIFY_SUCCESS);
        }
    }

    /**
     * Processes and updates the users choices as passed to the choice web service.
     *
     * @param array $choices An array of user choices as defined by the choice web service
     *                      {@see \mod_allocationform\external\choices}
     * @param \stdClass $course The course the form is for
     * @return boolean
     */
    public function process_webservice($choices, \stdClass $course) {
        $this->course = $course;
        $this->formdata = (object) $choices;
        return $this->update();
    }

    /**
     * Create or update submitted choices
     *
     * @return boolean
     */
    protected function update() {
        global $DB;

        $data = $this->build_data();
        if ($data === false) {
            return false;
        }

        if ($this->allocation) {// There was a valid record found, so do an update.
            $id = $this->allocation->id;
            $data->id = $id;
            $DB->update_record('allocationform_choices', $data);
        } else { // Create a new record.
            $id = $DB->insert_record('allocationform_choices', $data, true);
        }

        $completion = new \completion_info($this->course);
        $formid = $this->formid;
        $cm = get_coursemodule_from_instance('allocationform', $formid, $this->course->id, false, MUST_EXIST);

        $allocationform = $DB->get_record('allocationform', array('id' => $formid));

        if ($completion->is_enabled($cm) && $allocationform->trackcompletion) {
            if (!empty($data->choice1) || !empty($data->notwant)) {
                $completion->update_state($cm, COMPLETION_COMPLETE);
            } else {
                $completion->update_state($cm, COMPLETION_INCOMPLETE);
            }
        }

        $this->allocation = $DB->get_record('allocationform_choices', array('id' => $id), '*', MUST_EXIST);

        return true;
    }

    /**
     * Gets the data needed to populate the allocate_form with the user's choices.
     *
     * @return \stdClass
     */
    public function get_choices_for_form() : \stdClass {
        if ($this->allocation) {
            $return = clone $this->allocation;
            unset($return->id);
            unset($return->formid);
            unset($return->userid);
        } else {
            $return = new \stdClass();
        }
        return $return;
    }

    /**
     * Get the allocation object for the current user
     *
     * @return object $this->allocation The allocation object retrieved from the database
     */
    public function get_allocation() {
        return $this->allocation;
    }
}

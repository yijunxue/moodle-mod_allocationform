<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File contianing the form that is used to select allocation preferences
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform;

defined('MOODLE_INTERNAL') || die();
require_once("$CFG->libdir/formslib.php");

/**
 * The form that is used to select allocation preferences
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class allocate_form extends \moodleform {

    /**
     * Choices form definition
     */
    public function definition() {
        $mform =& $this->_form;

        $course = $this->_customdata['course'];
        $allocationform = $this->_customdata['allocationform'];
        $id = $this->_customdata['id'];
        if (!empty($this->_customdata['choices'])) {
            $choices = $this->_customdata['choices'];
        } else {
            $choices = 0;
        }
        if (!empty($this->_customdata['allocation'])) {
            $allocation = $this->_customdata['allocation'];
        } else {
            $allocation = 0;
        }
        if (!empty($this->_customdata['notwant'])) {
            $notwant = $this->_customdata['notwant'];
        } else {
            $notwant = 0;
        }
        $user = $this->_customdata['user'];

        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Constants.
        $mform->addElement('hidden', 'course', null);
        $mform->setType('course', PARAM_ALPHANUM);
        $mform->setConstant('course', $course);

        $mform->addElement('hidden', 'allocationform', null);
        $mform->setType('allocationform', PARAM_ALPHANUM);
        $mform->setConstant('allocationform', $allocationform);

        $mform->addElement('hidden', 'id', null);
        $mform->setType('id', PARAM_ALPHANUM);
        $mform->setConstant('id', $id);

        // Instructions.
        $options = array('choices' => $choices, 'allocation' => $allocation);
        if ($notwant) {
            $instructions = get_string('instructionsnotwant', 'mod_allocationform', $options);
        } else {
            $instructions = get_string('instructions', 'mod_allocationform', $options);
        }

        $html = \html_writer::tag('p', $instructions);
        $mform->addElement('html', $html);

        // Mainform.
        $allocationoptions = $this->get_choice_array($allocationform, $user);

        // Build a set of select boxes, one for each choice that is allowed to the user.
        for ($i = 1; $i <= $choices; $i++) {
            $mform->addElement('select', "choice$i",
                    get_string('choice', 'mod_allocationform', array('choice' => $i)), $allocationoptions, null);
            $mform->addRule("choice$i", null, 'required', null, 'client');
            $mform->setDefault("choice$i", 0);
        }

        // Build a select for the user to say which option they do not wish to be allocated to.
        if ($notwant) { // This should be displayed.
            $allocationoptions[0] = get_string('notrequired', 'mod_allocationform');
            $mform->addElement('select', 'notwant', get_string('al_notwant', 'mod_allocationform'), $allocationoptions, null);
            $mform->addHelpButton('notwant', 'al_notwant', 'mod_allocationform');
            $mform->setDefault('notwant', 0);
        }

        $this->add_action_buttons();
    }

    /**
     * Get an array of choices
     *
     * @param int $formid
     * @param int $userid
     * @return array $allocationoptions
     */
    private function get_choice_array($formid, $userid) {
        global $DB;

        $fields = 'o.id, o.name, o.heading';
        $params['formid'] = $formid;
        $params['formid2'] = $formid;
        $params['userid'] = $userid;

        $disallowsql = 'SELECT d.disallow_allocation FROM {allocationform_disallow} d '.
                'WHERE d.formid = :formid2 AND d.userid = :userid';

        $sql = "SELECT $fields
                    FROM {allocationform_options} o
                    WHERE o.formid = :formid
                        AND o.id NOT IN ($disallowsql)
                    ORDER BY sortorder";

        $records = $DB->get_records_sql($sql, $params);
        $allocationoptions = array(0 => '');

        foreach ($records as $record) {
            $allocationoptions[$record->id] = $record->name;
        }

        return $allocationoptions;
    }

    /**
     * Form validation method
     *
     * @param array $data array of ("fieldname"=>value) of submitted data
     * @param array $files array of uploaded files "element_name"=>tmp_file_path
     * @return array of "element_name"=>"error_description" if there are errors,
     *         or an empty array if everything is OK (true allowed for backwards compatibility too).
     */
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);

        $allocationform = $this->_customdata['allocationform'];

        if (!empty($this->_customdata['choices'])) {
            $choices = $this->_customdata['choices'];
        } else {
            $choices = 0;
        }

        if (!empty($this->_customdata['notwant'])) {
            $notwant = $this->_customdata['notwant'];
        } else {
            $notwant = 0;
        }

        $user = $this->_customdata['user'];

        $allocationoptions = $this->get_choice_array($allocationform, $user);

        // Check the choices are valid.
        for ($i = 1; $i <= $choices; $i++) {
            if ($data["choice$i"] < 1) { // No selection made.
                $errors["choice$i"] = get_string('not_set', 'mod_allocationform');
            } else if (!array_key_exists($data["choice$i"], $allocationoptions)) { // The option has already been selected.
                $errors["choice$i"] = get_string('duplicate_choice', 'mod_allocationform');
            } else { // The option is fine so we must remove it from the array.
                unset($allocationoptions[$data["choice$i"]]);
            }
        }

        if ($notwant) {
            if (!array_key_exists($data["notwant"], $allocationoptions)) { // The option has already been selected.
                $errors['notwant'] = get_string('duplicate_choice', 'mod_allocationform');
            }
        }

        return $errors;
    }
}

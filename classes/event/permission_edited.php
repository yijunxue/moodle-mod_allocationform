<?php
// This file is part of the allocation form activity module
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing the permission_edited class
 *
 * @package    mod_allocationform
 * @copyright  2015 onwards University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk> - Upgrade to Moodle 2.7
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\event;

/**
 * Stores a "permission edited" event
 *
 * array $other {
 *      Extra information about access denied event.
 *
 *      - int option: Defines the option edited.
 * }
 *
 * @package    mod_allocationform
 * @copyright  2015 onwards University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk> - Upgrade to Moodle 2.7
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class permission_edited extends \core\event\base {
    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'u';
        $this->data['edulevel'] = self::LEVEL_OTHER;
        $this->data['objecttable'] = 'allocationform';
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        if (isset($this->other['option'])) {
            $optionid = $this->other['option'];
            $description = "Permission edited for option $optionid in allocation form $this->objectid by userid $this->userid";
        } else {
            $description = "Permission edited for allocation form $this->objectid by userid $this->userid";
        }
        return $description;
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('permissionedited', 'mod_allocationform');
    }

    /**
     * Get URL related to the action
     *
     * @return \moodle_url
     */
    public function get_url() {
        $params = ['id' => $this->contextinstanceid];
        if (isset($this->other['option'])) {
            $params['option'] = $this->other['option'];
            $url = '/mod/allocationform/editrestriction.php';
        } else {
            // An older version of this event, we do not have enough information to send to the editing page.
            $url = '/mod/allocationform/view.php';
        }
        return new \moodle_url($url, $params);
    }

    /**
     * Return the legacy event log data.
     *
     * @return array|null
     */
    protected function get_legacy_logdata() {
        // The legacy log table expects a relative path to /mod/allocationform/.
        $logurl = substr($this->get_url()->out_as_local_url(), strlen('/mod/allocationform/'));
        return array('permissionedited', 'mod_allocationform', $this->objectid, $logurl);
    }
}

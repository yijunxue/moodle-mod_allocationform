<?php
// This file is part of the allocation form activity module
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing csv_generated class
 *
 * @package    mod_allocationform
 * @copyright  2015 onwards University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk> - Upgrade to Moodle 2.7
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\event;


/**
 * Stores a "CSV generated" event
 *
 * array $other {
 *      Extra information about access denied event.
 *
 *      - int function: Defines which function the page should do.
 * }
 *
 * This event is no longer in use. There are now more specific events for CSV generation.
 * It needs to stay in the code base so there are no issue with old events that have been logged in Moodle.
 *
 * @package    mod_allocationform
 * @copyright  2015 onwards University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk> - Upgrade to Moodle 2.7
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @deprecated since version 3.0.2
 */
class csv_generated extends \core\event\base {
    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'r';
        $this->data['edulevel'] = self::LEVEL_OTHER;
        $this->data['objecttable'] = 'allocationform';
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        return "CSV generated for allocation form $this->objectid by userid $this->userid";
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('csvgenerated', 'mod_allocationform');
    }

    /**
     * Get URL related to the action
     *
     * @return \moodle_url
     */
    public function get_url() {
        $params = array(
            'id' => $this->contextinstanceid,
        );
        return new \moodle_url('/mod/allocationform/view.php', $params);
    }

    /**
     * Return the legacy event log data.
     *
     * @return array|null
     */
    protected function get_legacy_logdata() {
        // The legacy log table expects a relative path to /mod/allocationform/.
        $logurl = substr($this->get_url()->out_as_local_url(), strlen('/mod/allocationform/'));
        return array('csvgenerated', 'mod_allocationform', $this->objectid, $logurl);
    }
}

<?php
// This file is part of the allocation form activity module
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing the permission_viewed class
 *
 * @package    mod_allocationform
 * @copyright  2019 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\event;

/**
 * Stores a "permission viewed" event
 *
 * array $other {
 *      Extra information about access denied event.
 *
 *      - int option: Defines the option viewed.
 * }
 *
 * @package    mod_allocationform
 * @copyright  2019 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class permission_viewed extends \core\event\base {
    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'r';
        $this->data['edulevel'] = self::LEVEL_OTHER;
        $this->data['objecttable'] = 'allocationform';
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        $optionid = $this->other['option'];
        return "Permissions viewed for option $optionid in allocation form $this->objectid by userid $this->userid";
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('permissionviewed', 'mod_allocationform');
    }

    /**
     * Get URL related to the action
     *
     * @return \moodle_url
     */
    public function get_url() {
        $params = array(
            'id' => $this->contextinstanceid,
            'option' => $this->other['option'],
        );
        return new \moodle_url('/mod/allocationform/editrestriction.php', $params);
    }

    /**
     * Return the legacy event log data.
     *
     * @return array|null
     */
    protected function get_legacy_logdata() {
        // The legacy log table expects a relative path to /mod/allocationform/.
        $logurl = substr($this->get_url()->out_as_local_url(), strlen('/mod/allocationform/'));
        return array('permissionviewed', 'mod_allocationform', $this->objectid, $logurl);
    }
}

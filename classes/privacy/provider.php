<?php
// This file is part of the Allocation form activity.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Privacy providers for the plugin.
 *
 * @package    mod_allocationform
 * @category   privacy
 * @copyright  2018 Nottingham University
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\privacy;

use \core_privacy\local\metadata\collection;
use \core_privacy\local\request\contextlist;
use \core_privacy\local\request\approved_contextlist;
use \core_privacy\local\request\approved_userlist;
use \core_privacy\local\request\writer;
use \core_privacy\local\request\userlist;
use \core_privacy\local\request\helper;

defined('MOODLE_INTERNAL') || die;

/**
 * Definition of the data that is stored by the plugin.
 *
 * @see https://docs.moodle.org/dev/Privacy_API
 *
 * @package    mod_allocationform
 * @category   privacy
 * @copyright  2018 Nottingham University
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class provider implements
        \core_privacy\local\metadata\provider,
        \core_privacy\local\request\plugin\provider,
        \core_privacy\local\request\core_userlist_provider {
    /**
     * Returns meta data about the Allocation form activity.
     *
     * @param collection $collection The initialised collection to add items to.
     * @return collection A listing of user data stored through this system.
     */
    public static function get_metadata(collection $collection) : collection {
        // No core sub systems used.
        // Describes the personal information in the database tables.
        // The choices a user has made.
        $choices = [
            'userid' => 'privacy:metadata:allocationform_choices:userid',
            'choice1' => 'privacy:metadata:allocationform_choices:choice1',
            'choice2' => 'privacy:metadata:allocationform_choices:choice2',
            'choice3' => 'privacy:metadata:allocationform_choices:choice3',
            'choice4' => 'privacy:metadata:allocationform_choices:choice4',
            'choice5' => 'privacy:metadata:allocationform_choices:choice5',
            'choice6' => 'privacy:metadata:allocationform_choices:choice6',
            'choice7' => 'privacy:metadata:allocationform_choices:choice7',
            'choice8' => 'privacy:metadata:allocationform_choices:choice8',
            'choice9' => 'privacy:metadata:allocationform_choices:choice9',
            'choice10' => 'privacy:metadata:allocationform_choices:choice10',
            'notwant' => 'privacy:metadata:allocationform_choices:notwant',
        ];
        // The options a user has been allocation to.
        $allocations = [
            'userid' => 'privacy:metadata:allocationform_allocations:userid',
            'allocation' => 'privacy:metadata:allocationform_allocations:allocation',
        ];
        // The options a user is not allowed to select from.
        $disallowed = [
            'userid' => 'privacy:metadata:allocationform_disallow:userid',
            'disallow_allocation' => 'privacy:metadata:allocationform_disallow:disallow_allocation',
        ];
        $collection->add_database_table(
            'allocationform_choices',
            $choices,
            'privacy:metadata:allocationform_choices'
        );
        $collection->add_database_table(
            'allocationform_allocations',
            $allocations,
            'privacy:metadata:allocationform_allocations'
        );
        $collection->add_database_table(
            'allocationform_disallow',
            $disallowed,
            'privacy:metadata:allocationform_disallow'
        );
        // No user preferences are stored.
        // No data is exported to any external systems.
        return $collection;
    }

    /**
     * Get the list of contexts that contain user information for the specified user.
     *
     * @param int $userid The user to search.
     * @return contextlist $contextlist The contextlist containing the list of contexts used in this plugin.
     */
    public static function get_contexts_for_userid(int $userid) : contextlist {
        $contextlist = new contextlist();
        $contextlist->set_component('mod_allocationform');
        $params = array(
            'contextlevel' => CONTEXT_MODULE,
            'modname' => 'allocationform',
            'userid' => $userid,
        );
        $choicessql = "SELECT c.id
                         FROM {context} c
                   INNER JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
                   INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
                   INNER JOIN {allocationform} a ON a.id = cm.instance
                   INNER JOIN {allocationform_choices} ch ON a.id = ch.formid
                        WHERE ch.userid = :userid";
        $contextlist->add_from_sql($choicessql, $params);
        $allocationsql = "SELECT c.id
                            FROM {context} c
                      INNER JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
                      INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
                      INNER JOIN {allocationform} a ON a.id = cm.instance
                      INNER JOIN {allocationform_allocations} al ON a.id = al.formid
                           WHERE al.userid = :userid";
        $contextlist->add_from_sql($allocationsql, $params);
        $disallowsql = "SELECT c.id
                          FROM {context} c
                    INNER JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
                    INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
                    INNER JOIN {allocationform} a ON a.id = cm.instance
                    INNER JOIN {allocationform_disallow} d ON a.id = d.formid
                         WHERE d.userid = :userid";
        $contextlist->add_from_sql($disallowsql, $params);
        return $contextlist;
    }

    /**
     * Export all user data for the specified user, in the specified contexts.
     *
     * @param approved_contextlist $contextlist The approved contexts to export information for.
     */
    public static function export_user_data(approved_contextlist $contextlist) {
        global $DB;

        if (empty($contextlist)) {
            return;
        }

        $user = $contextlist->get_user();
        list($contextsql, $contextparams) = $DB->get_in_or_equal($contextlist->get_contextids(), SQL_PARAMS_NAMED);

        // Build up the data about the allocation forms.
        $tutorialsql = "SELECT a.id, c.id AS ctxid
                  FROM {context} c
            INNER JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
            INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
            INNER JOIN {allocationform} a ON a.id = cm.instance
                 WHERE c.id $contextsql";
        $tutorialparams = array(
            'contextlevel' => CONTEXT_MODULE,
            'modname' => 'allocationform',
        );
        $tutorialparams += $contextparams;
        $allocationforms = $DB->get_recordset_sql($tutorialsql, $tutorialparams);

        foreach ($allocationforms as $allocationform) {
            $context = \context::instance_by_id($allocationform->ctxid);
            $data = helper::get_context_data($context, $user);
            writer::with_context($context)
                ->export_data([], $data);
            helper::export_context_files($context, $user);
        }

        static::export_choices($user, $contextlist);
        static::export_allocations($user, $contextlist);
        static::export_rescrictions($user, $contextlist);
    }

    /**
     * Exports the choices a user has made.
     *
     * @param \stdClass $user
     * @param \core_privacy\local\request\approved_contextlist $contextlist
     */
    protected static function export_choices($user, approved_contextlist $contextlist) {
        global $DB;
        list($contextsql, $contextparams) = $DB->get_in_or_equal($contextlist->get_contextids(), SQL_PARAMS_NAMED);

        $sql = "SELECT ch.*, c.id AS ctxid, o1.name AS c1, o2.name AS c2, o3.name AS c3, o4.name AS c4, o5.name AS c5,
                       o6.name AS c6, o7.name AS c7, o8.name AS c8, o9.name AS c9, o10.name AS c10, onw.name AS nw
                  FROM {context} c
            INNER JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
            INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
            INNER JOIN {allocationform} a ON a.id = cm.instance
            INNER JOIN {allocationform_choices} ch ON ch.formid = a.id AND ch.userid = :userid
             LEFT JOIN {allocationform_options} o1 ON ch.choice1 = o1.id AND o1.formid = a.id
             LEFT JOIN {allocationform_options} o2 ON ch.choice2 = o2.id AND o2.formid = a.id
             LEFT JOIN {allocationform_options} o3 ON ch.choice3 = o3.id AND o3.formid = a.id
             LEFT JOIN {allocationform_options} o4 ON ch.choice4 = o4.id AND o4.formid = a.id
             LEFT JOIN {allocationform_options} o5 ON ch.choice5 = o5.id AND o5.formid = a.id
             LEFT JOIN {allocationform_options} o6 ON ch.choice6 = o6.id AND o6.formid = a.id
             LEFT JOIN {allocationform_options} o7 ON ch.choice7 = o7.id AND o7.formid = a.id
             LEFT JOIN {allocationform_options} o8 ON ch.choice8 = o8.id AND o8.formid = a.id
             LEFT JOIN {allocationform_options} o9 ON ch.choice9 = o9.id AND o9.formid = a.id
             LEFT JOIN {allocationform_options} o10 ON ch.choice10 = o10.id AND o10.formid = a.id
             LEFT JOIN {allocationform_options} onw ON ch.notwant = onw.id AND onw.formid = a.id
                 WHERE c.id $contextsql";
        $params = array(
            'contextlevel' => CONTEXT_MODULE,
            'modname' => 'allocationform',
            'userid' => $user->id,
        );
        $params += $contextparams;
        $choices = $DB->get_recordset_sql($sql, $params);

        $subcontext = array(
            get_string('privacy:export:choices', 'mod_allocationform'),
        );
        foreach ($choices as $choice) {
            $context = \context::instance_by_id($choice->ctxid);
            $data = new \stdClass();
            // If a choice is empty if has not been selected, so simply do not export it.
            if (!empty($choice->c1)) {
                $data->choice1 = $choice->c1;
            }
            if (!empty($choice->c2)) {
                $data->choice2 = $choice->c2;
            }
            if (!empty($choice->c3)) {
                $data->choice3 = $choice->c3;
            }
            if (!empty($choice->c4)) {
                $data->choice4 = $choice->c4;
            }
            if (!empty($choice->c5)) {
                $data->choice5 = $choice->c5;
            }
            if (!empty($choice->c6)) {
                $data->choice6 = $choice->c6;
            }
            if (!empty($choice->c7)) {
                $data->choice7 = $choice->c7;
            }
            if (!empty($choice->c8)) {
                $data->choice8 = $choice->c8;
            }
            if (!empty($choice->c9)) {
                $data->choice9 = $choice->c9;
            }
            if (!empty($choice->c10)) {
                $data->choice10 = $choice->c10;
            }
            if (!empty($choice->nw)) {
                $data->notwant = $choice->nw;
            }
            writer::with_context($context)->export_data($subcontext, $data);
        }
    }

    /**
     * Exports the choices a user has been allocated to.
     *
     * @param \stdClass $user
     * @param \core_privacy\local\request\approved_contextlist $contextlist
     */
    protected static function export_allocations($user, approved_contextlist $contextlist) {
        global $DB;
        list($contextsql, $contextparams) = $DB->get_in_or_equal($contextlist->get_contextids(), SQL_PARAMS_NAMED);

        $sql = "SELECT al.*, c.id AS ctxid, o.name
                  FROM {context} c
            INNER JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
            INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
            INNER JOIN {allocationform} a ON a.id = cm.instance
            INNER JOIN {allocationform_allocations} al ON al.formid = a.id AND al.userid = :userid
            INNER JOIN {allocationform_options} o ON o.formid = a.id AND o.id = al.allocation
                 WHERE c.id $contextsql";
        $params = array(
            'contextlevel' => CONTEXT_MODULE,
            'modname' => 'allocationform',
            'userid' => $user->id,
        );
        $params += $contextparams;
        $allocations = $DB->get_recordset_sql($sql, $params);

        $subcontext = get_string('privacy:export:allocations', 'mod_allocationform');

        foreach ($allocations as $allocation) {
            $context = \context::instance_by_id($allocation->ctxid);
            $optioncontext = $allocation->name;
            $data = (object)array(
                'name' => $allocation->name,
            );
            writer::with_context($context)->export_data([$subcontext, $optioncontext], $data);
        }
    }

    /**
     * Exports the options a user has been restricted from selecting.
     *
     * @param \stdClass $user
     * @param \core_privacy\local\request\approved_contextlist $contextlist
     */
    protected static function export_rescrictions($user, approved_contextlist $contextlist) {
        global $DB;
        list($contextsql, $contextparams) = $DB->get_in_or_equal($contextlist->get_contextids(), SQL_PARAMS_NAMED);

        $sql = "SELECT d.*, c.id AS ctxid, o.name
                  FROM {context} c
            INNER JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
            INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
            INNER JOIN {allocationform} a ON a.id = cm.instance
            INNER JOIN {allocationform_disallow} d ON d.formid = a.id AND d.userid = :userid
            INNER JOIN {allocationform_options} o ON o.formid = a.id AND o.id = d.disallow_allocation
                 WHERE c.id $contextsql";
        $params = array(
            'contextlevel' => CONTEXT_MODULE,
            'modname' => 'allocationform',
            'userid' => $user->id,
        );
        $params += $contextparams;
        $restrictions = $DB->get_recordset_sql($sql, $params);

        $subcontext = get_string('privacy:export:restrictions', 'mod_allocationform');

        foreach ($restrictions as $restriction) {
            $context = \context::instance_by_id($restriction->ctxid);
            $optioncontext = $restriction->name;
            $data = (object)array(
                'name' => $restriction->name,
            );
            writer::with_context($context)->export_data([$subcontext, $optioncontext], $data);
        }
    }

    /**
     * Delete all data for all users in the specified context.
     *
     * @param context $context The specific context to delete data for.
     */
    public static function delete_data_for_all_users_in_context(\context $context) {
        global $DB;

        // Check that this is a context_module.
        if (!$context instanceof \context_module) {
            return;
        }

        // Get the course module.
        if (!$cm = get_coursemodule_from_id('allocationform', $context->instanceid)) {
            return;
        }

        $params = ['formid' => $cm->instance];
        $DB->delete_records('allocationform_choices', $params);
        $DB->delete_records('allocationform_allocations', $params);
        $DB->delete_records('allocationform_disallow', $params);
    }

    /**
     * Delete all user data for the specified user, in the specified contexts.
     *
     * @param approved_contextlist $contextlist The approved contexts and user information to delete information for.
     */
    public static function delete_data_for_user(approved_contextlist $contextlist) {
        global $DB;
        $user = $contextlist->get_user();
        foreach ($contextlist as $context) {
            // Check if this is the correct type of context.
            if (!$context instanceof \context_module) {
                continue;
            }
            // Get the course module.
            if (!$cm = get_coursemodule_from_id('allocationform', $context->instanceid)) {
                continue;
            }
            $params = ['formid' => $cm->instance, 'userid' => $user->id];
            $DB->delete_records('allocationform_choices', $params);
            $DB->delete_records('allocationform_allocations', $params);
            $DB->delete_records('allocationform_disallow', $params);
        }
    }

    /**
     * Get the list of users who have data within a context.
     *
     * @param userlist $userlist The userlist containing the list of users who have data in this context/plugin combination.
     * @return void
     */
    public static function get_users_in_context(userlist $userlist) {
        $context = $userlist->get_context();
        if ($context->contextlevel != CONTEXT_MODULE) {
            return;
        }
        $params = array(
            'modname' => 'allocationform',
            'contextid' => $context->id,
            'contextlevel' => CONTEXT_MODULE,
        );
        $choicessql = "SELECT ch.userid
                         FROM {context} c
                   INNER JOIN {course_modules} cm ON cm.id = c.instanceid
                   INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
                   INNER JOIN {allocationform} a ON a.id = cm.instance
                   INNER JOIN {allocationform_choices} ch ON a.id = ch.formid
                        WHERE c.id = :contextid AND c.contextlevel = :contextlevel";
        $userlist->add_from_sql('userid', $choicessql, $params);
        $allocationsql = "SELECT al.userid
                            FROM {context} c
                      INNER JOIN {course_modules} cm ON cm.id = c.instanceid
                      INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
                      INNER JOIN {allocationform} a ON a.id = cm.instance
                      INNER JOIN {allocationform_allocations} al ON a.id = al.formid
                           WHERE c.id = :contextid AND c.contextlevel = :contextlevel";
        $userlist->add_from_sql('userid', $allocationsql, $params);
        $disallowsql = "SELECT d.userid
                          FROM {context} c
                    INNER JOIN {course_modules} cm ON cm.id = c.instanceid
                    INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
                    INNER JOIN {allocationform} a ON a.id = cm.instance
                    INNER JOIN {allocationform_disallow} d ON a.id = d.formid
                         WHERE c.id = :contextid AND c.contextlevel = :contextlevel";
        $userlist->add_from_sql('userid', $disallowsql, $params);
    }

    /**
     * Delete data for multiple users within a single context.
     *
     * @param approved_userlist $userlist The approved context and user information to delete information for.
     * @return void
     */
    public static function delete_data_for_users(approved_userlist $userlist) {
        global $DB;
        $context = $userlist->get_context();
        if ($context->contextlevel != CONTEXT_MODULE) {
            return;
        }
        // Make sure it is an allocation form context.
        if (!$cm = get_coursemodule_from_id('allocationform', $context->instanceid)) {
            return;
        }
        list($sql, $params) = $DB->get_in_or_equal($userlist->get_userids(), SQL_PARAMS_NAMED);
        $params['allocationform'] = $cm->instance;
        $DB->delete_records_select('allocationform_allocations', "formid = :allocationform AND userid $sql", $params);
        $DB->delete_records_select('allocationform_choices', "formid = :allocationform AND userid $sql", $params);
        $DB->delete_records_select('allocationform_disallow', "formid = :allocationform AND userid $sql", $params);
    }
}

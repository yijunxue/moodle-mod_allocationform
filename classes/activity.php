<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Class to control the activity.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform;

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . '/moodlelib.php');
require_once($CFG->dirroot . '/calendar/lib.php');

/**
 * Class to manipulate the allocation form.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class activity {
    /** Calendar event for the allocations being available. */
    const EVENT_AVALIABLE = 'avaliable';

    /** Calendar event for the choice deadline of an allocation form. */
    const EVENT_DEADLINE = 'deadline';

    /**
     * Checks if the allocation form needs an automatic state change.
     *
     * If an automatic change is needed it will update the state and clear the course cache.
     *
     * @global \moodle_database $DB
     * @param \cm_info $cm
     * @return bool
     */
    public static function automatic_state_change(\cm_info $cm) : bool {
        global $DB;
        $statechanged = false;
        if ($cm->customdata['state'] == helper::STATE_READY && $cm->customdata['deadline'] < time()) {
            // The deadline has passed, so send the form for processing.
            $record = $DB->get_record('allocationform', ['id' => $cm->instance], '*', MUST_EXIST);
            $allocationform = new \mod_allocationform\init($record);
            $allocationform->change_state(helper::STATE_PROCESS);
            $statechanged = true;
        }
        return $statechanged;
    }

    /**
     * Checks if a user can access the form.
     *
     * @param int $userid
     * @param \cm_info $cm
     * @return bool
     */
    public static function user_can_access(int $userid, \cm_info $cm) : bool {
        // Editors can always access.
        if (self::user_can_edit($userid, $cm)) {
            return true;
        }
        // Users who can view allocations can access during the review and released states.
        $allocationstate = in_array($cm->customdata['state'], [helper::STATE_REVIEW, helper::STATE_PROCESSED]);
        if ($allocationstate && has_capability('mod/allocationform:viewallocations', $cm->context)) {
            return true;
        }
        // Users who can be allocated can access at anytime, except during editing.
        $restrictedstate = in_array($cm->customdata['state'], [helper::STATE_EDITING]);
        if (!$restrictedstate && self::user_can_be_allocated($userid, $cm)) {
            return true;
        }
        return false;
    }

    /**
     * Checks if a user can make a choice in the allocation form at this time.
     *
     * @param int $userid
     * @param \cm_info $cm
     * @return boolean
     */
    public static function user_can_choose(int $userid, \cm_info $cm) : bool {
        $readystate = $cm->customdata['state'] == helper::STATE_READY;
        $startdatepassed = $cm->customdata['startdate'] < time();
        $deadlinereached = $cm->customdata['deadline'] > time();
        $validtime = $readystate && $startdatepassed && $deadlinereached;
        return $validtime && self::user_can_be_allocated($userid, $cm);
    }

    /**
     * Tests if the user can review the allocations.
     *
     * @param int $userid
     * @param \cm_info $cm
     * @return bool
     */
    public static function user_can_review(int $userid, \cm_info $cm) : bool {
        $context = \context_module::instance($cm->id);
        $reviewstate = $cm->customdata['state'] == helper::STATE_REVIEW;
        $capabilities = ['mod/allocationform:edit', 'mod/allocationform:viewallocations'];
        return $reviewstate && has_any_capability($capabilities, $context, $userid);
    }

    /**
     * Tests if the user can be allocated to an allocation form.
     *
     * @param int $userid
     * @param \cm_info $cm
     * @return bool
     */
    public static function user_can_be_allocated(int $userid, \cm_info $cm) : bool {
        $context = \context_module::instance($cm->id);
        return user_has_role_assignment($userid, $cm->customdata['roleid'], $context->id);
    }

    /**
     * Tests if the user can edit an allocation form.
     *
     * @param int $userid
     * @param \cm_info $cm
     * @return bool
     */
    public static function user_can_edit(int $userid, \cm_info $cm) : bool {
        $context = \context_module::instance($cm->id);
        return has_capability('mod/allocationform:edit', $context, $userid);
    }

    /**
     * Creates an instance of the allocation form activity.
     *
     * @global \moodle_database $DB
     * @param \stdClass $record
     * @return int
     */
    public static function create(\stdClass $record) : int {
        global $DB;
        $time = time();
        $record->timecreated = $time;
        $record->timemodified = $time;
        $record->id = $DB->insert_record('allocationform', $record);
        self::update_calendar($record);
        return $record->id;
    }

    /**
     * Deletes an allocation form.
     *
     * @global \moodle_database $DB
     * @param int $id The id of the allocation form
     * @return bool
     */
    public static function delete(int $id) : bool {
        global $DB;
        if (!$allocationform = $DB->get_record('allocationform', array('id' => $id))) {
            return false;
        }
        $cm = get_coursemodule_from_instance('allocationform', $id, 0, false, MUST_EXIST);
        \core_completion\api::update_completion_date_event($cm->id, 'allocationform', $id, null);
        $DB->delete_records('allocationform_allocations', array('formid' => $allocationform->id));
        $DB->delete_records('allocationform_choices', array('formid' => $allocationform->id));
        $DB->delete_records('allocationform_disallow', array('formid' => $allocationform->id));
        $DB->delete_records('allocationform_options', array('formid' => $allocationform->id));
        $DB->delete_records('allocationform', array('id' => $allocationform->id));
        return true;
    }

    /**
     * Returns an array of users who are participating in an allocation form.
     *
     * @global \moodle_database $DB
     * @param int $id The id of the allocation form.
     * @return boolean|array false if no participants, array of objects otherwise
     */
    public static function get_participants(int $id) {
        global $DB;
        $params = array();
        $params['formid'] = $id;
        $sql = "SELECT DISTINCT u.id
                  FROM {role_assignments} r
                  JOIN {user} u ON u.id = r.userid
             LEFT JOIN {allocationform_choices} a ON u.id = a.userid
                 WHERE a.formid = :formid";
        return $DB->get_records_sql($sql, $params);
    }

    /**
     * Gets a list of options that the a user can choose on a form in their display order.
     *
     * @global \moodle_database $DB
     * @param int $userid The id of the user
     * @param int $id The id of the allocation form record
     * @param string $fields The option fields to be returned, by default id and name (optional)
     * @return array
     */
    public static function get_valid_choices(int $userid, int $id, string $fields = 'id, name') {
        global $DB;
        $params = ['formid' => $id, 'formid2' => $id, 'userid' => $userid];
        $sql = "SELECT $fields
                  FROM {allocationform_options}
                 WHERE formid = :formid
                   AND id NOT IN (
                       SELECT disallow_allocation
                         FROM {allocationform_disallow}
                        WHERE formid = :formid2 AND userid = :userid
                       )
              ORDER BY sortorder";
        return $DB->get_records_sql($sql, $params);
    }

    /**
     * Updates an instance of an allocation form.
     *
     * @global \moodle_database $DB
     * @param \stdClass $record The updated record
     * @return bool
     */
    public static function update(\stdClass $record) : bool {
        global $DB;
        $record->timemodified = time();
        $DB->update_record('allocationform', $record);
        self::update_calendar($record);
        return true;
    }

    /**
     * Add and updates events for an allocation form.
     *
     * @param \stdClass $data The data passed by the editing form.
     */
    protected static function update_calendar(\stdClass $data) {
        $completionexpected = (!empty($data->completionexpected)) ? $data->completionexpected : null;
        \core_completion\api::update_completion_date_event(
                $data->coursemodule,
                'allocationform',
                $data->id,
                $completionexpected
        );
        self::create_deadline_event($data->course, $data->id, $data->name, $data->deadline, $data->visible);
    }

    /**
     * Make an event for the choice deadline.
     *
     * This will have two uses:
     * 1. To let students know when they need to have made their choice by
     * 2. To let teachers know they should review the allocations
     *
     * @global \moodle_database $DB
     * @param int $course The id of the course the allocation form is on.
     * @param int $allocationform The id of the allocation form.
     * @param string $name The name of the allocation form.
     * @param int $deadline The timestamp for the deadline.
     * @param bool $visible If the form is avalibale to students.
     */
    protected static function create_deadline_event(int $course, int $allocationform, string $name, int $deadline, bool $visible) {
        global $DB;
        $details = (object) array(
            'courseid' => $course,
            'description' => '',
            'eventtype' => self::EVENT_DEADLINE,
            'groupid' => 0,
            'instance' => $allocationform,
            'modulename' => 'allocationform',
            'name' => get_string('calendar:deadline', 'mod_allocationform', $name),
            'timeduration' => 0,
            'timestart' => $deadline,
            'timesort' => $deadline,
            'type' => CALENDAR_EVENT_TYPE_ACTION,
            'userid' => 0,
            'visible' => $visible,
        );
        $select = "modulename = :modulename
                   AND instance = :instance
                   AND eventtype = :eventtype
                   AND groupid = 0
                   AND courseid <> 0";
        $params = array('modulename' => 'allocationform', 'instance' => $details->instance, 'eventtype' => $details->eventtype);
        $details->id = $DB->get_field_select('event', 'id', $select, $params);
        if ($details->id) {
            \calendar_event::load($details->id)->update($details, false);
        } else {
            \calendar_event::create($details, false);
        }
    }

    /**
     * Creates or updates an event to state that the allocations are available to users.
     *
     * @global \moodle_database $DB
     * @param \cm_info $cm The course module for an allocation form.
     * @return void
     */
    public static function create_avaliable_event(\cm_info $cm) {
        global $DB;
        if ($cm->modname != 'allocationform') {
            // Just in case someone passes an invalid course module.
            return;
        }
        $details = (object) array(
            'courseid' => $cm->course,
            'description' => $cm->content,
            'eventtype' => self::EVENT_AVALIABLE,
            'groupid' => 0,
            'instance' => $cm->instance,
            'modulename' => 'allocationform',
            'name' => get_string('calendar:avaliable', 'mod_allocationform', $cm->name),
            'timeduration' => 0,
            'timestart' => time(),
            'timesort' => time() + WEEKSECS,
            'type' => CALENDAR_EVENT_TYPE_ACTION,
            'userid' => 0,
            'visible' => $cm->visible,
        );
        $select = "modulename = :modulename
                   AND instance = :instance
                   AND eventtype = :eventtype
                   AND groupid = 0
                   AND courseid <> 0";
        $params = array('modulename' => 'allocationform', 'instance' => $details->instance, 'eventtype' => $details->eventtype);
        $details->id = $DB->get_field_select('event', 'id', $select, $params);
        if ($details->id) {
            \calendar_event::load($details->id)->update($details, false);
        } else {
            \calendar_event::create($details, false);
        }
    }

    /**
     * Deletes the event that says the results are available.
     *
     * @global \moodle_database $DB
     * @param int $id the id of an allocation form.
     * @return void
     */
    public static function delete_avaliable_event(int $id) {
        global $DB;
        $select = "modulename = :modulename
                   AND instance = :instance
                   AND eventtype = :eventtype
                   AND groupid = 0
                   AND courseid <> 0";
        $params = array('modulename' => 'allocationform', 'instance' => $id, 'eventtype' => self::EVENT_AVALIABLE);
        $event = $DB->get_field_select('event', 'id', $select, $params);
        if ($event) {
            \calendar_event::load($event)->delete();
        }
    }

    /**
     * Resets data for all allocation forms in a course.
     *
     * @param \stdClass $data
     * @return array
     */
    public static function reset_user_data(\stdClass $data) {
        $return = [];
        $component = get_string('modulenameplural', 'allocationform');
        if ($data->reset_allocation_state || $data->reset_allocation_userdata) {
            self::delete_allocations_in_course($data->courseid);
            self::delete_choices_in_course($data->courseid);
            self::delete_restrictions_in_course($data->courseid);
            $choiceitem = get_string('deleteuserdata', 'allocationform');
            $return[] = ['component' => $component, 'item' => $choiceitem, 'error' => false];
        }
        if ($data->reset_allocation_state) {
            self::reset_states_in_course($data->courseid);
            $stateitem = get_string('resetstate', 'allocationform');
            $return[] = ['component' => $component, 'item' => $stateitem, 'error' => false];
        }
        if ($data->timeshift) {
            // Shift all dates, events are handled by the calling function.
            $datefields = ['startdate', 'deadline'];
            shift_course_mod_dates('allocationform', $datefields, $data->timeshift, $data->courseid);
            $return[] = ['component' => $component, 'item' => get_string('datechanged'), 'error' => false];
        }
        return $return;
    }

    /**
     * Returns the SQL needed to get a list of form ids in a course.
     *
     * @param int $courseid
     * @return array The first value is the SQL, the second the parameters for the SQL.
     */
    protected static function get_forms_in_course_sql(int $courseid) : array {
        $sql = "SELECT id FROM {allocationform} WHERE course = :courseid";
        $params = ['courseid' => $courseid];
        return [$sql, $params];
    }

    /**
     * Deletes all user restrictions from the allocation forms in a course.
     *
     * @global \moodle_database $DB
     * @param int $courseid
     */
    protected static function delete_restrictions_in_course(int $courseid) {
        global $DB;
        list($formsql, $params) = self::get_forms_in_course_sql($courseid);
        $DB->delete_records_select('allocationform_disallow', "formid IN ($formsql)", $params);
    }

    /**
     * Deletes all the allocations from allocation forms in a course.
     *
     * @global \moodle_database $DB
     * @param int $courseid
     * @return void
     */
    protected static function delete_allocations_in_course(int $courseid) {
        global $DB;
        list($formsql, $params) = self::get_forms_in_course_sql($courseid);
        $DB->delete_records_select('allocationform_allocations', "formid IN ($formsql)", $params);
    }

    /**
     * Deletes all user choices from allocation forms in a course.
     *
     * @global \moodle_database $DB
     * @param int $courseid
     * @return void
     */
    protected static function delete_choices_in_course(int $courseid) {
        global $DB;
        list($formsql, $params) = self::get_forms_in_course_sql($courseid);
        $DB->delete_records_select('allocationform_choices', "formid IN ($formsql)", $params);
    }

    /**
     * Sets all forms in the course back to the editing state.
     *
     * @global \moodle_database $DB
     * @param int $courseid
     * @return void
     */
    protected static function reset_states_in_course(int $courseid) {
        global $DB;
        // Reset the states of all the allocation forms back to editing.
        $editingstate = helper::STATE_EDITING;
        $DB->set_field('allocationform', 'state', $editingstate, ['course' => $courseid]);
        // Delete events that should not be present in the editing state.
        $eventparams = ['modulename' => 'allocationform', 'eventtype' => self::EVENT_AVALIABLE, 'courseid' => $courseid];
        $events = $DB->get_records('event', $eventparams);
        foreach ($events as $event) {
            \calendar_event::load($event)->delete();
        }
    }
}

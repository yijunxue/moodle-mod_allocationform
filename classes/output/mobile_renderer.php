<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing mobile renderers for the allocationform module
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\output;


/**
 * Defines the mobile renderer for the allocationform module.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mobile_renderer extends \plugin_renderer_base {
    /** @var string The directory the templates are in. */
    protected $dir = 'latest';

    /**
     * Sets up the render correctly for the version of the app.
     *
     * @param int $version
     */
    public function set_version(int $version): void {
        if ($version < 3950) {
            // The app used Ionic3.
            $this->dir = 'ionic3';
        }
    }

    /**
     * Renders allocations for all users view.
     *
     * @param \mod_allocationform\output\allallocations $allallocations
     * @return bool|string
     */
    public function render_allallocations(allallocations $allallocations) {
        $data = $allallocations->export_for_template($this);
        return $this->render_from_template("mod_allocationform/mobile/{$this->dir}/allallocations", $data);
    }

    /**
     * Renders allocations for a single user view.
     *
     * @param \mod_allocationform\output\allocation $allocation
     * @return bool|string
     */
    public function render_allocation(allocation $allocation) {
        $data = $allocation->export_for_template($this);
        return $this->render_from_template("mod_allocationform/mobile/{$this->dir}/allocation", $data);
    }

    /**
     * Renders access denied view.
     *
     * @param \mod_allocationform\output\denyaccess $denyaccess
     * @return bool|string
     */
    public function render_denyaccess(denyaccess $denyaccess) {
        $data = $denyaccess->export_for_template($this);
        return $this->render_from_template("mod_allocationform/mobile/{$this->dir}/denyaccess", $data);
    }

    /**
     * Renders the editing view.
     *
     * @param \mod_allocationform\output\editing $editing
     * @return bool|string
     */
    public function render_editing(editing $editing) {
        $data = $editing->export_for_template($this);
        return $this->render_from_template("mod_allocationform/mobile/{$this->dir}/editing", $data);
    }

    /**
     * Renders ready view.
     *
     * @param \mod_allocationform\output\mobile_ready $processing
     * @return bool|string
     */
    public function render_mobile_ready(mobile_ready $processing) {
        $data = $processing->export_for_template($this);
        return $this->render_from_template("mod_allocationform/mobile/{$this->dir}/ready", $data);
    }

    /**
     * Renders processing view.
     *
     * @param \mod_allocationform\output\processing $processing
     * @return bool|string
     */
    public function render_processing(processing $processing) {
        $data = $processing->export_for_template($this);
        return $this->render_from_template("mod_allocationform/mobile/{$this->dir}/processing", $data);
    }
}

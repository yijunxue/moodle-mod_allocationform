<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing renderable for displaying allocations for a single user.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\output;
use mod_allocationform\helper;


/**
 * Renderable for displaying allocations for a single user.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class allocation extends mobilerenderable {
    /** @var \cm_info The course module information for an allocation form. */
    public $cm;

    /** @var \mod_allocationform\output\option_result[] The options on the form, including the users assigned to them. */
    public $options = [];

    /**
     * Gets a populated allocation renderable.
     *
     * @global \moodle_database $DB
     * @param \cm_info $cm
     * @param int $userid
     * @return \mod_allocationform\output\allocation
     */
    public static function get(\cm_info $cm, int $userid) : allocation {
        global $DB;
        $renderable = new allocation();
        $renderable->cm = $cm;
        $params = array(
            'formid' => $cm->instance,
            'userid' => $userid,
        );
        $sql = "SELECT a.formid, a.userid, o.id AS allocation, o.name, o.maxallocation
                  FROM {allocationform_allocations} a
                  JOIN {allocationform_options} o ON a.allocation = o.id
                 WHERE a.formid = :formid AND a.userid = :userid
              ORDER BY o.sortorder ASC";
        $allocations = $DB->get_recordset_sql($sql, $params);
        foreach ($allocations as $allocation) {
            $option = new option_result();
            $option->cm = $cm;
            $option->id = $allocation->allocation;
            $option->maxallocation = $allocation->maxallocation;
            $option->name = $allocation->name;
            $renderable->options[$allocation->allocation] = $option;
        }
        return $renderable;
    }

    /**
     * {@see \templatable::export_for_template}
     */
    public function export_for_template(\renderer_base $output) {
        $progress = new progress($this->cm->customdata['state']);
        $return = (object) array(
            'id' => $this->cm->instance,
            'options' => [],
            'progress' => $progress->export_for_template($output),
            'released' => $this->cm->customdata['state'] == helper::STATE_PROCESSED,
        );
        foreach ($this->options as $option) {
            $return->options[] = $option->export_for_template($output);
        }
        return $return;
    }
}

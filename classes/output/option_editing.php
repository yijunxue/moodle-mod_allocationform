<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing renderable for an option in the editing view.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\output;


/**
 * Renderable for an option in the editing view.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class option_editing implements \renderable, \templatable {
    /** @var \cm_info The course module information for an allocation form. */
    public $cm;

    /** @var int The id of the option record in the database. */
    public $id;

    /** @var int The maximum number of users who can be allocated. */
    public $maxallocation = 0;

    /** @var string The name of the option. */
    public $name;

    /** @var int The number of users who are restricted from this option. */
    public $restrictedusers = 0;

    /**
     * Creates a option_editing renderable from a database record.
     *
     * The record should also contain the count of restricted users for the option.
     *
     * @param \stdClass $record
     * @param \cm_info $cm
     * @return \mod_allocationform\output\option_editing
     */
    public static function create_from_record(\stdClass $record, \cm_info $cm) : option_editing {
        $option = new option_editing();
        $option->cm = $cm;
        $option->id = $record->id;
        $option->name = $record->name;
        $option->maxallocation = $record->maxallocation;
        $option->restrictedusers = $record->restrictedusers;
        return $option;
    }

    /**
     * {@see \templatable::export_for_template}
     */
    public function export_for_template(\renderer_base $output) {
        $urlparams = ['id' => $this->cm->id, 'option' => $this->id];
        $return = array(
            'deleteurl' => new \moodle_url('/mod/allocationform/deleteoption.php', $urlparams),
            'editurl' => new \moodle_url('/mod/allocationform/editoption.php', $urlparams),
            'id' => $this->id,
            'maxallocation' => $this->maxallocation,
            'name' => $this->name,
            'restrictedusers' => $this->restrictedusers,
            'restricturl' => new \moodle_url('/mod/allocationform/editrestriction.php', $urlparams),
        );
        return (object)$return;
    }
}

<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing renderable for the active view.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\output;
use mod_allocationform\activity;
use mod_allocationform\allocate_form;
use mod_allocationform\helper;

/**
 * Renderable for the active view.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class ready implements \renderable, \templatable {
    /** @var \cm_info The course module information for an allocation form. */
    public $cm;

    /** @var \mod_allocationform\allocate_form The form to let a user make choices. */
    public $choiceform;

    /**
     * Constructor.
     *
     * @param allocate_form $form
     * @param \cm_info $cm
     */
    public function __construct(allocate_form $form, \cm_info $cm) {
        $this->cm = $cm;
        $this->choiceform = $form;
    }

    /**
     * {@see \templatable::export_for_template}
     */
    public function export_for_template(\renderer_base $output) {
        global $USER;
        $courseurl = new \moodle_url('/course/view.php', ['id' => $this->cm->course]);
        $editparams = ['id' => $this->cm->id, 'state' => helper::STATE_EDITING];
        $editurl = new \moodle_url('/mod/allocationform/changestate.php', $editparams);
        $exporturl = new \moodle_url('/mod/allocationform/exportchoices.php', ['id' => $this->cm->id]);
        $progress = new progress($this->cm->customdata['state']);
        $canexport = has_capability('mod/allocationform:exportchoices', $this->cm->context);
        $return = (object) array(
            'beforestart' => $this->cm->customdata['startdate'] > time(),
            'canedit' => activity::user_can_edit($USER->id, $this->cm),
            'canexportchoices' => $canexport,
            'courseurl' => $courseurl,
            'choiceform' => '',
            'deadline' => $this->cm->customdata['deadline'],
            'editingurl' => $editurl,
            'exportchoicesurl' => $exporturl,
            'id' => $this->cm->instance,
            'progress' => $progress->export_for_template($output),
            'startdate' => $this->cm->customdata['startdate'],
        );
        ob_start();
        $this->choiceform->display();
        $return->choiceform = ob_get_clean();
        return $return;
    }
}

<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing renderable for the editing view.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\output;
use mod_allocationform\helper;


/**
 * Renderable for the editing view.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class editing extends mobilerenderable {
    /** @var int The number of users who will not allocate successfully due to the number of restrictions placed on them. */
    public $badrestrictions;

    /** @var \cm_info The course module information for an allocation form. */
    public $cm;

    /** @var option_editing[] An array of option records. */
    public $options = [];

    /** @var int The number of users who can be allocated. */
    public $people = 0;

    /** @var int The number of slots available for allocation. */
    public $totalslots = 0;

    /**
     * Gets an editing renderable loaded with data for an allocation form.
     *
     * @global \moodle_database $DB
     * @param \cm_info $cm Course module information for an allocation form.
     * @return \mod_allocationform\output\editing
     */
    public static function get(\cm_info $cm) : editing {
        global $DB;
        $editing = new editing();
        $editing->cm = $cm;
        $editing->people = count_role_users($cm->customdata['roleid'], $cm->context, true);
        // Get the options for the form, including a count of the restricted users.
        $params = ['formid' => $cm->instance];
        $optionssql = "SELECT o.id, o.name, o.maxallocation, o.sortorder, COALESCE(COUNT(d.id), 0) AS restrictedusers
                         FROM {allocationform_options} o
                    LEFT JOIN {allocationform_disallow} d ON d.disallow_allocation = o.id
                        WHERE o.formid = :formid
                     GROUP BY o.id, o.name, o.maxallocation, o.sortorder
                     ORDER BY o.sortorder";
        $options = $DB->get_records_sql($optionssql, $params);
        $optioncount = 0;
        foreach ($options as $option) {
            $editing->options[$option->id] = option_editing::create_from_record($option, $cm);
            $editing->totalslots += $option->maxallocation;
            $optioncount++;
        }
        // Find the number of users who will not be able to be fully allocated due to restrictions the placed on them.
        $sql = "SELECT userid, COUNT(o.formid) as options
                  FROM {allocationform_options} o
                  JOIN {allocationform_disallow} d ON d.disallow_allocation = o.id
                  JOIN {user} u ON u.id = userid
                 WHERE d.formid = :formid AND u.deleted = 0
              GROUP BY userid , o.formid
                HAVING COUNT(o.formid) > :limit";
        $params['limit'] = $optioncount - $cm->customdata['choices'];
        $editing->badrestrictions = count($DB->get_records_sql($sql, $params));
        return $editing;
    }

    /**
     * {@see \templatable::export_for_template}
     */
    public function export_for_template(\renderer_base $output) {
        $activeparams = ['id' => $this->cm->id, 'state' => helper::STATE_READY];
        $activateurl = new \moodle_url('/mod/allocationform/changestate.php', $activeparams);
        $edioptionturl = new \moodle_url('/mod/allocationform/editoption.php', ['id' => $this->cm->id]);
        $progress = new progress($this->cm->customdata['state']);
        $return = (object)array(
            'activateurl' => $activateurl,
            'badrestrictions' => $this->badrestrictions,
            'deadline' => $this->cm->customdata['deadline'],
            'deadlinepassed' => ($this->cm->customdata['deadline'] < time()),
            'id' => $this->cm->instance,
            'newoptionurl' => $edioptionturl,
            'numberofchoices' => $this->cm->customdata['choices'],
            'options' => [],
            'people' => $this->people,
            'progress' => $progress->export_for_template($output),
            'totalallocations' => $this->cm->customdata['allocations'],
            'totalslots' => $this->totalslots,
            'toofewslots' => (($this->totalslots / $this->cm->customdata['allocations']) < $this->people),
        );
        foreach ($this->options as $option) {
            $return->options[] = $option->export_for_template($output);
        }
        return $return;
    }
}

<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing the methods used to get data for the mobile app.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\output;

use mod_allocationform\helper;
use mod_allocationform\activity;


/**
 * Gets data for the mobile app 3.5 and above.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mobile {
    /**
     * Returns the allocation view for the mobile app.
     *
     * @param array $args Arguments from tool_mobile_get_content web service
     * @return array HTML, javascript and otherdata
     */
    public static function allocationform_view(array $args) : array {
        global $PAGE, $USER;
        $args = (object)$args;
        $cm = get_fast_modinfo($args->courseid)->get_cm($args->cmid);
        if ($cm->modname !== 'allocationform') {
            throw new \coding_exception('invalid_module');
        }
        require_login($args->courseid , false , $cm, true, true);
        require_capability ('mod/allocationform:viewform', $cm->context);
        $output = $PAGE->get_renderer('mod_allocationform', 'mobile');

        // Set the renderer up correctly for the version of the Moodle app that is being used.
        $output->set_version($args->appversioncode);

        if (activity::automatic_state_change($cm)) {
            // Gets an upto date cm_info object for the allocationform.
            $cm = get_fast_modinfo($args->courseid)->get_cm($args->cmid);
        }
        if (!activity::user_can_access($USER->id, $cm)) {
            $renderable = new denyaccess($cm);
        } else {
            $renderable = static::get_renderable($cm);
        }

        return array(
            'templates' => array(
                [
                    'id' => 'main',
                    'html' => $output->render($renderable),
                ],
            ),
            'javascript' => $renderable->get_javascript(),
            'otherdata' => $renderable->get_otherdata(),
            'files' => [],
        );
    }

    /**
     * Gets the renderable for the allocation form view.
     *
     * @param \cm_info $cm
     * @return \mod_allocationform\output\mobilerenderable
     */
    protected static function get_renderable(\cm_info $cm) : mobilerenderable {
        global $USER;
        switch ($cm->customdata['state']) {
            case helper::STATE_EDITING: // The form is having it's options edited.
                $renderable = \mod_allocationform\output\editing::get($cm);
                break;
            case helper::STATE_READY: // The form will let people input their choices.
                $renderable = \mod_allocationform\output\mobile_ready::get($cm, $USER->id);
                break;
            case helper::STATE_PROCESS: // The form is waiting on cron to allocate people.
                $renderable = new \mod_allocationform\output\processing($cm);
                break;
            case helper::STATE_REVIEW: // Waiting for an editor to review the allocations.
            case helper::STATE_PROCESSED: // The allocations have been released.
                if (has_capability('mod/allocationform:viewallocations', $cm->context)) {
                    $renderable = \mod_allocationform\output\allallocations::get($cm);
                } else {
                    $renderable = \mod_allocationform\output\allocation::get($cm, $USER->id);
                }
                break;
        }
        return $renderable;
    }
}

<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing renderable for allocation form progress.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\output;
use mod_allocationform\helper;

/**
 * Renderable for allocation form progress.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class progress implements \renderable, \templatable {
    /** @var bool Editing mode is on. */
    public $editing = false;

    /** @var bool allocations are being made. */
    public $process = false;

    /** @var bool Allocations are relased. */
    public $processed = false;

    /** @var bool Users will be able to make choices. */
    public $ready = false;

    /** @var bool Allocations are being reviewed. */
    public $review = false;

    /**
     * Constructor.
     *
     * @param int $state
     */
    public function __construct(int $state) {
        switch ($state) {
            case helper::STATE_EDITING:
                $this->editing = true;
                break;
            case helper::STATE_READY:
                $this->ready = true;
                break;
            case helper::STATE_PROCESS:
                $this->process = true;
                break;
            case helper::STATE_REVIEW:
                $this->review = true;
                break;
            case helper::STATE_PROCESSED:
                $this->processed = true;
                break;
        }
    }

    /**
     * {@see \templatable::export_for_template}
     */
    public function export_for_template(\renderer_base $output) {
        return (object)(array)$this;
    }
}

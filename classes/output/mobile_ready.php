<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing renderable for the active view in the mobile app.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\output;
use mod_allocationform\activity;


/**
 * Renderable for the active view in the mobile app.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mobile_ready extends mobilerenderable {
    /** @var \cm_info The course module information for an allocation form. */
    public $cm;

    /** @var \stdClass The users choice record. */
    public $choices;

    /** @var \mod_allocationform\output\option_mobile_ready[] Array of options the user may select from. */
    public $options = [];

    /**
     * Gets a modbile_ready renderable for an allocations form filled with the correct data.
     *
     * @param \cm_info $cm
     * @param int $userid
     * @return \mod_allocationform\output\mobile_ready
     */
    public static function get(\cm_info $cm, int $userid) : mobile_ready {
        $ready = new mobile_ready();
        $ready->cm = $cm;
        $formid = $cm->instance;
        $options = activity::get_valid_choices($userid, $formid);
        foreach ($options as $option) {
            $ready->options[] = option_mobile_ready::create_from_record($option);
        }
        $allocation = new \mod_allocationform\allocation($userid, $formid);
        $ready->choices = $allocation->get_choices_for_form();
        return $ready;
    }

    /**
     * {@see \templatable::export_for_template}
     */
    public function export_for_template(\renderer_base $output) {
        $numchoices = $this->cm->customdata['choices'];
        $choices = [];
        for ($i = 1; $i <= $numchoices; $i++) {
            $choice = new \stdClass();
            $choice->id = "choice$i";
            $choice->number = $i;
            $choices[] = $choice;
        }
        $options = [];
        foreach ($this->options as $option) {
            $options[] = $option->export_for_template($output);
        }
        return array(
            'beforestart' => $this->cm->customdata['startdate'] > time(),
            'choices' => $choices,
            'deadline' => $this->cm->customdata['deadline'],
            'id' => $this->cm->instance,
            'numallocations' => $this->cm->customdata['allocations'],
            'numchoices' => $numchoices,
            'notwant' => $this->cm->customdata['notwant'],
            'options' => $options,
            'startdate' => $this->cm->customdata['startdate'],
        );
    }

    /**
     * {@see \mod_allocationform\output\mobilerenderable::get_javascript}
     */
    public function get_javascript(): string {
        $js = <<<JS
this.check_response = function(response) {
    // Stores warnings for the whole form.
    this.CONTENT_OTHERDATA.warnings = [];
    // Stores warnings for individual choices.
    this.CONTENT_OTHERDATA.validation = new Object();
    if (!response.status) {
        for (var warning in response.warnings) {
            // Get the warning data from the web service.
            var item = response.warnings[warning].item;
            var itemid = response.warnings[warning].itemid;
            var warningcode = response.warnings[warning].warningcode;
            var message = response.warnings[warning].message;
            if (item === 'allocationform') {
                this.CONTENT_OTHERDATA.warnings.push('plugin.mod_allocationform.' + warningcode);
            } else if (item === 'allocationform_option') {
                for (var option in this.CONTENT_OTHERDATA.data) {
                    var choice = this.CONTENT_OTHERDATA.data[option];
                    if (choice == itemid && !(option === 'notwant' && warningcode === 'choicerequired')) {
                        if (this.CONTENT_OTHERDATA.validation[option] === undefined) {
                            this.CONTENT_OTHERDATA.validation[option] = [];
                        }
                        this.CONTENT_OTHERDATA.validation[option].push('plugin.mod_allocationform.' + warningcode);
                    }
                }
            }
        }
    }
};
JS;
        return $js;
    }

    /**
     * {@see \mod_allocationform\output\mobilerenderable::get_otherdata}
     */
    public function get_otherdata() : array {
        $data = array();
        for ($i = 1; $i <= $this->cm->customdata['choices']; $i++) {
            $id = "choice$i";
            $data[$id] = (isset($this->choices->{$id})) ? $this->choices->{$id} : '0';
        }
        if ($this->cm->customdata['notwant']) {
            $data['notwant'] = (isset($this->choices->notwant)) ? $this->choices->notwant : '0';
        }
        return array(
            'data' => json_encode($data),
            'deadline' => $this->cm->customdata['deadline'],
            'warnings' => json_encode([]),
            'validation' => json_encode(new \stdClass()),
        );
    }
}

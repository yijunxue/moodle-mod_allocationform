<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing renderable for displaying all allocations.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\output;
use mod_allocationform\activity;
use mod_allocationform\helper;


/**
 * Renderable for displaying all allocations.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class allallocations extends mobilerenderable {
    /** @var \cm_info The course module information for an allocation form. */
    public $cm;

    /** @var \mod_allocationform\output\option_result[] The options on the form, including the users assigned to them. */
    public $options = [];

    /** @var \mod_allocationform\output\user[] The users who have not been allocated. */
    public $unallocated = [];

    /**
     * Gets a populated allallocations renderable for an allocation form.
     *
     * @global \moodle_database $DB
     * @param \cm_info $cm The course module information object for an allocation form.
     * @return \mod_allocationform\output\allallocations
     */
    public static function get(\cm_info $cm) : allallocations {
        global $DB;
        $allallocations = new allallocations();
        $allallocations->cm = $cm;
        // Get the allocations.
        $userfieldsapi = \core_user\fields::for_name();
        $usernamefields = $userfieldsapi->get_sql('u', false, '', '', false)->selects;

        $allocationsql = "SELECT a.formid, a.userid, o.id AS allocation, $usernamefields, o.name, o.maxallocation
                            FROM {user} u
                            JOIN {allocationform_allocations} a ON u.id = a.userid
                      RIGHT JOIN {allocationform_options} o ON a.allocation = o.id
                           WHERE o.formid = :formid
                        ORDER BY o.sortorder ASC, u.lastname ASC, u.firstname ASC";
        $params = array(
            'formid' => $cm->instance,
        );
        $allocations = $DB->get_recordset_sql($allocationsql, $params);
        foreach ($allocations as $allocation) {
            if (!isset($allallocations->options[$allocation->allocation])) {
                $allallocations->options[$allocation->allocation] = new option_result();
                $allallocations->options[$allocation->allocation]->cm = $cm;
                $allallocations->options[$allocation->allocation]->id = $allocation->allocation;
                $allallocations->options[$allocation->allocation]->name = $allocation->name;
                $allallocations->options[$allocation->allocation]->maxallocation = $allocation->maxallocation;
            }
            if (!empty($allocation->userid)) {
                $allallocations->options[$allocation->allocation]->users[] = new user($allocation);
            }
        }
        // Get the unallocated users.
        $contexts = $cm->context->get_parent_context_ids();
        list($parentcontexts, $parentparams) = $DB->get_in_or_equal($contexts, SQL_PARAMS_NAMED, 'ctx');
        $sql = "SELECT DISTINCT u.id, $usernamefields
                  FROM {role_assignments} r
                  JOIN {user} u ON u.id = r.userid
                 WHERE (r.contextid = :context OR r.contextid $parentcontexts)
                   AND r.roleid = :roleid
                   AND u.deleted = 0
                   AND u.id NOT IN (SELECT DISTINCT a.userid FROM {allocationform_allocations} a WHERE formid = :formid)
              ORDER BY u.lastname ASC, u.firstname ASC";
        $params['roleid'] = $cm->customdata['roleid'];
        $params['context'] = $cm->context->id;
        $unallocated = $DB->get_recordset_sql($sql, $params + $parentparams);
        foreach ($unallocated as $user) {
            $allallocations->unallocated[$user->id] = new user($user);
        }
        return $allallocations;
    }

    /**
     * {@see \templatable::export_for_template}
     */
    public function export_for_template(\renderer_base $output) {
        $progress = new progress($this->cm->customdata['state']);
        $exportchoice = new \moodle_url('/mod/allocationform/exportchoices.php', ['id' => $this->cm->id]);
        $exportallocations = new \moodle_url('/mod/allocationform/exportallocations.php', ['id' => $this->cm->id]);
        $releaseparams = ['id' => $this->cm->id, 'state' => helper::STATE_PROCESSED];
        $releaseurl = new \moodle_url('/mod/allocationform/changestate.php', $releaseparams);
        $reprocessparams = ['id' => $this->cm->id, 'state' => helper::STATE_PROCESS];
        $reprocessurl = new \moodle_url('/mod/allocationform/changestate.php', $reprocessparams);
        $return = (object) array(
            'canedit' => has_capability('mod/allocationform:edit', $this->cm->context),
            'canexportallocations' => has_capability('mod/allocationform:exportallocations', $this->cm->context),
            'canexportchoices' => has_capability('mod/allocationform:exportchoices', $this->cm->context),
            'canreprocess' => has_capability('mod/allocationform:viewallocations', $this->cm->context),
            'exportallocationurl' => $exportallocations,
            'exportchoiceurl' => $exportchoice,
            'hasunallocated' => count($this->unallocated) > 0,
            'id' => $this->cm->instance,
            'inreview' => $this->cm->customdata['state'] == helper::STATE_REVIEW,
            'options' => [],
            'progress' => $progress->export_for_template($output),
            'releaseurl' => $releaseurl,
            'reprocessurl' => $reprocessurl,
            'unallocated' => [],
        );
        foreach ($this->options as $option) {
            $return->options[] = $option->export_for_template($output);
        }
        foreach ($this->unallocated as $unallocated) {
            $return->unallocated[] = $unallocated->export_for_template($output);
        }
        return $return;
    }
}

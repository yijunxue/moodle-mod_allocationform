<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing renderable for options in the active view in the mobile app.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\output;


/**
 * Renderable for options in the active view in the mobile app.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class option_mobile_ready implements \renderable, \templatable {
    /** @var int The id of the option record in the database. */
    public $id;

    /** @var string The name of the option. */
    public $name;

    /**
     * Creates a option_mobile_ready object.
     *
     * @param \stdClass $record
     * @return \mod_allocationform\output\option_mobile_ready
     */
    public static function create_from_record(\stdClass $record) : option_mobile_ready {
        $option = new option_mobile_ready();
        $option->id = $record->id;
        $option->name = $record->name;
        return $option;
    }

    /**
     * {@see \templatable::export_for_template}
     */
    public function export_for_template(\renderer_base $output) {
        return array(
            'id' => $this->id,
            'name' => $this->name,
        );
    }
}

<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing renderable for the options in the review and release views.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\output;

/**
 * Renderable for the options in the review and release views.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class option_result implements \renderable, \templatable {
    /** @var \cm_info The course module information for an allocation form. */
    public $cm;

    /** @var int The id of the option record in the database. */
    public $id;

    /** @var int The maximum number of users who can be allocated. */
    public $maxallocation = 0;

    /** @var string The name of the option. */
    public $name;

    /** @var \mod_allocationform\output\user[] The users who have been allocated to the option. */
    public $users = [];

    /**
     * {@see \templatable::export_for_template}
     */
    public function export_for_template(\renderer_base $output) {
        $urlparams = ['id' => $this->cm->id, 'option' => $this->id];
        $editurl = new \moodle_url('/mod/allocationform/editoption.php', $urlparams);
        $return = (object) array(
            'editurl' => $editurl,
            'maxallocation' => $this->maxallocation,
            'name' => $this->name,
            'users' => [],
        );
        foreach ($this->users as $user) {
            $return->users[] = $user->export_for_template($output);
        }
        return $return;
    }
}

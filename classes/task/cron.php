<?php
// This file is part of the Allocation form activity.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing CRON task which allocates student choices.
 *
 * @package   mod_allocationform
 * @category  task
 * @copyright 2015 University of Nottingham
 * @author    Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\task;
use \mod_allocationform\helper;


/**
 * CRON task to allocate student choices.
 *
 * @package   mod_allocationform
 * @category  task
 * @copyright 2015 University of Nottingham
 * @author    Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class cron extends \core\task\scheduled_task {

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('cron', 'mod_allocationform');
    }

    /**
     * Do the job.
     * Throw exceptions on errors (the job will be retried).
     *
     * @return void
     */
    public function execute() {
        global $DB;

        // Emulate normal session - we use admin account by default. Needed to add new events.
        cron_setup_user(null, null, true);

        // Set the state of all allocation forms that have passed their deadline and have a state of ready to await processing.
        $now = time();

        $params = array('now' => $now, 'ready' => helper::STATE_READY);
        $DB->set_field_select('allocationform', 'state', helper::STATE_PROCESS,
                "deadline <= :now AND state = :ready", $params);

        // Find all allocation forms that are awaiting processing and do the allocation.
        $rs = $DB->get_recordset('allocationform', array('state' => helper::STATE_PROCESS));
        $allocations = 0;

        foreach ($rs as $record) {
            $allocation = new \mod_allocationform\init($record);
            $unallocatedchoices = $allocation->allocate();
            if ($unallocatedchoices && $unallocatedchoices !== '') {
                mtrace($unallocatedchoices . "\n");
            }
            $allocations++;
        }

        $rs->close();

        mtrace($allocations . ' allocation forms processed.' . "\n");
    }

}

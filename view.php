<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of allocationform
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use mod_allocationform\helper;
use mod_allocationform\activity;

require_once(dirname(dirname(__DIR__)) . '/config.php');
require_once(__DIR__ . '/lib.php');
require_once($CFG->libdir . '/completionlib.php');

$id = optional_param('id', 0, PARAM_INT); // Course_module ID.
$instanceid = optional_param('n', 0, PARAM_INT);  // Allocationform instance ID.

if ($id) {
    list($course, $cm) = get_course_and_cm_from_cmid($id, 'allocationform');
} else if ($instanceid) {
    list($course, $cm) = get_course_and_cm_from_instance($instanceid, 'allocationform');
} else {
    throw new \moodle_exception('invalidaccess');
}

require_login($course, true, $cm);
$context = context_module::instance($cm->id);
$courseurl = new moodle_url('/course/view.php', array('id' => $course->id));

// Print the page header.
$PAGE->set_url('/mod/allocationform/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($cm->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);

require_capability('mod/allocationform:viewform', $context); // User does not have permission to view the allocation form.
// Tell the page to use the allocationform renderer.
$output = $PAGE->get_renderer('mod_allocationform');

if (activity::automatic_state_change($cm)) {
    // Gets an upto date cm_info object for the allocationform.
    $cm = get_fast_modinfo($course)->get_cm($cm->id);
}

if (!activity::user_can_access($USER->id, $cm)) {
    $event = \mod_allocationform\event\access_denied::create(array(
        'objectid' => $cm->instance,
        'context' => $context,
    ));
    $event->trigger();
    $renderable = new \mod_allocationform\output\denyaccess($cm);
    echo $output->header();
    echo $output->heading(format_string($cm->name));
    echo $output->render($renderable);
    echo $output->footer();
    exit;
}

switch ($cm->customdata['state']) {
    case helper::STATE_EDITING: // The form is having it's options edited.
        $renderable = \mod_allocationform\output\editing::get($cm);
        break;

    case helper::STATE_READY: // The form will let people input their choices.
        $allocation = new \mod_allocationform\allocation($USER->id, $cm->instance);
        // Create the form.
        $choiceformparams = array(
            'course' => $course->id,
            'allocationform' => $cm->instance,
            'id' => $cm->id,
            'function' => helper::FUNC_SAVE_PREFS,
            'choices' => $cm->customdata['choices'],
            'allocation' => $cm->customdata['allocations'],
            'notwant' => $cm->customdata['notwant'],
            'user' => $USER->id,
        );
        $allocateform = new \mod_allocationform\allocate_form(null, $choiceformparams);
        $allocateform->set_data($allocation->get_choices_for_form());

        if ($cm->customdata['startdate'] < time()) {
            // Process the form if the start date has passed.
            $savedata = activity::user_can_be_allocated($USER->id, $cm);
            $allocation->parse_form($allocateform, $savedata);
            // Mark viewed by user (if required).
            $completion = new completion_info($course);
            $completion->set_module_viewed($cm);
        }
        $renderable = new \mod_allocationform\output\ready($allocateform, $cm);
        break;

    case helper::STATE_PROCESS: // The form is waiting on cron to allocate people.
        $renderable = new \mod_allocationform\output\processing($cm);
        break;

    case helper::STATE_PROCESSED:
        // The form has been allocated, and reviewed. it is now available to be viewed by anyone.
        $completion = new completion_info($course);
        $completion->set_module_viewed($cm);

    case helper::STATE_REVIEW: // Waiting for an editor to review the allocations.
        if (has_capability('mod/allocationform:viewallocations', $context)) {
            $renderable = \mod_allocationform\output\allallocations::get($cm);
        } else {
            $renderable = \mod_allocationform\output\allocation::get($cm, $USER->id);
        }
        break;

    default: // We should never get here unless someone has done something bad with the database.
        throw new \moodle_exception('invalidstate', 'mod_allocationform', $courseurl);
}

// Log that the page has been viewed.
$event = \mod_allocationform\event\allocations_viewed::create(array(
    'objectid' => $cm->instance,
    'context' => $context
));
$event->trigger();

echo $output->header();
echo $output->heading(format_string($cm->name));

// Render the activity information.
$completiondetails = \core_completion\cm_completion_details::get_instance($cm, $USER->id); // Fetch completion information.
$activitydates = \core\activity_dates::get_dates_for_module($cm, $USER->id); // Fetch activity dates.
echo $output->activity_information($cm, $completiondetails, $activitydates);

echo $output->render($renderable);
echo $output->footer();

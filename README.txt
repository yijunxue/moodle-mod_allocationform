ABOUT
==========
The 'Allocation form' module was developed on behalf of The University of Nottingham by
    Neill Magill
    Barry Oosthuizen

This module may be distributed under the terms of the General Public License
(see http://www.gnu.org/licenses/gpl.txt for details)

PURPOSE
==========
The Allocation form module is designed to allow instructors of a course
to create slots that the students can sign themselves upto.

INSTALLATION
==========
The Allocation form module follows the standard installation procedure.

1. Create folder <path to your moodle dir>/mod/allocationform.
2. Extract files from folder inside archive to created folder.
3. Visit page Home ► Site administration ► Notifications to complete installation.

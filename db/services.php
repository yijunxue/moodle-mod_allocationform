<?php
// This file is part of the Allocation form plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
//

/**
 * Setup the webservices for the plugin.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$functions = array(
    'mod_allocationform_choices_submit' => array(
        'classname' => 'mod_allocationform\external\choices',
        'methodname' => 'submit',
        'classpath' => '/mod/allocationform/classes/external/choices.php',
        'description' => "Records the options that a user would like to be assigned to.",
        'type' => 'write',
        'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE, 'local_mobile'),
    ),
    'mod_allocationform_view_allocationform' => array(
        'classname' => 'mod_allocationform\external\view',
        'methodname' => 'view',
        'classpath' => '/mod/allocationform/classes/external/view.php',
        'description' => "Logs that the user viewed the allocation form activity",
        'type' => 'write',
        'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE, 'local_mobile'),
    ),
    'mod_allocationform_view_denied' => array(
        'classname' => 'mod_allocationform\external\access_denied',
        'methodname' => 'view',
        'classpath' => '/mod/allocationform/classes/external/access_denied.php',
        'description' => "Logs that the user was denied access to view the allocation form activity",
        'type' => 'write',
        'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE, 'local_mobile'),
    ),
);

<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines the plugins mobile support.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/* @global \stdClass $CFG The global configuration object. */
$addons = array(
    'mod_allocationform' => array(
        'handlers' => array(
            'allocationform' => array(
                'displaydata' => array(
                    'icon' => $CFG->wwwroot . '/mod/allocationform/pix/icon.svg',
                    'class' => '',
                ),
                'delegate' => 'CoreCourseModuleDelegate',
                'method' => 'allocationform_view',
            ),
        ),
        'lang' => array(
            ['al_notwant', 'allocationform'],
            ['allocations', 'allocationform'],
            ['allocations_not_visible', 'allocationform'],
            ['cannoteditinapp', 'allocationform'],
            ['choice', 'allocationform'],
            ['choicealreadyselected', 'allocationform'],
            ['choiceincorrectstate', 'allocationform'],
            ['choiceinvalid', 'allocationform'],
            ['choicerequired', 'allocationform'],
            ['deadline_message', 'allocationform'],
            ['incorrectnumberofchoices', 'allocationform'],
            ['instructions', 'allocationform'],
            ['instructionsnotwant', 'allocationform'],
            ['no_allocations', 'allocationform'],
            ['nopermission', 'allocationform'],
            ['not_active', 'allocationform'],
            ['notrequired', 'allocationform'],
            ['nousersallocated', 'allocationform'],
            ['queued_for_processing', 'allocationform'],
            ['savefail', 'allocationform'],
            ['unallocated', 'allocationform'],
            ['youralloactions', 'allocationform'],
        ),
    ),
);

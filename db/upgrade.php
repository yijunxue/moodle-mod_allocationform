<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file keeps track of upgrades to the allocationform module
 *
 * Sometimes, changes between versions involve alterations to database
 * structures and other major things that may break installations. The upgrade
 * function in this file will attempt to perform all the necessary actions to
 * upgrade your older installation to the current version. If there's something
 * it cannot do itself, it will tell you what you need to do.  The commands in
 * here will all be database-neutral, using the functions defined in DLL libraries.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Execute allocationform upgrade from the given old version
 *
 * @param int $oldversion
 * @return bool
 */
function xmldb_allocationform_upgrade($oldversion) {
    global $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2014040301) {

        // Define field trackcompletion to be added to allocationform.
        $table = new xmldb_table('allocationform');
        $field = new xmldb_field('trackcompletion', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0', 'roleid');

        // Conditionally launch add field trackcompletion.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Allocationform savepoint reached.
        upgrade_mod_savepoint(true, 2014040301, 'allocationform');
    }

    if ($oldversion < 2014070900) {

        $sql = "SELECT  o.id, o.formid, o.name FROM {allocationform_options} o
                JOIN {allocationform_options} d ON d.formid = o.formid AND d.name = o.name
                WHERE o.id <> d.id ORDER BY o.formid, o.name, o.id";

        $duplicates = $DB->get_records_sql($sql);

        if (!empty($duplicates)) {
            $previousformid = '';
            $previousname = '';

            $i = 1;

            foreach ($duplicates as $option) {

                if ($previousformid === $option->formid && $previousname === $option->name) {
                    $i++;
                } else {
                    $i = 1;
                }

                $previousformid = $option->formid;
                $previousname = $option->name;
                $option->name = "$option->name #$i";
                $DB->update_record('allocationform_options', $option);
            }
        }

        // Define index formid_name_index (unique) to be added to allocationform_options.
        $table = new xmldb_table('allocationform_options');
        $index = new xmldb_index('formid_name_index', XMLDB_INDEX_UNIQUE, array('formid', 'name'), array('uniqueoptions'));

        // Conditionally launch add index formid_name_index.
        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }

        // Allocationform savepoint reached.
        upgrade_mod_savepoint(true, 2014070900, 'allocationform');
    }

    return true;
}
